const menu = [
  {
    icon: 'ChartSquare',
    path: '/home',
    label: 'ภาพรวม'
  },
  {
    icon: 'Question',
    path: '/questionaire',
    label: 'คำถาม'
  },
  {
    icon: 'User',
    path: '/user',
    label: 'ผู้ใช้งาน'
  },
  {
    icon: 'Setting',
    path: '/setting',
    label: 'ตั้งค่าระบบ'
  }
]
export default menu
