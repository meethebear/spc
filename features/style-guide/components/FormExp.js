import React, { useCallback } from 'react'
import { Card } from 'antd'
import { Form, Field, useForm, getMessageReq } from '@/components/form'
import Button from '@/components/Button'
import moment from 'moment'
import checkAllowedPassword from '@/utils/allowedPassword'

const INIT_FORM = {
  input: '',
  password: '',
  number: '',
  select: '',
  radio: 'male',
  checkbox: [],
  date_picker: moment(),
  date_range_picker: [],
  text_area: '',
  time_picker: null,
  upload: null
}

const RUlE_FORM = {
  input: {
    required: getMessageReq('input')
  },
  password: {
    required: getMessageReq('input'),
    isAllowed: {
      func: (value) => checkAllowedPassword(value),
      msg: 'Use 8-30 characters with a mix of uppercase, lowercase letters, number, and symbols'
    }
  },
  date_picker: {
    required: getMessageReq('Date Picker')
  },
}

const OptionSelect = [
  {
    id: '1',
    name: 'b1'
  },
  {
    id: '2',
    name: 'b2'
  },
  {
    id: '3',
    name: 'b3'
  }
]

const SEX = [
  {
    value: 'male',
    label: 'Male'
  },
  {
    value: 'female',
    label: 'Female'
  },
]

const CheckboxOptions = [
  {
    value: 'a1',
    label: 'A1'
  },
  {
    value: 'a2',
    label: 'A2'
  },
  {
    value: 'b1',
    label: 'b1'
  },
  {
    value: 'b2',
    label: 'b2'
  },
]
const FormExp = () => {
  const form = useForm({
    initialValues: INIT_FORM,
    rules: RUlE_FORM
  })

  const buildBody = useCallback((values, next) => {
    const body = {
      input: values.input || null,
      password: values.password || null,
      number: values.number || null,
      select: values.select || null,
      radio: values.radio || null,
      checkbox: values.checkbox || null,
      date_picker: values.date_picker?.format?.() || null,
      date_range_start: values.date_range_picker?.[0]?.format?.() || null,
      date_range_end: values.date_range_picker?.[1]?.format?.() || null,
      text_area: values.text_area || null,
      time_picker: values.time_picker?.format?.('HH:mm') || null,
      upload: values.upload?.file || null
    }

    next(body, values)
  }, [])

  const handlerSubmit = useCallback(async (body, values) => {
    console.log('body', body)
    console.log('values', values)
    // const {} = await callAPI(body)
  }, [])
  return (
    <section id='form-exp'>
      <Card>
        <Form form={form} handlerSubmit={[buildBody, handlerSubmit]}>
          <Field.Input
            name='input'
            id='input'
            label='Input'
          />
          <Field.Password
            name='password'
            id='password'
            label='Password'
          />
          <Field.Number
            name='number'
            id='number'
            label='Number'
          />
          <Field.Select
            name='select'
            id='select'
            label='Select'
            options={OptionSelect}
            keys={['id', 'name']}
          />
          <Field.Radio
            name='radio'
            id='radio'
            label='Radio'
            options={SEX}
          />
          <Field.Checkbox
            name='checkbox'
            id='checkbox'
            label='Checkbox'
            options={CheckboxOptions}
          />
          <Field.DatePicker
            name='date_picker'
            id='date_picker'
            label='Date Picker'
          />
          <Field.DateRangePicker
            name='date_range_picker'
            id='date_range_picker'
            label='Date Range Picker'
          />
          <Field.TextArea
            name='text_area'
            id='text_area'
            label='TextArea'
          />
          <Field.TimePicker
            name='time_picker'
            id='time_picker'
            label='Time picker'
          />
          <Field.Upload
            name='upload'
            id='upload'
            label='Upload'
          />
          <Button htmlType='submit'>
            Submit
          </Button>
        </Form>
        <br />
        <br />
        <a href='https://olapat.github.io/react-useform-doc/' target={'_blank'} rel="noreferrer">
          Web Docs useFrom
        </a>
      </Card>
    </section>
  )
}

export default FormExp