import React, { useCallback } from 'react'
import { Card, Row, Col } from 'antd'
import { Form, Field, useForm, getMessageReq, useFormList, FormList } from '@/components/form'
import Button from '@/components/Button'
import moment from 'moment'

const INIT_FORM = {
  input: '',
  password: '',
  number: '',
  select: '',
  radio: 'male',
  checkbox: [],
  date_picker: moment(),
  date_range_picker: [],
  text_area: '',
  time_picker: null,
  upload: null,

}

const initData = [
  {
    "amount": 1234,
    "is_delete": false,
    "name": "product1",
    "type_save": "null",
    "unit": "Dr"
  }
]

const RUlE_FORM = {
  input: {
    required: getMessageReq('input')
  },
}

const RUlE_ITEM = {
  amount: {
    required: getMessageReq('amount')
  },
  name: {
    required: getMessageReq('name')
  },
}

const OptionSelect = [
  {
    id: '1',
    name: 'b1'
  },
  {
    id: '2',
    name: 'b2'
  },
  {
    id: '3',
    name: 'b3'
  }
]

const SEX = [
  {
    value: 'male',
    label: 'Male'
  },
  {
    value: 'female',
    label: 'Female'
  },
]

const CheckboxOptions = [
  {
    value: 'a1',
    label: 'A1'
  },
  {
    value: 'a2',
    label: 'A2'
  },
  {
    value: 'b1',
    label: 'b1'
  },
  {
    value: 'b2',
    label: 'b2'
  },
]

const KEYS_LIST = {
  PRODUCT: 'products'
}

const FormExp = () => {
  const form = useFormList({
    initialValues: INIT_FORM,
    rules: RUlE_FORM
  }, {
    initialValues: initData?.map(item => ({
      values: {
        amount: item.amount,
        name: item.name,
        type_save: item.type_save,
        unit: item.unit,
      },
      errors: {},
      rules: RUlE_ITEM
    }))
  })

  const { listCtl } = form

  const buildBody = useCallback((values, next) => {
    const body = {
      input: values.input || null,
      password: values.password || null,
      number: values.number || null,
      select: values.select || null,
      radio: values.radio || null,
      checkbox: values.checkbox || null,
      date_picker: values.date_picker?.format?.() || null,
      date_range_start: values.date_range_picker?.[0]?.format?.() || null,
      date_range_end: values.date_range_picker?.[1]?.format?.() || null,
      text_area: values.text_area || null,
      time_picker: values.time_picker?.format?.('HH:mm') || null,
      upload: values.upload?.file || null,
      products: values.products
    }

    next(body, values)
  }, [])

  const handlerSubmit = useCallback(async (body, values) => {
    console.log('body', body)
    console.log('values', values)
    // const {} = await callAPI(body)
  }, [])

  const onClickAddItem = useCallback(() => {
    listCtl.addListItem({
      values: {
        amount: '',
        name: '',
        type_save: '',
        unit: '',
      },
      rules: RUlE_ITEM,
      errors: {}
    })
  }, [listCtl.addListItem])

  const onChangeItem = useCallback((index, name, value) => {
    listCtl.onChange(index, name, value)
  }, [listCtl.onChange])

  return (
    <section id='form-exp'>
      <Card>
        <FormList form={form} handlerSubmit={[buildBody, handlerSubmit]} listCtls={{ [KEYS_LIST.PRODUCT]: listCtl }}>
          <Field.Input
            name='input'
            id='input'
            label='Input'
          />
          <Field.Number
            name='number'
            id='number'
            label='Number'
          />
          <Field.Select
            name='select'
            id='select'
            label='Select'
            options={OptionSelect}
            keys={['id', 'name']}
          />
          <Field.Radio
            name='radio'
            id='radio'
            label='Radio'
            options={SEX}
          />
          <Field.Checkbox
            name='checkbox'
            id='checkbox'
            label='Checkbox'
            options={CheckboxOptions}
          />
          <Field.DatePicker
            name='date_picker'
            id='date_picker'
            label='Date Picker'
          />
          <Field.DateRangePicker
            name='date_range_picker'
            id='date_range_picker'
            label='Date Range Picker'
          />
          <Field.TextArea
            name='text_area'
            id='text_area'
            label='TextArea'
          />
          <Field.TimePicker
            name='time_picker'
            id='time_picker'
            label='Time picker'
          />
          <Field.Upload
            name='upload'
            id='upload'
            label='Upload'
          />
          <br />

          {listCtl.values?.map?.((item, index) => (
            <Row gutter={16} style={{ marginTop: 16, paddingLeft: 25, paddingRight: 25 }} align={'top'} key={index}>
              <Col span={4}>
                <Field.Number
                  name='amount'
                  label='Amount'
                  id={`amount${index}`}
                  value={item.values.amount}
                  required={!!item.rules.amount.required}
                  error={!!item.errors.amount}
                  onChange={(name, value) => onChangeItem(index, name, value)}
                />
              </Col>
              <Col span={4}>
                <Field.Input
                  name='name'
                  label='Name'
                  value={item.values.name}
                  required={!!item.rules.name.required}
                  error={!!item.errors.name}
                  onChange={(name, value) => onChangeItem(index, name, value)}
                />
              </Col>
              <Col>
              <Button htmlType='button' onClick={() => listCtl.removeListItem(index)} type='default'>
                -
              </Button>
              </Col>
            </Row>
          ))}
          <Button htmlType='button' onClick={onClickAddItem} type='default'>
            Add
          </Button>
          <br />
          <br />
          <Button htmlType='submit'>
            Submit
          </Button>
        </FormList>
        <br />
        <br />
        <a href='https://olapat.github.io/react-useform-doc/' target={'_blank'} rel="noreferrer">
          Web Docs useFrom
        </a>
      </Card>
    </section>
  )
}

export default FormExp