import FormListExp from '../components/FormListExp'

const FormList = () => {
  return (
    <main>
      <FormListExp />
    </main>
  )
}

export default FormList
