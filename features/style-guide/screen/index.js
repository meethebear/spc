import FormExp from '../components/FormExp'

const StyleGuide = () => {
  return (
    <main>
      <FormExp />
    </main>
  )
}

export default StyleGuide