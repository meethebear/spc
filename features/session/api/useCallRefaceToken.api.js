
import { useCallback } from 'react'
import { useSelector } from 'react-redux'

import selector from '@/utils/selector'
import useAPI from '@/utils/hook/useAPIService'

const useCallRefaceToken = () => {
  const name = 'refaceToken'
  const API = useAPI(name, 'none')
  const { tasksRunning } = useSelector(selector(['tasksRunning']))

  const apiCallRefaceToken = useCallback(async () => {
    try {
      API.begin()
      const { data } = await API.get('/api/refresh-token', { auth: false })

      return { success: true, data }
    } catch (error) {
      console.log(error)
      API.error(error.response)

      return { success: false }
    } finally {
      API.end()
    }
  }, [API])

  return [apiCallRefaceToken, tasksRunning?.[name]?.loading]
}

export default useCallRefaceToken
