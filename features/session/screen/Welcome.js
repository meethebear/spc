import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { useDispatch } from 'react-redux'
import { userLogin } from '@/actions/user.action'
import pm from '@/utils/permission'
import PropTypes from 'prop-types'

const Welcome = props => {
  const { session } = props
  const dispatch = useDispatch()
  const { replace } = useRouter()

  useEffect(() => {
    if (session?.token) {
      dispatch(userLogin(session))
      if (pm(session).isSuperAdmin()) {
        replace('/backoffice/user')
      } else {
        replace('/backoffice')
      }
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return null
}

Welcome.propTypes = {
  session: PropTypes.object
}

export default Welcome
