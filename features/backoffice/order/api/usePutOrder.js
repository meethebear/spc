import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector } from "react-redux";
import { message } from 'antd'
import { resError, resOK } from "@/utils/response";

const usePutOrder = () => {
  const name = "useUpdateOrder";
  const API = useAPI(name, 'none')
  const { tasksRunning } = useSelector(selector(["tasksRunning"]));

  const editOrder = async (body) => {
    console.log(body);
    try {
      API.begin();
      const { data } = await API.put(`/api/v1/order`, body)
      const res = resOK(data)
      message.success(res.message)
      return res
    } catch (error) {
      const e = resError(error);
      API.error(e);
    } finally {
      API.end();
    } return { 
      success: false
    }
    
  };

  return [tasksRunning?.[name]?.loading, editOrder];
};

export default usePutOrder;