import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector } from "react-redux";
import { message } from 'antd'
import { resError, resOK } from "@/utils/response";
import { useState } from "react";

const useCreateOrder = () => {
  const name = "CreateOrder";
  const API = useAPI(name, 'overlay')
  const { tasksRunning } = useSelector(selector(["tasksRunning"]));
  const [result, useResult ] = useState();
  const createorder = async (body) => {
    try {
      API.begin();
      const { data } = await API.post(`/api/v1/order`, body)
      const res = resOK(data)
      useResult(res)
      message.success(res.message)
      return res
    } catch (error) {
      const e = resError(error);
      API.error(e);
      message.error(e?.errorMessage);
    } finally {
      API.end();
    }
    return {
      success: false
    }
  };

  return [result?.data, tasksRunning?.[name]?.loading, createorder];
};

export default useCreateOrder;