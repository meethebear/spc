import { useState, useEffect, useCallback } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector, useDispatch } from "react-redux";
import { resError, resOK } from "@/utils/response";
import { actionIndexOrder } from '@/app/actions/order.action';
import moment from "moment";

const useFetchOrder = () => {
  const name = 'fetchDataOrder'
  const API = useAPI(name, 'none')
  const { order, tasksRunning } = useSelector(selector(['order', 'tasksRunning']))
  const useThunkDispatch = () => useDispatch()
  const dispatch = useThunkDispatch()

  const fetchDataOrder = useCallback(async (filter = {}) => {
    try {
      API.begin()
      const { data } = await API.get('/api/v1/order', { params: { ...filter } })
      const res = resOK(data)
      dispatch(actionIndexOrder({
        filter: { page: res.data.page, page_size: res.data.page_size, ...filter, }, data: res.data
      }))
    } catch (error) {
      const e = resError(error)
      API.error(e)
    } finally {
      API.end()
    }
  }, [])

  return [order, tasksRunning?.[name]?.loading, fetchDataOrder]
}

export default useFetchOrder