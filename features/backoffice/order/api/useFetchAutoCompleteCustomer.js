import { useState, useEffect, useCallback } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector, useDispatch } from "react-redux";
import { resError, resOK } from "@/utils/response";

const useFetchAutoCompleteCustomer = () => {
  const name = 'fetchGetCustomerAutocomplete'
  const API = useAPI(name, 'none')
  const { tasksRunning } = useSelector(selector(['tasksRunning']))
  const [result, useResult] = useState();

  const fetchGetCustomerAutocomplete = useCallback(async (filter = {}) => {
    try {
      API.begin()
      const { data } = await API.get('/api/v1/master/customer', { params: { ...filter } })
      const res = resOK(data)
      useResult(res)
    } catch (error) {
      const e = resError(error)
      API.error(e)
    } finally {
      API.end()
    }
  }, [])

  useEffect(() => {
    fetchGetCustomerAutocomplete()
  }, [])

  return [result?.data, tasksRunning[name]?.loading, fetchGetCustomerAutocomplete]

}

export default useFetchAutoCompleteCustomer 