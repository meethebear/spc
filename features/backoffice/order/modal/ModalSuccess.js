import { Modal, Button, Typography } from "antd"
import { CheckCircleFilled } from '@ant-design/icons';

const { Text } = Typography

const Success = (props) => {
  const { visible, closeModal } = props
  return (
    <Modal className="modal-footer-center"
      visible={visible}
      onCancel={closeModal}
      footer={[
        <Button
        key="submit"
        htmlType="submit"
        onClick={closeModal}
        style={{ backgroundColor: "#FF8E0DE5", borderRadius: 8, color: "#FAFAFA", width: 200}}
      >
        ตกลง
      </Button>
      ]}>
      <div style={{ display: "flex", justifyContent: "center", flexDirection: "column" }}>
        <CheckCircleFilled style={{fontSize: '600%', color: '#2EB373'}}/>
        <Text style={{ display: "flex", justifyContent: "center", marginTop: 10, marginBottom: 10, fontWeight: 600, fontSize: 16 }}>การจัดส่งสำเร็จ</Text>
        <Text type="secondary" style={{ display: "flex", width: "90%", margin: "auto", textAlign: "center", justifyContent:'center' }}>
          การจัดส่งออเดอร์หมายเลข #
          สำเร็จเรียบร้อยแล้ว
        </Text>
      </div>
    </Modal>
  )
}
export default Success