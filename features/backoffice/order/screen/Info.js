import { Spin } from 'antd'
import InfoOrder from '../components/InfoOrder'
import useGetAPI from '@/utils/hook/useGetAPI'
import PropTypes from 'prop-types'
import { Row, Breadcrumb } from "antd"
import { HomeOutlined } from "@ant-design/icons";

const status_type = {
  PRODUCTION_PENDING: ["#afafaf", "#202020", "รอผลิต"],
  PRODUCTION_DOING: ["#D2DAFF", "#0029FF", "กำลังผลิต"],
  PRODUCTION_FINISHED: ["#C7FEC9", "#09A910", "ผลิตเสร็จ"],
  TRANSPORTATION_DOING: ["#D2DAFF", "#0029FF", "กำลังจัดส่ง"],
  TRANSPORTATION_FAILED: ["#FEC7C7", "#FF0000", "จัดส่งไม่สำเร็จ"],
  TRANSPORTATION_FINISHED: ["#C7FEC9", "#09A910", "จัดส่งสำเร็จ"],
};

const CheckStatus = (status) => {
  if (status === 'PRODUCTION_PENDING') {
    return 'รอผลิต'
  }
  if (status === 'PRODUCTION_DOING') {
    return 'กำลังผลิต'
  }
  if (status === 'PRODUCTION_FINISHED') {
    return 'ผลิตเสร็จ'
  }
  if (status === 'TRANSPORTATION_DOING') {
    return 'กำลังจัดส่ง'
  }
  if (status === 'TRANSPORTATION_FINISHED') {
    return 'สำเร็จแล้ว'
  }
  else {
    return 'ไม่สำเร็จ'
  }
}
const styleStatus = (status) => {
  if (status === 'PRODUCTION_PENDING') {
    return { marginLeft: 10, backgroundColor: '#E4E4E4', borderRadius: 18, width: 60, display: "flex", justifyContent: "center" }
  }
  if (status === 'PRODUCTION_DOING') {
    return { marginLeft: 10, backgroundColor: 'rgb(210, 218, 255)', borderRadius: 18, width: 60, display: "flex", justifyContent: "center", color: 'rgb(0, 41, 255)' }
  }
  if (status === 'PRODUCTION_FINISHED') {
    return { marginLeft: 10, backgroundColor: 'rgb(199, 254, 201)', borderRadius: 18, width: 60, display: "flex", justifyContent: "center", color: 'rgb(9, 169, 16)' }
  }
  if (status === 'TRANSPORTATION_DOING') {
    return { marginLeft: 10, backgroundColor: 'rgb(210, 218, 255)', borderRadius: 18, width: 60, display: "flex", justifyContent: "center", color: 'rgb(0, 41, 255)' }
  }
  if (status === 'TRANSPORTATION_FINISHED') {
    return { marginLeft: 10, backgroundColor: 'rgb(199, 254, 201)', borderRadius: 18, width: 60, display: "flex", justifyContent: "center", color: 'rgb(9, 169, 16)' }
  }
  if (status === 'TRANSPORTATION_FAILED') {
    return { marginLeft: 10, backgroundColor: 'rgb(254, 199, 199)', borderRadius: 18, width: 60, display: "flex", justifyContent: "center", color: 'rgb(255, 0, 0)' }
  }
}

const OrderInfo = props => {
  const { id } = props
  const [info, loading] = useGetAPI('fetchOrder', `/order/${id}`)
  return (
    <div style={{ margin: '30px 80px' }}>
      <Row>
        <Breadcrumb>
          <Breadcrumb.Item href="/backoffice/order">
            <HomeOutlined />
            <span>ข้อมูลงาน</span>
          </Breadcrumb.Item>
          <Breadcrumb.Item>รายละเอียดงาน</Breadcrumb.Item>
        </Breadcrumb>
      </Row>
      <div className='flex justify-between'>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <h1 className='title-3'>รายละเอียดงาน</h1>
          <div style={styleStatus(info?.status)}>
            <div style={{ fontSize: 12, fontWeight: 500 }}>
              {CheckStatus(info?.status)}
            </div>
          </div>
        </div>
      </div>
      <Spin spinning={loading !== false}>
        <InfoOrder
          info={info}
        />
      </Spin>
    </div>
  )
}

OrderInfo.propTypes = {
  id: PropTypes.string.isRequired
}

export default OrderInfo
