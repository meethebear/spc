import React, { useCallback, useMemo, useEffect } from "react";
import { Typography, Row, Col, Button, message, Card, Pagination, Spin } from "antd";
import { Field, Form, useForm } from "@/components/form";
import { SearchOutlined, PlusOutlined } from "@ant-design/icons";
import moment from "moment";
import Link from "next/link";
import TaskCard from "../components/TaskCardData";
import { useSelector } from "react-redux";
import { useFetchOrder, useFetchAutoCompleteCustomer } from "../api"
import { STATUS_ORDER } from '@/utils/constant'


const { Text } = Typography;

let timeout
const TaskPage = () => {

  console.log("STATUS_ORDER", STATUS_ORDER);

  const order = useSelector((state) => state.order);
  const search = useMemo(() => order?.order?.filter || {}, [order])

  const [dataOrder, loading, fetchDataOrder] = useFetchOrder();
  const [autoCompleteCustomer, loadingcustomer, fetchautocompletecustomer] = useFetchAutoCompleteCustomer();

  const form = useForm({
    initialValues: {
      date: moment(search.date, 'YYYY-MM-DD'),
      search: search.search,
      status: search.status,
      page: search?.page,
      per_page: search?.page_size,
    },
    rules: {},
    onValuesUpdate: values => {
      if (timeout) clearTimeout(timeout)
      timeout = setTimeout(() => {
        const body = {
          date: moment(values?.date).format('YYYY-MM-DD'),
          search: values?.search,
          status: values?.status,
          customer_id: values?.company_id,
          page: values?.page || search?.page,
          per_page: values?.page_size || search?.page_size,
        };
        console.log("body>>>", body);
        fetchDataOrder(body);
      }, 600)
    }
  })

  useEffect(() => {
    fetchDataOrder({ date: moment().format('YYYY-MM-DD') })
  }, [fetchDataOrder])

  const defaultValue = useCallback(async (body = {}) => {
    await fetchDataOrder({ ...search, ...body })
  }, [fetchDataOrder, search])

  const onChange = (page,per_page) => {
    fetchDataOrder({
      date: search?.date,
      search: search?.search,
      status: search?.status,
      page: page,
      per_page: per_page
    })
  }

  return (
    <div style={{ margin: 40 }}>
      <Row style={{ justifyContent: "space-between", marginBottom: 15 }}>
        {/* <Text style={{ fontSize: 22, fontWieght: 800 }}>รายการงาน</Text> */}
        <h1 className='title-3'>รายการงาน </h1>
        <Link href="/backoffice/order/create">
          <Button style={{ backgroundColor: "#FF900E", borderRadius: 8, color: "#FAFAFA" }} icon={<PlusOutlined />}>
            สร้างงาน
          </Button>
        </Link>
      </Row>
      <Form form={form}>
        <Card style={{ backgroundColor: "#F0F0F0", borderRadius: 8 }}>
          <Row gutter={16} style={{ marginTop: 20 }}>
            <Col xs={24} sm={24} md={12} lg={12} xl={6}>
              <Field.Input
                name="search"
                prefix={<SearchOutlined
                  style={{ color: "#afafaf" }} />}
                placeholder="ค้นหารหัสงาน"
              />
            </Col>
            <Col xs={24} sm={0} md={0} lg={0} xl={6} />
            <Col xs={24} sm={24} md={12} lg={12} xl={4}>
              <Field.Select
                name='company_id'
                placeholder="ชื่อบริษัท"
                options={autoCompleteCustomer?.map(item =>
                ({
                  label: item.customer_company_name,
                  value: item.id
                }))}
                showSearch
                allowClear
              />
            </Col>
            <Col xs={24} sm={24} md={12} lg={12} xl={4}>
              <Field.Select
                name='status'
                placeholder="สถานะทั้งหมด"
                options={STATUS_ORDER}
                allowClear
              />
            </Col>
            <Col xs={24} sm={24} md={12} lg={12} xl={4}>
              <Field.DatePicker
                name='date'
                allowClear={false}
                format="DD/MM/YYYY"
              />
            </Col>
          </Row>
        </Card>
        <TaskCard
          dataTaskCard={dataOrder?.order?.data}
          defaultValue={defaultValue}
          filter={dataOrder?.order?.filter}
          loading={loading}
        />
      </Form>
      <br />
      <Pagination
        current={dataOrder?.order?.data?.pagination?.page || 1}
        pageSize={dataOrder?.order?.data?.pagination?.per_page || 10}
        onChange={onChange}
        total={dataOrder?.order?.data?.pagination?.total}
      />
    </div>
  );
};

export default TaskPage;