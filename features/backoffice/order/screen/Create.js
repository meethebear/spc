import FormOrder from '../components/FormOrder'
import useGetUserTransaction from '@/api/master/useGetUserTransaction'
import useGetCustomer from '@/api/master/useGetCustomer'
import { useCallback, useState } from 'react'
import { Typography, Row, Card, Col, Divider, Button, Breadcrumb, Radio } from "antd"
import { CaretDownOutlined, HomeOutlined } from "@ant-design/icons";

const UserManageCreate = () => {
  const [userTransaction, loadingUserTransaction] = useGetUserTransaction()
  const [customer, loadingCustomer] = useGetCustomer()
  const [isReloading, setIsReloading] = useState(false)

  const reloadForm = useCallback(() => {
    setIsReloading(true)
    setTimeout(() => {
      setIsReloading(false)
    }, 300)
  }, [setIsReloading])

  return (
    <div style={{ margin: '30px 80px' }}>
      <Row>
        <Breadcrumb>
          <Breadcrumb.Item href="/backoffice/order">
            <HomeOutlined />
            <span>ข้อมูลงาน</span>
          </Breadcrumb.Item>
          <Breadcrumb.Item>สร้างงาน</Breadcrumb.Item>
        </Breadcrumb>
      </Row>
      <div className='flex justify-between'>
        <h1 className='title-3'>สร้างงาน</h1>
      </div>
      {!isReloading
        ? (
          <FormOrder
            init={null}
            optionsUserTransportation={userTransaction}
            loadingOptionsUserTransportation={loadingUserTransaction}
            optionsCustomer={customer}
            loadingOptionsCustomer={loadingCustomer}
            reloadForm={reloadForm}
          />
        )
        : (
          <div className='bg-card-form' style={{ maxWidth: '1125px', height: 760 }}>

          </div>
        )}
    </div>
  )
}

export default UserManageCreate