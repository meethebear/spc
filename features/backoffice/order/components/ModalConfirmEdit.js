import { Typography, Modal, Button } from "antd";
import { InfoCircleOutlined, WarningOutlined } from "@ant-design/icons";
import { useState } from "react"
import { useRouter } from "next/router";
import PropTypes from 'prop-types'

const { Text } = Typography

const ConfirmEdit = (props) => {

  const { visible, closeModal } = props

  const router = useRouter();
  const { query } = router

  const handleOk = () => {
    router.push(`/backoffice/order/edit?id=${query.id}`)
    console.log("OK");
  };

  return (
    <Modal
      visible={visible}
      onOk={handleOk}
      onCancel={closeModal}

      footer={[
        <Button
          type="default"
          key="reset"
          htmlType="reset"
          onClick={closeModal}
          style={{ borderRadius: 8, marginRight: 30, width: 100 }}
        >
          ยกเลิก
        </Button>,
        <Button
          key="submit"
          htmlType="submit"
          onClick={handleOk}
          style={{ backgroundColor: "#FF8E0DE5", borderRadius: 8, marginRight: 100, color: "#FAFAFA" }}
        >
          ใช่ แก้ไขข้อมูล
        </Button>,
      ]}>
      <div style={{ display: "flex", justifyContent: "center", flexDirection: "column" }}>
        <WarningOutlined style={{ color: "#FF7A00", fontSize: 90 }} width="50" />
        <Text style={{ display: "flex", justifyContent: "center", marginTop: 10, marginBottom: 10, fontWeight: 600, fontSize: 16 }}>ยืนยันการแก้ไขข้อมูล</Text>
        <Text type="secondary" style={{ display: "flex", width: "90%", margin: "auto", textAlign: "center" }}>
          การแก้ไขข้อมูลในรายการงานจะเป็นย้อนสถานะกลับ
          มาที่ “รอผลิต” อีกครั้ง คุณต้องการดำเนินการต่อใช่หรือไม่
        </Text>
      </div>
    </Modal>
  )
}
ConfirmEdit.propTypes = {
  visible: PropTypes.bool,
  closeModal: PropTypes.func
}
export default ConfirmEdit;