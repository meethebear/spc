import { Button, Card, Col, Row, Typography, Dropdown, Menu, Pagination, Modal } from "antd";
import { useState, useCallback, useEffect, useMemo } from "react";
import Image from "next/image";
import { MenuOutlined, MoreOutlined, InfoCircleOutlined } from "@ant-design/icons";
import Link from "next/link";
import TableCardOrder from "./TableCardOrder";
import { useDeleteOrder, useFetchOrder } from "../api"
import stf from '@/utils/stringFormat'
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import PropTypes from 'prop-types'
import styles from '../styles/task.module.css'
import QueueAnim from 'rc-queue-anim';


const { Text } = Typography;
const { SubMenu } = Menu
const { confirm } = Modal;

const TaskCardData = (props) => {

  const { dataTaskCard, defaultValue, loading } = props

  const [dataOrder, loadingOrder, fetchDataOrder] = useFetchOrder();

  const [loadingDelete, deleteOrder] = useDeleteOrder();

  const router = useRouter();

  const order = useSelector((state) => state.order);
  const search = useMemo(() => order?.order?.filter || {}, [order])

  const confirmDelete = useCallback(
    (item) => {
      const body = {
        id: item
      }
      confirm({
        title: "คุณแน่ใจใ่ช่ไหม ที่จะลบข้อมูลดังกล่าว ?",
        icon: <InfoCircleOutlined style={{ color: "#EE2524" }} />,
        content: `ข้อมูลนี้จะถูกลบโดยทันที และจะไม่สามารถนำกลับมาได้`,
        okText: "ลบข้อมูล",
        okType: "danger",
        cancelText: "ยกเลิก",

        async onOk() {
          const { success } = await deleteOrder(body);
          console.log("success", success);
          if (success) {
            await defaultValue();
          }
        }
      },
      );
    }, [deleteOrder, defaultValue]
  );

  const editById = (id) => {
    console.log(id);
    router.push(`/backoffice/order/edit?id=${id}`)
  }

  const Detail = (id) => {
    router.push(`/backoffice/order/${id}`)
  }

  const CheckStatus = (status) => {
    if (status === 'PRODUCTION_PENDING') {
      return 'รอผลิต'
    }
    if (status === 'PRODUCTION_DOING') {
      return 'กำลังผลิต'
    }
    if (status === 'PRODUCTION_FINISHED') {
      return 'ผลิตเสร็จ'
    }
    if (status === 'TRANSPORTATION_DOING') {
      return 'กำลังจัดส่ง'
    }
    if (status === 'TRANSPORTATION_FINISHED') {
      return 'สำเร็จแล้ว'
    }
    else {
      return 'ไม่สำเร็จ'
    }
  }

  const styleStatus = (status) => {
    if (status === 'PRODUCTION_PENDING') {
      return { marginLeft: 10, backgroundColor: '#E4E4E4', borderRadius: 18, width: 60, display: "flex", justifyContent: "center" }
    }
    if (status === 'PRODUCTION_DOING') {
      return { marginLeft: 10, backgroundColor: 'rgb(210, 218, 255)', borderRadius: 18, width: 60, display: "flex", justifyContent: "center", color: 'rgb(0, 41, 255)' }
    }
    if (status === 'PRODUCTION_FINISHED') {
      return { marginLeft: 10, backgroundColor: 'rgb(199, 254, 201)', borderRadius: 18, width: 60, display: "flex", justifyContent: "center", color: 'rgb(9, 169, 16)' }
    }
    if (status === 'TRANSPORTATION_DOING') {
      return { marginLeft: 10, backgroundColor: 'rgb(210, 218, 255)', borderRadius: 18, width: 60, display: "flex", justifyContent: "center", color: 'rgb(0, 41, 255)' }
    }
    if (status === 'TRANSPORTATION_FINISHED') {
      return { marginLeft: 10, backgroundColor: 'rgb(199, 254, 201)', borderRadius: 18, width: 60, display: "flex", justifyContent: "center", color: 'rgb(9, 169, 16)' }
    }
    if (status === 'TRANSPORTATION_FAILED') {
      return { marginLeft: 10, backgroundColor: 'rgb(254, 199, 199)', borderRadius: 18, width: 60, display: "flex", justifyContent: "center", color: 'rgb(255, 0, 0)' }
    }
  }

  const styleFont = (status) => {
    if (status === 'PRODUCTION_PENDING') {
      return { fontSize: 12, fontWeight: 500, color: 'rgb(32, 32, 32)' }
    }
    if (status === 'PRODUCTION_DOING') {
      return { fontSize: 12, fontWeight: 500, color: 'rgb(0, 41, 255)' }
    }
    if (status === 'PRODUCTION_FINISHED') {
      return { fontSize: 12, fontWeight: 500, color: 'rgb(9, 169, 16)' }
    }
    if (status === 'TRANSPORTATION_DOING') {
      return { fontSize: 12, fontWeight: 500, color: 'rgb(0, 41, 255)' }
    }
    if (status === 'TRANSPORTATION_FINISHED') {
      return { fontSize: 12, fontWeight: 500, color: 'rgb(9, 169, 16)' }
    }
    if (status === 'TRANSPORTATION_FAILED') {
      return { fontSize: 12, fontWeight: 500, color: 'rgb(255, 0, 0)' }
    }
  }

  const menu = (item) => (
    <Menu>
      <>
        <Menu.Item onClick={() => { editById(item.id) }}>
          แก้ไขงาน
        </Menu.Item>
        <Menu.Item onClick={() => { confirmDelete(item.id) }}>
          ลบงาน
        </Menu.Item>
      </>
    </Menu>
  );

  return (
    <>
      {dataTaskCard?.rows?.map((item, index) => {
        return (
          <>
            <div>
              <QueueAnim delay={500} duration={1000}
                animConfig={[
                  { opacity: [1, 0], translateY: [0, 50] },
                  { opacity: [1, 0], translateY: [0, -50] }
                ]}>
                <Card style={{ marginTop: 24, borderRadius: 8 }} key={item?.id}>
                  <Row gutter={16} style={{ marginBottom: 16, justifyContent: "space-between" }}>
                    <div style={{ display: "flex" }}>
                      <div style={{ backgroundColor: "#F0F0F0", display: "flex", padding: 8, borderRadius: 12, }}>
                        <Image
                          src="/images/icon_box.svg"
                          alt="photon-express"
                          width={25}
                          height={24}
                        />
                      </div>
                      <div style={{ flexDirection: "column", paddingLeft: 16 }}>
                        <Row xs={24} sm={24} md={12} lg={12} xl={24}>
                          <Col>
                            <Text>{item.customer.customer_company_name}</Text>
                          </Col>
                        </Row>
                        <div>
                          <Row xs={24} sm={24} md={12} lg={12} xl={24}>
                            <Col><Text type="secondary" style={{ fontSize: 16 }}>รหัสงาน</Text></Col>
                            <Col>
                              <Text style={{ marginLeft: 6 }}>{item.code}</Text>
                            </Col>
                            <Col><Text style={{ marginLeft: 8 }}>(ส่งวันที่ {stf(item.date_to_transportation).dateTH()})</Text></Col>
                          </Row>
                        </div>
                      </div>

                    </div>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <div style={styleStatus(item.status)}>
                        <div><Text style={styleFont(item.status)}>{CheckStatus(item.status)}</Text></div>
                      </div>
                      <div>
                        <Button type='primary' style={{ borderRadius: 6, marginRight: 16, marginLeft: 16 }} icon={<MenuOutlined />} onClick={() => Detail(item.id)}>รายละเอียด</Button>
                      </div>
                      <Dropdown overlay={menu(item)} disabled={
                        item.status === 'PRODUCTION_PENDING' || item.status === 'PRODUCTION_DOING' || item?.status === 'PRODUCTION_FINISHED' ? false : true
                      }
                      >
                        <Button icon={<MoreOutlined />} />
                      </Dropdown>
                    </div>
                  </Row>
                  <TableCardOrder dataTable={item} loading={loading} defaultValue={defaultValue} />
                </Card>
              </QueueAnim>
            </div>
          </>
        )
      })}
    </>

  );
}
TaskCardData.propTypes = {
  dataTaskCard: PropTypes.array.isRequired,
  defaultValue: PropTypes.func,
  loading: PropTypes.bool
}

export default TaskCardData;