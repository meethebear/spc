import { message, Typography, Row, Col } from "antd";
import PropTypes from 'prop-types'
import stf from '@/utils/stringFormat'
import Table from '@/components/Table'
import LabelBoxInfo from '../components/LabelBoxInfo'
import Button from '@/components/Button'
import { useState } from "react";
import ConfirmEdit from "./ModalConfirmEdit";
import { WarningFilled } from '@ant-design/icons';
import useGetUserTransaction from '@/api/master/useGetUserTransaction'

const { Link } = Typography

const Text = props => {
  const { text } = props
  if (text || text === 0) {
    return text
  }

  return '-'
}

const InfoOrder = props => {
  const { info } = props

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [userTransaction, loadingUserTransaction] = useGetUserTransaction();

  console.log('info>>>', info);

  const columns = [
    {
      title: 'ชื่อสินค้า',
      dataIndex: 'name',
      key: 'name',
      width: '60%'
    },
    {
      title: 'จำนวนที่สั่ง',
      dataIndex: 'quantity',
      key: 'quantity',
      align: 'center',
    },
    {
      title: 'จำนวนที่ส่ง',
      dataIndex: 'actual_quantity',
      key: 'actual_quantity',
      align: 'center',
      render: (text, record, index) => {
        if (record?.quantity === record?.actual_quantity) {
          return record?.actual_quantity
        } else {
          return (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <div>
                <WarningFilled style={{ color: '#FF7A00' }} />
              </div>
              <div style={{ marginLeft: 10 }}>
                {record?.actual_quantity}
              </div>
            </div>
          )
        }
      }
    },
    {
      title: 'หน่วย',
      dataIndex: 'unit',
      key: 'unit',
      align: 'center',
    },
    {
      title: 'ราคา',
      dataIndex: 'amount',
      key: 'amount',
      align: 'center',
      render: (value) => stf(value).normal(2) + '฿'
    },
  ]

  const closeModal = () => {
    setIsModalVisible(false)
  }

  const price = info?.products?.map((item) => {
    return item?.quantity * item?.amount
  }
  )
  const actual_price = info?.products?.map((item) => {
    return item?.actual_quantity * item?.amount
  }
  )

  const total = price?.reduce((x, y) => x + y, 0)
  const actual_total = actual_price?.reduce((x, y) => x + y, 0)

  const assigned = info?.assigned_transportation?.map((items) => (
    userTransaction?.find(item => item.id === items)
  ))
  const TranSportation = (item, index) => {
    const lastindexs = assigned?.length
    const lastIndex = lastindexs - 1
    if (lastIndex === index) {
      return item?.full_name
    } else {
      return item?.full_name + ','
    }

  }
  return (
    <>
      <div className='bg-card-form'>
        <div className='flex justify-between'>
          <div>
            <h2 className='mb-0'>รหัสงาน </h2>
            <p className='text-desc-form mb-10'><Text text={info?.code} /></p>
          </div>
          <div>
            <h2 className='mb-0'>แก้ไขข้อมูลล่าสุด</h2>
            <p className='text-desc-form mb-10'><Text text={stf(info?.updated_at || info?.created_at).date('DD/MM/YYYY HH:mm')} /></p>
          </div>
        </div>
        <hr />
        <div>
          <h2>ข้อมูลลูกค้าและวันที่จัดส่ง</h2>
        </div>
        <div className='flex column gap1' style={{}}>
          <LabelBoxInfo
            label='วันที่จัดส่ง'
            value={<Text text={stf(info?.date_to_transportation).date()} />}
          />
          <LabelBoxInfo
            label='ชื่อบริษัท'
            value={<Text text={info?.customer?.customer_company_name} />}
          />
          <LabelBoxInfo
            label='ผู้ติดต่อ'
            value={<Text text={info?.customer?.contact_name} />}
          />
          <LabelBoxInfo
            label='ที่อยู่ลูกค้า'
            value={<Text text={`${info?.address || ''} ${info?.sub_district || ''} ${info?.district || ''} ${info?.province || ''} ${info?.post_code || ''}`} />}
          />
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <span style={{ fontSize: 16, color: '#808080' }}>Lat/Long:</span>
            <div style={{ marginLeft: 55 }}>
              {info?.latitude && info?.longitude ?
                <a href={`https://www.google.com/maps/search/?api=1&query=${info.latitude},${info.longitude}`} rel="noreferrer" target='_blank'>
                  <Button type="primary">เปิดแผนที่</Button>
                </a> : '-'
              }
            </div>
          </div>
          <LabelBoxInfo
            label='หมายเหตุ'
            value={<Text text={info?.remark_to_transportation} />}
          />
        </div>
        <div style={{ marginTop: '1rem' }}>
          <h2>ผู้รับผิดชอบ</h2>
        </div>
        <Row gutter={[16]} style={{ marginBottom: 10 }}>
          <Col xs={24} sm={24} md={24} lg={12} xl={10}>
            <LabelBoxInfo
              label='เจ้าหน้าที่ฝ่ายผลิต'
              value={<Text text={info?.updated_status?.PRODUCTION_DOING?.by_info?.full_name} />}
              spanLabel='span-2'
              spanValue='span-3'
              contentGrid="flex wrap"
            />
          </Col>
          <Col xs={24} sm={24} md={24} lg={6} xl={8}>
            <LabelBoxInfo
              label='เริ่มดำเนินการ'
              value={<Text text={stf(info?.updated_status?.PRODUCTION_DOING.at).date('HH:mm')} />}
              spanLabel='span-'
              spanValue='span-1'
              contentGrid="flex wrap"
            />
          </Col>
          <Col xs={24} sm={24} md={12} lg={6} xl={4}>
            <LabelBoxInfo
              label='เสร็จสิ้น'
              value={<Text text={stf(info?.updated_status?.PRODUCTION_FINISHED.at).date('HH:mm')} />}
              spanLabel='span-1'
              spanValue='span-1'
              contentGrid="flex wrap"
            />
          </Col>
        </Row>
        <Row style={{ marginBottom: 10 }}>
          <div className="span-6">
            <LabelBoxInfo
              label='หมายเหตุจากฝ่ายผลิต'
              value={<Text text={info?.remark_from_production} />}
              spanLabel='span-2'
              spanValue='span-3'
              contentGrid="flex wrap"
            />
          </div>
        </Row>
        <Row gutter={16} style={{ marginBottom: 10 }}>
          <Col xs={24} sm={24} md={24} lg={12} xl={10}>
            <LabelBoxInfo
              label='เจ้าหน้าที่ฝ่ายจัดส่ง'
              value={<Text text={
                info?.updated_status?.TRANSPORTATION_DOING?.by_info?.full_name ? info?.updated_status?.TRANSPORTATION_DOING?.by_info?.full_name :
                  assigned?.map((item, index) => <span key={index} style={{ marginRight: 5, fontSize: 14 }}>{TranSportation(item, index)}</span>)
              } />}
              spanLabel='span-2'
              spanValue='span-3'
              contentGrid="flex wrap"
            />
          </Col>
          <Col xs={24} sm={24} md={24} lg={6} xl={8}>
            <LabelBoxInfo
              label='เริ่มดำเนินการ'
              value={<Text text={stf(info?.updated_status?.TRANSPORTATION_DOING.at).date('HH:mm')} />}
              spanLabel='span-1'
              spanValue='span-1'
              contentGrid="flex wrap"
            />
          </Col>
          <Col xs={24} sm={24} md={24} lg={6} xl={4}>
            <LabelBoxInfo
              label='เสร็จสิ้น'
              value={<Text text={stf(info?.updated_status?.TRANSPORTATION_FAILED.at || info?.updated_status?.TRANSPORTATION_FINISHED.at).date('HH:mm')} />}
              spanLabel='span-1'
              spanValue='span-1'
              contentGrid="flex wrap"
            />
          </Col>
        </Row>
        <div className="span-6">
          <LabelBoxInfo
            label='หมายเหตุจากฝ่ายส่ง'
            value={<Text text={info?.remark_to_transportation} />}
            spanLabel='span-2'
            spanValue='span-3'
            contentGrid="grid-auto-fill-07"
          />
        </div>
        <br />
        <hr />
        <div style={{ marginTop: '1rem' }}>
          <h2>รายละเอียดสินค้า</h2>
        </div>
        <div>
          <Table
            columns={columns}
            data={info?.products || []}
            pagination={false}
            bordered
          />
        </div>
        <br />
        <hr />
        <div className="flex justify-end" style={{ marginTop: '1rem' }}>
          <div className="flex justify-between align-center" style={{ maxWidth: 300, width: '100%' }}>
            <label className="text-dark-blue">รวมทั้งสิ้น :</label>
            {total !== actual_total ? <div><WarningFilled style={{ color: '#FF7A00' }} /></div> : null}
            <p className='title-3 mb-0' style={{ fontSize: '1.625rem' }}>
              {price === actual_price ? stf(total).normal(2) : stf(actual_total).normal(2)}
            </p>
          </div>
        </div>
        <div className="flex justify-end" style={{ marginTop: '1rem' }}>
          <div className='grid-auto-fill-05 gap1 align-end' style={{ width: '100%', maxWidth: 260 }}>
            <div className='span-2'>
              {info?.status === "PRODUCTION_PENDING" || info?.status === "PRODUCTION_DOING" || info?.status === 'PRODUCTION_FINISHED' ? <Button htmlType='button' type='default' className='w-100' onClick={() => setIsModalVisible(true)}>
                แก้ไขข้อมูล
              </Button> : null}
            </div>
            <div className='span-2'>
              <Link href="/backoffice/order">
                <Button htmlType='submit' className='w-100'>
                  หน้าแรก
                </Button>
              </Link>
            </div>
          </div>
        </div>
      </div>
      <ConfirmEdit visible={isModalVisible} closeModal={closeModal} />
    </>
  )
}

Text.propTypes = {
  text: PropTypes.string
}

InfoOrder.propTypes = {
  info: PropTypes.object
}

export default InfoOrder