import React from "react";
import { Typography, Table, Pagination } from "antd";
import PropTypes from 'prop-types'
import stf from '@/utils/stringFormat'

const { Text } = Typography
const TableCardOrder = (props) => {

  const { dataTable } = props

  const columns = [

    {
      title: 'รายการสินค้า',
      dataIndex: "name",
    },
    {
      title: 'จำนวน',
      dataIndex: "quantity",
      width: '15%',
      align: "center"
    },
    {
      title: 'หน่วย',
      dataIndex: "unit",
      width: '15%',
      align: "center"
    },
    {
      title: 'ราคา',
      dataIndex: "amount",
      width: '15%',
      align: "center"
    },
  ]

  return (
    <Table
      dataSource={dataTable?.products}
      columns={columns}
      pagination={false}
    />
  );

}
TableCardOrder.propTypes = {
  dataTable: PropTypes.array.isRequired,
}
export default TableCardOrder