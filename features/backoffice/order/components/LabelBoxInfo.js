import PropTypes from 'prop-types'
import styles from '../styles/LabelBoxInfo.module.css'

const LabelBoxInfo = props => {
  const { label, value, spanLabel, spanValue, contentGrid } = props
  return (
    <div className={`${contentGrid} gap1`}>
      <label className={`${styles.label} ${spanLabel}`}>{label}:</label>
      <div className={spanValue}>
      <span className={styles.value} title={value?.props?.text || ''}>{value}</span>
      </div>
    </div>
  )
}

LabelBoxInfo.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.node,
  spanLabel: PropTypes.string,
  spanValue: PropTypes.string,
  contentGrid: PropTypes.string
}

LabelBoxInfo.defaultProps = {
  spanLabel: '',
  spanValue: 'span-6',
  contentGrid: 'grid-auto-fill-1'
}

export default LabelBoxInfo