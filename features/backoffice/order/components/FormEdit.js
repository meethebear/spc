import { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { useRouter } from 'next/router'
import { FormList, useFormList, Field, getMessageReq } from '@/components/form'
import Button from '@/components/Button'
import { PlusOutlined, CloseOutlined, InfoCircleOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types'
import usePutAPI from '@/utils/hook/usePutAPI'
import moment from '@/utils/momentTH'
import { Divider, message, Col, Row } from 'antd';

const FormOrderEdit = props => {
  const { init,
    optionsCustomer, loadingOptionsCustomer,
    optionsUserTransportation, loadingOptionsUserTransportation,
    reloadForm, dataEdit
  } = props

  
  const { replace } = useRouter()
  const { query } = useRouter()
  const actionSaving = useRef()

  const [dateNow, setDateNow] = useState(moment().format('DD/MM/YYYY HH:mm'))

  const form = useFormList({
    initialValues: ({
      date_to_transportation: moment(dataEdit?.data?.date_to_transportation),
      customer_id: dataEdit?.data?.customer?.id,
      customer_address_id: dataEdit?.data?.customer_address_id,
      contact: dataEdit?.data?.customer?.contact_name,
      users_transportation: dataEdit?.data?.assigned_transportation || [],
      remark_to_transportation: dataEdit?.data?.remark_to_transportation,
    }),
    rules: {
      date_to_transportation: {
        required: getMessageReq('Date to transportation', 'วันที่จัดส่งสินค้า', 'th')
      },
      customer_id: {
        required: getMessageReq('Customer', 'ชื่อบริษัท', 'th')
      },
      customer_address_id: {
        required: getMessageReq('Customer Address', 'ที่อยู่จัดส่ง', 'th')
      },
    }
  }, {
    initialValues: getInitList(dataEdit)
  })


  const { listCtl, handlerChange, values } = form

  const optionsAddress = useMemo(() => {
    if (!values.customer_id) return []
    if (!optionsCustomer?.length) return []
    const customer = optionsCustomer.find(item => item.id === values.customer_id)
    return customer?.addresses.map((item, index) => ({
      ...item,
      label: `ที่อยู่ ${index + 1} ${item.address} ${item.sub_district} ${item.district} ${item.province} ${item.post_code} `
    }))

  }, [optionsCustomer, values.customer_id])

  const [apiEdit] = usePutAPI('updateOrder', '/order')

  const buildBody = useCallback((values, next) => {
    const DataCustomer = optionsCustomer.find(item => item.customer_company_name === values.customer_id)
    const DataCustomerAddress = DataCustomer?.addresses?.map(item => item.id)
    const strCustomerAddress = DataCustomerAddress?.toString()
    const prevProducts = init?.products || []
    const newProduct = []
    prevProducts.forEach(item => {
      const nw = values.products.find(n => item.id === n.id)
      if (!nw) {
        newProduct.push({
          amount: item.amount ? Number(item.amount) : null,
          is_delete: true,
          name: item.name,
          quantity: Number(item.quantity),
          type_save: "DELETE",
          unit: item.unit,
        })

        return
      }

      if (nw) {
        if (
          nw.amount != item.amount ||
          nw.name != item.name ||
          nw.quantity != item.quantity
        ) {
          newProduct.push({
            amount: item.amount ? Number(item.amount) : null,
            is_delete: false,
            name: item.name,
            quantity: Number(item.quantity),
            type_save: "UPDATE",
            unit: item.unit,
          })
        }
      }
    });

    values.products.forEach(item => {
      if (!item.id) {
        newProduct.push({
          amount: item.amount ? Number(item.amount) : null,
          is_delete: false,
          name: item.name,
          quantity: Number(item.quantity),
          type_save: "NEW",
          unit: item.unit,
        })
      }
    })

    const body = {
      assigned_transportation: values?.users_transportation,
      id: query?.id,
      customer_address_id: strCustomerAddress || values.customer_address_id,
      customer_id: DataCustomer?.id || values.customer_id,
      date_to_transportation: values.date_to_transportation ? moment(values.date_to_transportation).format('YYYY-MM-DD') : null,
      products: newProduct,
      remark_to_transportation: values.remark_to_transportation,
    }

    console.log("body", body);

    next(body, values)
  }, [init])

  const handlerSubmit = useCallback(async (body) => {
    const { success } = await apiEdit(body)
    if (success) {
      if (actionSaving.current === 'NEW') {
        reloadForm()
        message.success('งานถูกสร้างขึ้นเรียบร้อย')
      } else {
        message.success('งานถูกสร้างขึ้นเรียบร้อย')
        replace(`/backoffice/order/${query.id}?save=success`)
      }

    }
  },
  [apiEdit, actionSaving, reloadForm]
  )

  const onChangeCustomer = useCallback((name, value) => {
    const customer = optionsCustomer.find(item => item.id === value)
    handlerChange({
      [name]: value,
      contact: customer.contact_name,
      customer_address_id: null
    })
  }, [handlerChange, optionsCustomer])

  const onClickAddItem = useCallback(() => {
    listCtl.addListItem({
      values: {
        amount: '',
        name: '',
        type_save: '',
        unit: '',
      },
      rules: RUlE_ITEM,
      errors: {}
    })
  }, [listCtl.addListItem])

  const onChangeItem = useCallback((index, name, value) => {
    listCtl.onChange(index, name, value)
  }, [listCtl.onChange])

  const onChangeAmount = useCallback(
    async (index, name, value) => {
      const valueAmount = value.replace(/(?!-)[^0-9.]/g, "")
      const prev = listCtl.values[index]
      listCtl.changeListItem(index, {
        ...prev,
        values: {
          ...prev.values,
          amount: valueAmount
        }
      })
    }, [listCtl.changeListItem, listCtl.values]
  )

  const removeProduct = useCallback((index) => {
    listCtl.removeListItem(index)
  }, []);

  useEffect(() => {
    setTimeout(() => {
      setDateNow(moment().format('DD/MM/YYYY HH:mm'))
    }, 1000 * 60)
  }, [])

  const disabledDate = (current) => {
    return current && current < moment().endOf('day');
  };

  return (
    <div className='bg-card-form'>
      <FormList form={form} handlerSubmit={[buildBody, handlerSubmit]} listCtls={{ products: listCtl }}>
        <div className='flex justify-between'>
          <div>
            <h2 className='mb-0'>รหัสงาน <InfoCircleOutlined /></h2>
            <p className='text-desc-form mb-10'>กรุณาระบุข้อมูลด้านล่างเพื่อดำเนินการต่อ</p>
          </div>
          <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <h2 className='mb-0'>วันที่สร้าง</h2>
            <p className='text-desc-form mb-10'>{dateNow}</p>
          </div>
        </div>
        <hr />
        <div>
          <h2>ข้อมูลลูกค้าและวันที่จัดส่ง</h2>
        </div>

        <div className='grid-auto-fill-2 gap1' style={{ marginTop: '1rem' }}>
          <div className='span-1'>
            <Field.DatePicker
              label='วันที่จัดส่งสินค้า'
              name='date_to_transportation'
              disabledDate={disabledDate}
            />
          </div>
          <div className='span-1'>
            <Field.Select
              label='ชื่อบริษัท'
              name='customer_id'
              placeholder='ค้นหาชื่อบริษัท...'
              options={optionsCustomer}
              loading={loadingOptionsCustomer}
              keys={['id', 'customer_company_name']}
              onChange={onChangeCustomer}
            />
          </div>
          <div className='span-1'>
            <Field.Select
              label='ที่อยู่จัดส่ง'
              name='customer_address_id'
              placeholder='เลือกที่อยู่...'
              options={optionsAddress}
              keys={['id', 'label']}
            />
          </div>
          <div className='span-1'>
            <Field.Input
              label='ชื่อผู้ติดต่อ'
              name='contact'
              disabled
            />
          </div>
        </div>
        <hr />
        <div>
          <h2>ข้อมูลฝ่ายจัดส่ง</h2>
        </div>
        <div className='grid-auto-fill-2 gap1' style={{ marginTop: '1rem' }}>
          <div className='span-2'>
            <Field.TextArea
              label='รายละเอียดการจัดสินค้า'
              name='remark_to_transportation'
            />
          </div>
        </div>
        <hr />
        <div className='flex gap1 align-center' style={{ marginTop: '1rem', marginBottom: '1rem' }}>
          <h2 className='mb-0'>รายละเอียดสินค้า</h2>
          <Button className='btn-orange' icon={<PlusOutlined />} onClick={onClickAddItem}>
            เพิ่มสินค้า
          </Button>
        </div>
        {listCtl.values.map((item, index) => (
          <>
            <Divider />
            <div className='bg-card-item' key={index}>
            <Row gutter={16}>
              <Col xs={10} sm={10} md={5} lg={6} xl={6}>
                <Field.Input
                  label='ชื่อสินค้า'
                  name='name'
                  id={`name${index}`}
                  value={item.values.name}
                  required={!!item.rules.name.required}
                  error={item.errors.name}
                  onChange={(name, value) => onChangeItem(index, name, value)}
                />
                </Col>
                <Col xs={10} sm={10} md={5} lg={5} xl={5}>
                <Field.Number
                  label='จำนวน'
                  name='quantity'
                  id={`quantity${index}`}
                  value={item.values.quantity}
                  required={!!item.rules.quantity.required}
                  error={item.errors.quantity}
                  onChange={(name, value) => onChangeItem(index, name, value)}
                />
                </Col>
                <Col xs={10} sm={10} md={5} lg={5} xl={5}>
                <Field.Input
                  label='หน่วย'
                  name='unit'
                  id={`unit${index}`}
                  value={item.values.unit}
                  required={!!item.rules.unit.required}
                  error={item.errors.unit}
                  onChange={(name, value) => onChangeItem(index, name, value)}
                />
                </Col>
              <Col xs={10} sm={10} md={5} lg={5} xl={5}>
                <Field.Input
                  label='ราคา'
                  name='amount'
                  id={`amount${index}`}
                  value={item.values.amount}
                  required={!!item.rules.amount?.required}
                  error={item.errors.amount}
                  onChange={(name, value) => onChangeAmount(index, name, value)}
                />
              </Col>
              <div style={{ alignItems: "center", display: "flex", marginLeft: "auto" }}>
                <Button onClick={() => removeProduct(index)} ghost icon={<CloseOutlined />} type='default'>
                </Button>
              </div>
              </Row>
                </div>
          </>
        ))}
        <br />
        <hr />
        <br />
        <div className='flex justify-end'>
          <div className='grid-auto-fill-05 gap1 align-end' style={{ width: '100%', maxWidth: 320 }}>
            <div className='span-2'>
              <Button htmlType='button' type='default' className='w-100' onClick={() => replace('/backoffice/order')}>
                ยกเลิก
              </Button>
            </div>
            <div className='span-3'>
              <Button htmlType='submit' className='w-100' onClick={() => actionSaving.current = 'CURRENT'}>
                อัพเดตงาน
              </Button>
            </div>
          </div>
        </div>
      </FormList>
    </div>
  )
}

const initialValues = {
  customer_address_id: "",
  customer_id: "",
  date_to_transportation: moment(),
  products: [],
  users_transportation: [],
  remark_to_transportation: ""
}

const initialList = [
  {
    values: {
      "amount": "",
      "name": "",
      "quantity": null,
      "unit": ""
    },
    rules: {
      quantity: {
        required: getMessageReq('quantity', 'จำนวน', 'th')
      },
      name: {
        required: getMessageReq('name_product', 'ชื่อสินค้า', 'th')
      },
      unit: {
        required: getMessageReq('unit', 'หน่วย', 'th')
      },
    },
    errors: {}
  }
]

const RUlE_ITEM = {
  quantity: {
    required: getMessageReq('quantity', 'จำนวน', 'th')
  },
  name: {
    required: getMessageReq('name_product', 'ชื่อสินค้า', 'th')
  },
  unit: {
    required: getMessageReq('unit', 'หน่วย', 'th')
  },
}

const getInitList = (dataEdit) => {
  if (dataEdit) {
    return dataEdit?.data?.products?.map(item => ({
      values: {
        "amount": item.amount,
        "name": item.name,
        "quantity": item.quantity,
        "unit": item.unit,
      },
      rules: RUlE_ITEM,
      errors: {}
    }))
  }

  return initialList
}

FormOrderEdit.propTypes = {
  init: PropTypes.object,
  optionsCustomer: PropTypes.array,
  loadingOptionsCustomer: PropTypes.bool,
  optionsUserTransportation: PropTypes.array,
  loadingOptionsUserTransportation: PropTypes.bool,
  reloadForm: PropTypes.func,
  dataEdit: PropTypes.object
}

export default FormOrderEdit
