import { Modal, Button, Typography } from "antd"
import { InfoCircleOutlined, WarningOutlined } from "@ant-design/icons";
import PropTypes from 'prop-types'
import Image from "next/image";
import { useState } from "react";
import ModalSuccess from './ModalSuccess'


const { Text } = Typography

const DeliveryConfirm = (props) => {
  const { visible, closeModal } = props
  const [Confirm, setConfirm] = useState(false);


  const CloseModal = () => {
    setConfirm(false)
  }

  const handleOk = () => {
    console.log("OK");
    closeModal();
    setConfirm(true)
  };

  return (
    <>
      <Modal
        visible={visible}
        onOk={handleOk}
        onCancel={closeModal}

        footer={[
          <Button
            type="default"
            key="reset"
            htmlType="reset"
            onClick={closeModal}
            style={{ borderRadius: 8, marginRight: 30, width: 100 }}
          >
            ยกเลิก
          </Button>,
          <Button
            key="submit"
            htmlType="submit"
            onClick={handleOk}
            style={{ backgroundColor: "#FF8E0DE5", borderRadius: 8, color: "#FAFAFA", width: 100, marginRight: 125 }}
          >
            ยืนยัน
          </Button>,
        ]}>
        <div style={{ display: "flex", justifyContent: "center", flexDirection: "column" }}>
          <Image
            src="/images/icon_deliverybox.svg"
            alt="photon-express"
            width={80}
            height={80}
          />
          <Text style={{ display: "flex", justifyContent: "center", marginTop: 10, marginBottom: 10, fontWeight: 600, fontSize: 16 }}>ยืนยันการจัดส่ง</Text>
          <Text type="secondary" style={{ display: "flex", width: "90%", margin: "auto", textAlign: "center" }}>
            คุณยืนยันที่จะทำการจัดส่งสำเร็จหรือไม่
            กรุณาตรวจสอบข้อมูลให้เรียบร้อยก่อนการยืนยันดำเนินการ หากดำเนินการแล้ว
            จะไม่สามารถย้อนกลับได้คุณต้องการดำเนินต่อหรือไม่ ?
          </Text>
        </div>
      </Modal>
      <ModalSuccess
        visible={Confirm}
        closeModal={CloseModal}
      />
    </>
  )
}
DeliveryConfirm.propTypes = {
  visible: PropTypes.bool,
  closeModal: PropTypes.func
}
export default DeliveryConfirm