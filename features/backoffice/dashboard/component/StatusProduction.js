import { Row, Collapse, Col, Typography } from "antd";
import styles from "../style/Dashboard.module.scss";
import BussinessPanel from "./BussinessPanel";
import { DownCircleOutlined } from "@ant-design/icons";
import PropTypes from 'prop-types'


const { Panel } = Collapse;
const { Title, Text, Paragraph } = Typography;

const StatusProduction = (props) => {
  const { dataProduction } = props;

  return (
    <div>
      <Row>
        <Col xs={24} md={12} lg={24}>
          <Collapse className={styles.collapse_styles}
            expandIcon={({ isActive }) => (
              <DownCircleOutlined rotate={isActive ? 0 : -90} />
            )}
            bordered={false} defaultActiveKey={[1, 2]}>
            <Panel
              header={`ฝ่ายผลิต : กำลังเตรียมสินค้า (${dataProduction?.PRODUCTION_DOING?.count || 0})`}
              key={1}
            >
              <Row>
                <Col xs={24} md={12} lg={24}>
                  <Row>
                    {dataProduction?.PRODUCTION_DOING?.orders?.map((item, index) => (
                      <div key={index} style={{ marginRight: 20 }}>
                        <BussinessPanel item={item} key={index} />
                      </div>
                    ))}
                  </Row>
                </Col>
              </Row>
            </Panel>
            <Panel
              header={`ฝ่ายผลิต : เตรียมเสร็จแล้ว (${dataProduction?.PRODUCTION_FINISHED?.count || 0})`}
              key={2}
            >
              <Row>
                <Col xs={24} md={12} lg={24}>
                  <Row>
                    {dataProduction?.PRODUCTION_FINISHED?.orders?.map((item, index) => (
                      <div key={index} style={{ marginRight: 20 }}>
                        <BussinessPanel item={item} key={index} />
                      </div>
                    ))}
                  </Row>
                </Col>
              </Row>
            </Panel>
          </Collapse>
        </Col>
      </Row>
    </div>
  );
};

StatusProduction.propTypes = {
  dataProduction: PropTypes.object,
}

export default StatusProduction;
