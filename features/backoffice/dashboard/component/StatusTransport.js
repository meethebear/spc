import { Row, Collapse, Col, Typography } from "antd";
import styles from "../style/Dashboard.module.scss";
import BussinessPanel from "./BussinessPanel";
import { DownCircleOutlined } from "@ant-design/icons";
import PropTypes from 'prop-types'
import CardDataTransport from './CardDataTransport'


const { Panel } = Collapse;
const { Title, Text, Paragraph } = Typography;

const AllStatus = (props) => {
  const { dataTransportation } = props;

  return (
    <div style={{ margin: 30 }}>
      <h1 style={{ fontSize: 26, fontWeight: 500 }}>ภาพรวมรายการขนส่งรายพนักงานประจำวัน</h1>
      <br />
      <CardDataTransport dataTransportations={dataTransportation} />
    </div>
  );
};

AllStatus.propTypes = {
  dataTransportation: PropTypes.object,
}

export default AllStatus;
