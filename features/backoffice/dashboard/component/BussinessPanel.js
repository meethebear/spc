import React, { useCallback } from "react";
import { Typography, Tag, Card, Divider } from "antd";
import style from "../style/Dashboard.module.scss";
import { InfoCircleFilled, WarningOutlined } from "@ant-design/icons";
import { useRouter } from "next/router";
import moment from "moment";

const { Text, Title } = Typography;

const BussinessPanel = (props) => {
  const { type, item } = props;
  const router = useRouter();
  const query = router


  const status_type = {
    PRODUCTION_PENDING: ["#afafaf", "#202020", "รอผลิต"],
    PRODUCTION_DOING: ["#D2DAFF", "#0029FF", "กำลังผลิต"],
    PRODUCTION_FINISHED: ["#C7FEC9", "#09A910", "ผลิตเสร็จ"],
    TRANSPORTATION_DOING: ["#D2DAFF", "#0029FF", "กำลังจัดส่ง"],
    TRANSPORTATION_FAILED: ["#FEC7C7", "#FF0000", "จัดส่งไม่สำเร็จ"],
    TRANSPORTATION_FINISHED: ["#C7FEC9", "#09A910", "จัดส่งสำเร็จ"],
  };

  const onClickDetail = useCallback((id) => {
    router.push(`/backoffice/dashboard/${id}`);
  }, []);

  return (
    <div className={style.card_margin} onClick={() => onClickDetail(item?.id)}>
      <Card className="card_styles">
        <>
          <div className={style.card_space}>
            <div>
              <Tag color="#202020">{item?.code}</Tag>
              <br />
              <Text className={style.card_sub_title}>
                {item?.customer?.customer_company_name}
              </Text>
            </div>
            <div>
              <Tag
                color={status_type[item?.status][0]}
                style={{ color: status_type[item?.status][1]}}
              >
                {status_type[item?.status][2]}
              </Tag>
              <br />
              {!["PRODUCTION_PENDING", "TRANSPORTATION_FAILED"].includes(
                item?.status
              ) && (
                <Text
                  className={style.card_sub_title}
                  style={{ float: "right" }}
                >
                  {moment(item?.updated_status[item?.status]?.at).format('HH:mm')} น.
                </Text>
              )}
            </div>
          </div>
          {item?.status == "TRANSPORTATION_FAILED" && (
            <Text className={style.card_failed} ellipsis>
              <InfoCircleFilled /> {item?.reason_failure}
            </Text>
          )}
        </>
        {item?.status !== "TRANSPORTATION_FAILED" && (
          <>
            <Divider dashed style={{ margin: "10px 0" }} />
            <ul className={style.card_content_list}>
              {item?.products?.map((product,index) => (
                <li key={index}>
                  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{ width:200, maxWidth: "100%" }}>
                  {product?.name}{" "}
                  </div>
                  
                  <span style={{ float: "right" }}>
                    {product?.quantity} {product?.unit}
                  </span>
                  </div>
                </li>
              ))}
            </ul>
          </>
        )}
      </Card>
      {item?.updated_at !== null ?
        <div className={style.card_has_updated}>
          <Text>
            <WarningOutlined /> มีการเปลี่ยนแปลงสินค้า
          </Text>
        </div>
      : null}
    </div>
  );
};

export default BussinessPanel;
