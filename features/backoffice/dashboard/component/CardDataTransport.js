import { Card, Row, Table, Typography } from "antd";
import { UserOutlined, CheckCircleFilled, CloseCircleFilled } from "@ant-design/icons";
import PropTypes from 'prop-types'
import moment from "moment";
import Link from "next/link";

const { Text } = Typography

const CardDataTransport = (props) => {

  const { dataTransportations } = props

  // const [screen, setScreen] = useState({Transportation:true, TransportationById:false});

  // slice # ใน code
  // const code = dataTransportation?.TRANSPORTATION_FAILED?.orders?.map(item => item.code)
  // const sliceCode = code?.map(item => item.slice(1,20))


  const columns = [

    {
      title: 'รหัสงาน',
      dataIndex: "code",
      render: (text, record, index) => (
        <div style={{ color: '#1263FF', textDecoration: 'underline' }}>
          {/* <a>{record?.code}</a> */}
          <Link href={`/backoffice/dashboard/${record?.id}`}>{record?.code}</Link>
        </div>
      )
    },
    {
      title: 'ข้อมูลลูกค้า',
      dataIndex: "customer",
      width: '28%',
      align: "left",
      render: (text, record, index) => (
        <div style={{ display: 'flex' }}>
          <div>{record?.customer?.customer_company_name}</div>
          <div style={{ marginLeft: 5 }}><Text type="secondary">({record?.customer?.contact_name})</Text></div>
        </div>
      )
    },
    {
      title: 'เวลารับงาน',
      dataIndex: "time",
      width: '15%',
      align: "center",
      render: (text, record, index) => (
        <div>
          {record?.start_date ? moment(record?.start_date).format('HH:mm') + ' น.' : '-'}
        </div>
      )
    },
    {
      title: 'ผลการจัดส่ง',
      dataIndex: "result",
      width: '15%',
      align: "center",
      render: (text, record, index) => (
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          {record?.end_date ?
            <div>
              {record?.status === 'TRANSPORTATION_FAILED' ?
                <CloseCircleFilled style={{ color: '#E00000' }} /> :
                <CheckCircleFilled style={{ color: '#09A910' }} />
              }
            </div> : null
          }
          <div style={{ marginLeft: 5 }}>
            {record?.end_date ? moment(record?.end_date).format('HH:mm') + ' น.' : '-'}
          </div>
        </div>
      )
    },
    {
      title: 'หมายเหตุ (ฝ่ายธุรการ)',
      dataIndex: "remark_from_production",
      width: '18%',
      align: "center"
    },
    {
      title: 'หมายเหตุฝ่ายจัดส่ง',
      dataIndex: "remark_to_transportation",
      width: '15%',
      align: "center"
    },
  ]

  const FormatNumberPhone = (item) => {
    const phone = item?.phone
    let match = phone.match(/^(\d{3})(\d{3})(\d{4})$/);
    if (match) {
      return match[1] + '-' + match[2] + '-' + match[3]
    } else {
      return '-'
    }
  }

  console.log("dataTransportations>>",dataTransportations?.map(item => item?.orders));
  return (
    <>
      {dataTransportations?.map((item) => (
        <>
        {/* {item?.order?.map((orders) => ( */}
          {/* orders.length >= 1 ? */}
          <Card style={{ width: '100%', marginBottom: 20, marginTop: 10 }}>
            <Row style={{ alignItems: 'baseline' }}>
              <div>
                <div style={{ borderRadius: '100%', border: 'solid 3px #11377C', width: 30, height: 30, display: 'flex', alignItems: "center", justifyContent: 'center' }}>
                  <div style={{}}>
                    <UserOutlined style={{ color: '#11377C' }} />
                  </div>
                </div>
              </div>
              <div style={{ marginLeft: 10 }}>
                <Text>{item?.full_name}</Text>
              </div>
              <div style={{ marginLeft: 10 }}>
                <Text type="secondary">({FormatNumberPhone(item)})</Text>
              </div>
            </Row>
            <br />
            <Table
              columns={columns}
              dataSource={item?.orders}
              pagination={false}
            />
          </Card> 
          {/* // : null))} */}
        </>
      ))}
    </>
  )
}
CardDataTransport.propTypes = {
  dataTransportations: PropTypes.array,
}
export default CardDataTransport;