import React, { useCallback } from "react";
import { Typography, Tag, Card, Divider } from "antd";
import style from "../style/Dashboard.module.scss";
import { InfoCircleFilled, WarningOutlined } from "@ant-design/icons";
import { useRouter } from "next/router";
import moment from "moment";
import PropTypes from 'prop-types'
import QueueAnim from 'rc-queue-anim';

const { Text, Title } = Typography;

const BussinessPanelAll = (props) => {
  const { item } = props;
  const router = useRouter();

  const status_type = {
    PRODUCTION_PENDING: ["#afafaf", "#202020", "รอผลิต"],
    PRODUCTION_DOING: ["#D2DAFF", "#0029FF", "กำลังผลิต"],
    PRODUCTION_FINISHED: ["#C7FEC9", "#09A910", "ผลิตเสร็จ"],
    TRANSPORTATION_DOING: ["#D2DAFF", "#0029FF", "กำลังจัดส่ง"],
    TRANSPORTATION_FAILED: ["#FEC7C7", "#FF0000", "จัดส่งไม่สำเร็จ"],
    TRANSPORTATION_FINISHED: ["#C7FEC9", "#09A910", "จัดส่งสำเร็จ"],
  };

  const onClickDetail = useCallback((id) => {
    router.push(`/backoffice/dashboard/${id}`);
  }, []);

  return (
    <div className={style.card_margin} onClick={() => onClickDetail(item?.id)}>
      <Card className="card_styles">
        <div >
          <div className={style.card_space_all}>
            <div>
              <Tag color="#202020" className={style.card_code}>{item?.code}</Tag>
              <br />
              <Text className={style.card_sub_title}>
                {item?.customer?.customer_company_name}
              </Text>
            </div>
            <div>
              <Tag
                color={status_type[item?.status][0]}
                style={{ color: status_type[item?.status][1] }}
                className={style.card_code}
              >
                {status_type[item?.status][2]}
              </Tag>
              <br />
              {!["PRODUCTION_PENDING", "TRANSPORTATION_FAILED"].includes(
                item?.status
              ) && (
                  <Text
                    className={style.card_sub_title}
                    style={{ float: "right" }}
                  >
                    {moment(item?.updated_status[item?.status]?.at).format('HH:mm')} น.
                  </Text>
                )}
            </div>
          </div>
          {item?.status == "TRANSPORTATION_FAILED" && (
            <Text className={style.card_failed} ellipsis>
              <InfoCircleFilled /> {item?.reason_failure}
            </Text>
          )}
        </div>
        
        {item?.status !== "TRANSPORTATION_FAILED" && (
          <>
            <Divider dashed style={{ margin: "10px 0" }} />
            <ul className={style.card_content_list}>
              {item?.products?.map((product, index) => (
                <li key={index}>
                  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <div className={style.product_list}>
                      {product?.name}{" "}
                    </div>

                    <span style={{ float: "right" }}>
                      {product?.actual_quantity} {product?.unit}
                    </span>
                  </div>
                </li>
              ))}
            </ul>
          </>
        )}
      </Card>
      
      {item?.updated_at && (
        <div className={style.card_has_updated}>
          <Text>
            <WarningOutlined /> มีการเปลี่ยนแปลงสินค้า
          </Text>
        </div>
      )}
    </div>
  );
};
BussinessPanelAll.propTypes = {
  item: PropTypes.array,
}

export default BussinessPanelAll;
