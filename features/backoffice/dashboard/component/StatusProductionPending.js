import { Row, Collapse, Col, Typography } from "antd";
import styles from "../style/Dashboard.module.scss";
import BussinessPanel from "./BussinessPanel";
import { DownCircleOutlined } from "@ant-design/icons";
import PropTypes from 'prop-types'


const { Panel } = Collapse;
const { Title, Text, Paragraph } = Typography;

const AllStatus = (props) => {
  const { dataProductionPending } = props;
  console.log("dataProductionPending>>>", dataProductionPending?.PRODUCTION_PENDING);

  return (
    <div>
      <Row>
        <Col xs={24} md={12} lg={24}>
          <Collapse
            className={styles.collapse_styles}
            expandIcon={({ isActive }) => (
              <DownCircleOutlined rotate={isActive ? 0 : -90} />
            )}
            bordered={false}
            defaultActiveKey={[1]}
          >
            <Panel header={`ฝ่ายธุรการ (${dataProductionPending?.PRODUCTION_PENDING?.count || 0})`} key={1}>
              <Row>
                <Col xs={24} md={12} lg={20}>
                  <Row>
                    {dataProductionPending?.PRODUCTION_PENDING?.orders?.map((item, index) => (
                      <div key={index} style={{ marginRight: 20 }}>
                        <BussinessPanel item={item} key={index} />
                      </div>
                    ))}
                  </Row>
                </Col>
              </Row>
            </Panel>
          </Collapse>
        </Col>
      </Row>
    </div>
  );
};

AllStatus.propTypes = {
  dataProductionPending: PropTypes.object,
}

export default AllStatus;
