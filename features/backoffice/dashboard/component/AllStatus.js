import { Row, Collapse, Col, Typography, Spin } from "antd";
import styles from "../style/Dashboard.module.scss";
import BussinessPanel from "./BussinessPanelAll";
import { DownCircleOutlined } from "@ant-design/icons";
import PropTypes from 'prop-types'
import style from "../style/Dashboard.module.scss";
import QueueAnim from 'rc-queue-anim';

const { Panel } = Collapse;
const { Title, Text, Paragraph } = Typography;

const AllStatus = (props) => {
  const { data, dataProduction, dataTransportation } = props;

  return (
    <div>
      <Row gutter={[16, 16]}>
        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
          <Collapse
            className={styles.collapse_styles}
            expandIcon={({ isActive }) => (
              <DownCircleOutlined rotate={isActive ? 0 : -90} />
            )}
            bordered={false}
            defaultActiveKey={[1]}
          >
            <Panel header={`ฝ่ายธุรการ (${data?.dashboard?.data?.PRODUCTION_PENDING?.count || 0})`} key={1}>
              <div className={style.collapse_scroll}>
                  {data?.dashboard?.data?.PRODUCTION_PENDING?.orders?.map((item, index) => (
                    <>
                    <QueueAnim delay={1000} duration={1000}>
                      <div key={index}>
                        <BussinessPanel item={item} key={index} />
                      </div>
                    </QueueAnim>
                  </>
                  ))}
              </div>
            </Panel>
          </Collapse>
        </Col>
        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
          <Collapse
            className={styles.collapse_styles}
            expandIcon={({ isActive }) => (
              <DownCircleOutlined rotate={isActive ? 0 : -90} />
            )}
            bordered={false}
            defaultActiveKey={[1, 2]}>

            <Panel header={`ฝ่ายผลิต : กำลังเตรียมสินค้า (${data?.dashboard?.data?.PRODUCTION_DOING?.count || dataProduction?.PRODUCTION_DOING?.count || 0})`}
              key={1}>
              <div className={style.collapse_scroll}>
                {data?.dashboard?.data?.PRODUCTION_DOING?.orders?.map((item, index) => (
                  <>
                    <QueueAnim delay={1000} duration={1000}>
                      <div key={index}>
                        <BussinessPanel item={item} key={index} />
                      </div>
                    </QueueAnim>
                  </>
                ))}
              </div>
            </Panel>

            <Panel
              header={`ฝ่ายผลิต : เตรียมเสร็จแล้ว (${data?.dashboard?.data?.PRODUCTION_FINISHED?.count || dataProduction?.PRODUCTION_FINISHED?.count || 0})`}
              key={2}>
              <div className={style.collapse_scroll}>
                {data?.dashboard?.data?.PRODUCTION_FINISHED?.orders?.map((item, index) => (
                  <>
                    <QueueAnim delay={1000} duration={1000}>
                      <div key={index}>
                        <BussinessPanel item={item} key={index} />
                      </div>
                    </QueueAnim>
                  </>
                )) || dataProduction?.PRODUCTION_FINISHED?.orders?.map((item, index) => (
                  <>
                    <QueueAnim delay={1000} duration={1000}>
                      <div key={index}>
                        <BussinessPanel item={item} key={index} />
                      </div>
                    </QueueAnim>
                  </>
                ))}
            </div>
          </Panel>
        </Collapse>
      </Col>
      <Col xs={26} sm={24} md={8} lg={8} xl={8}>
        <Collapse className={styles.collapse_styles}
          expandIcon={({ isActive }) => (
            <DownCircleOutlined rotate={isActive ? 0 : -90} />
          )}
          bordered={false} defaultActiveKey={[1, 2, 3]}>
          <Panel
            header={`ฝ่ายขนส่ง : จัดส่งไม่สำเร็จ (${data?.dashboard?.data?.TRANSPORTATION_FAILED?.count || dataTransportation?.TRANSPORTATION_FAILED?.count || 0})`}
            key={1}>
            <div className={style.collapse_scroll}>
                {data?.dashboard?.data?.TRANSPORTATION_FAILED?.orders?.map((item, index) => (
                  <>
                  <QueueAnim delay={1000} duration={1000}>
                    <div key={index}>
                      <BussinessPanel item={item} key={index} />
                    </div>
                  </QueueAnim>
                </>
                )) || dataTransportation?.TRANSPORTATION_FAILED?.orders?.map((item, index) => (
                  <>
                    <QueueAnim delay={1000} duration={1000}>
                      <div key={index}>
                        <BussinessPanel item={item} key={index} />
                      </div>
                    </QueueAnim>
                  </>
                ))}
            </div>
          </Panel>


          <Panel header={`ฝ่ายขนส่ง : กำลังจัดส่ง (${data?.dashboard?.data?.TRANSPORTATION_DOING?.count || dataTransportation?.TRANSPORTATION_DOING?.count || 0})`} key={2}>
            <div className={style.collapse_scroll}>
              <QueueAnim delay={1000} duration={1000}>
                {data?.dashboard?.data?.TRANSPORTATION_DOING?.orders?.map((item, index) => (
                  <>
                  <QueueAnim delay={1000} duration={1000}>
                    <div key={index}>
                      <BussinessPanel item={item} key={index} />
                    </div>
                  </QueueAnim>
                </>
                )) || dataTransportation?.TRANSPORTATION_DOING?.orders?.map((item, index) => (
                  <>
                    <QueueAnim delay={1000} duration={1000}>
                      <div key={index}>
                        <BussinessPanel item={item} key={index} />
                      </div>
                    </QueueAnim>
                  </>
                ))}
              </QueueAnim>
            </div>
          </Panel>
          <Panel header={`ฝ่ายขนส่ง : ส่งแล้ว (${data?.dashboard?.data?.TRANSPORTATION_FINISHED?.count || dataTransportation?.TRANSPORTATION_FINISHED?.count || 0})`} key={3}>
            <div className={style.collapse_scroll}>
              <QueueAnim delay={1000} duration={3000}>
                {data?.dashboard?.data?.TRANSPORTATION_FINISHED?.orders?.map((item, index) => (
                  <>
                  <QueueAnim delay={1000} duration={1000}>
                    <div key={index}>
                      <BussinessPanel item={item} key={index} />
                    </div>
                  </QueueAnim>
                </>
                )) || dataTransportation?.TRANSPORTATION_FINISHED?.orders?.map((item, index) => (
                  <>
                    <QueueAnim delay={1000} duration={1000}>
                      <div key={index}>
                        <BussinessPanel item={item} key={index} />
                      </div>
                    </QueueAnim>
                  </>
                ))}
              </QueueAnim>
            </div>
          </Panel>
        </Collapse>
      </Col>
    </Row>
    </div >
  );
};

AllStatus.propTypes = {
  data: PropTypes.array,
  dataProduction: PropTypes.array,
  dataTransportation: PropTypes.array,
}

export default AllStatus;
