import { useState, useEffect, useCallback } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector, useDispatch } from "react-redux";
import { resError, resOK } from "@/utils/response";
import { actionIndexDashboard } from '@/app/actions/dashboard.action';

const useFetchDashboard = () => {
  const name = 'fetchDataDashboard'
  const API = useAPI(name, 'none')
  const { dashboard, tasksRunning } = useSelector(selector(['dashboard', 'tasksRunning']))
  const useThunkDispatch = () => useDispatch()
  const dispatch = useThunkDispatch()

  const fetchDataDashboard = useCallback(async (filter) => {
    try {
      API.begin()
      const { data } = await API.get(`/dashboard`,{ params: { ...filter.body }})
      const res = resOK(data)
      dispatch(actionIndexDashboard({
        filter: { page: res.data.page, page_size: res.data.page_size, filter, }, data: res.data
      }))
    } catch (error) {
      const e = resError(error)
      API.error(e)
    } finally {
      API.end()
    }
  }, [])

  return [dashboard, tasksRunning?.[name]?.loading, fetchDataDashboard]
}

export default useFetchDashboard