import { useState, useEffect, useCallback } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector, useDispatch } from "react-redux";
import { resError, resOK } from "@/utils/response";
import { actionTransportationDashboard } from '@/app/actions/dashboard.action';
import moment from "moment";

const useFetchTransportation = () => {
  const name = 'fetchDataTransportation'
  const API = useAPI(name, 'none')
  const [result, useResult ] = useState();
  const { transportation, tasksRunning } = useSelector(selector(['transportation', 'tasksRunning']))
  const useThunkDispatch = () => useDispatch()
  const dispatch = useThunkDispatch()

  const fetchDataTransportation = useCallback(async (filter = {}) => {
    try {
      API.begin()
      const { data } = await API.get('/dashboard/transportation', { params: { ...filter } })
      const res = resOK(data)
      useResult(res)
      dispatch(actionTransportationDashboard({
        filter: { page: res.data.page, page_size: res.data.page_size, ...filter, }, data: res.data
      }))
    } catch (error) {
      const e = resError(error)
      API.error(e)
    } finally {
      API.end()
    }
  }, [])

  return [result?.data, tasksRunning?.[name]?.loading, fetchDataTransportation]
}

export default useFetchTransportation