import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Typography, Row, Card, Col, Divider, Button, Breadcrumb, Radio } from "antd"
import { CaretDownOutlined, HomeOutlined } from "@ant-design/icons";
import FormEdit from "../component/FormEdit"
///api
import useGetAPI from '@/utils/hook/useGetAPI'
import useGetUserTransaction from '@/api/master/useGetUserTransaction'
import { useFetchOrderById, useFetchAutoCompleteCustomer } from "@/features/backoffice/order/api"
import useGetCustomer from '@/api/master/useGetCustomer'

const TaskEdit = () => {

  const [datacustomerbyid, loadingcustomerbyid, fetchOrderById] = useFetchOrderById();
  const [autocompletecustomer, loadingcustomer, fetchautocompletecustomer] = useFetchAutoCompleteCustomer();
  const [userTransaction, loadingUserTransaction] = useGetUserTransaction();
  const [customer, loadingCustomer] = useGetCustomer();

  const [dataEdit, setDataEdit] = useState(null);

  const router = useRouter();
  const { query } = router

  useEffect(()=>{
    const fetchOrderEdit = async(id)=>{
     const res = await fetchOrderById(id);
     console.log("res",res);
     setDataEdit(res);
    }

    if(query?.id) {
     fetchOrderEdit(query?.id)
    }
  },[query])
  

  return (
    <div style={{ margin: '30px 80px' }}>
      <Row>
          <Breadcrumb>
            <Breadcrumb.Item href="/backoffice/dashboard">
              <HomeOutlined />
              <span>ข้อมูลงาน</span>
            </Breadcrumb.Item>
            <Breadcrumb.Item>รายละเอียดงาน</Breadcrumb.Item>
          </Breadcrumb>
        </Row>
      <div className='flex justify-between'>
        <h1 className='title-3'>แก้ไขงาน</h1>
      </div>
      {dataEdit &&
        <FormEdit
          init={null}
          dataEdit={dataEdit} 
          optionsUserTransportation={userTransaction}
          optionsCustomer={customer}
        />
      }
    </div>
  );

}

export default TaskEdit;