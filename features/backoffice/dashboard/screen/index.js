import { Fragment, useCallback, useEffect, useMemo, useState } from "react";
import { useForm, Form, Field } from "@/components/form";
import { Space, Tabs, Spin, Col } from "antd";
import AllStatus from "../component/AllStatus";
import styles from "../style/Dashboard.module.scss";
import { SearchOutlined } from "@ant-design/icons";
import moment from "moment";
import { useFetchDashboard, useFetchTransportation } from '../api'
import StatusProduction from '../component/StatusProduction'
import StatusTransport from '../component/StatusTransport'
import StatusProductionPending from '../component/StatusProductionPending'
import { useSelector } from "react-redux";
import useGetCustomer from '@/api/master/useGetCustomer'

const { TabPane } = Tabs;
let timeout
let timeoutAutoFetch


const initialValues = {
  date: moment(),
  search: [],
  customer_id: []
};
const rules = {};

const Dashboard = () => {

  const [DataDashboard, loadingDashboard, fetchDashboard] = useFetchDashboard();
  const [DataAutocompleteCustomer, loadingCustomer, fetchCustomer] = useGetCustomer();
  const [dataTransportations, loadingTransportation, fetchTransportation] = useFetchTransportation();

  const [dateform, setdateform] = useState(null);

  console.log('dataTransportations',dataTransportations);
  const dashboard = useSelector((state) => state.dashboard);
  const search = useMemo(() => dashboard?.dashboard?.filter || {}, [dashboard])

  const form = useForm({
    initialValues,
    rules,
    onValuesUpdate: values => {
      if (timeout) clearTimeout(timeout)
      timeout = setTimeout(() => {
        clearInterval(timeoutAutoFetch)
        const body = {
          date: values?.date.format('YYYY-MM-DD'),
          customer_ids: values?.search
        }
        console.log("body>>>", body);
        fetchDashboard({ body });
        fetchTransportation( body )
      }, 1000)
    }
  })

  const { values } = form

  if (timeoutAutoFetch) clearInterval(timeoutAutoFetch)
  timeoutAutoFetch = setInterval(() => {
    clearTimeout(timeout)
    const body = {
      date: values?.date ? moment(values?.date)?.format('YYYY-MM-DD') : moment()?.format('YYYY-MM-DD'),
      customer_ids: values?.search
    };
    fetchDashboard({ body });
    fetchTransportation( body )
  }, 5000)

  const onChangeTab = useCallback(async (value) => {
    console.log(value);
    // const body = {
    //   date: values?.date.format('YYYY-MM-DD'),
    //   customer_ids: values?.search
    // }
    // if (value === '4') {
    //   await fetchTransportation( body )

    // }
  }, []);

  const onSubmit = useCallback(async (values) => {
    const body = {
      search: values?.search,
      date: values?.date.format('YYYY-MM-DD'),
    }
    await fetchDashboard({ body })
  }, [fetchDashboard]);

  return (
    <div>
      <Form form={form} handlerSubmit={onSubmit}>
        <Tabs
          defaultActiveKey="1"
          onChange={onChangeTab}
          tabBarExtraContent={
            <Space direction="horizontal" className={styles.container}>
              <Field.DatePicker
                defaultValue={moment()}
                name="date"
                className="datepick_prefix_icon"
                format={"DD/MM/YYYY"}
                allowClear={false}
              />
              <div className='grid-auto-fill-2 gap1'>
                <div className='span-2' style={{ width: 100 }}>
                  <Field.Select
                    name="search"
                    mode='multiple'
                    prefix={<SearchOutlined style={{ color: "#afafaf" }} />}
                    placeholder="ค้นหา"
                    options={DataAutocompleteCustomer}
                    keys={['id', 'customer_company_name']}
                    style={{ width: 210 }}
                  />
                </div>
              </div>
            </Space>
          }
        >
          <TabPane key={1} tab={"ทั้งหมด"}>
            <AllStatus data={DataDashboard} show="all" />
          </TabPane>
          <TabPane key={2} tab={"ฝ่ายธุรการ"}>
            <StatusProductionPending dataProductionPending={DataDashboard?.dashboard?.data} />
          </TabPane>
          <TabPane key={3} tab={"ฝ่ายผลิต"}>
            <StatusProduction dataProduction={DataDashboard?.dashboard?.data} />
          </TabPane>
          <TabPane key={4} tab={"ฝ่ายขนส่ง"}>
            <StatusTransport dataTransportation={dataTransportations} />
          </TabPane>
        </Tabs>
      </Form>
    </div>
  );
};

export default Dashboard;
