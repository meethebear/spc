import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import FormCustomer from "./FormCustomer";
///api
import { useFetchCustomerById } from "../api"




const ManageVendor = () => {

  const [datacustomerbyid, loadingcustomerbyid, fetchCustomerById] = useFetchCustomerById();

  const [dataEdit, setDataEdit] = useState(null)



  const router = useRouter();
  const { query } = router

  useEffect(()=>{
    const fetchCustomerEdit = async(id)=>{
     const res = await fetchCustomerById(id);
     console.log("res",res);
     setDataEdit(res);
    }

    if(query?.id) {
     fetchCustomerEdit(query?.id)
    }
  },[query])

  return (
    <>
        {dataEdit && <FormCustomer dataEdit={dataEdit}/>}
    </>
  );

}

export default ManageVendor