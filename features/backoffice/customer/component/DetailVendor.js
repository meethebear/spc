import { useEffect, useState } from "react";
import { Typography, Row, Card, Col, Divider, Button, Breadcrumb } from "antd"
import { Form, useForm } from "@/components/form";
import styles from "@/features/backoffice/order/styles/task.module.css"
import { HomeOutlined } from "@ant-design/icons";
import Link from "next/link";
import { useRouter } from "next/router";
import moment from 'moment'
import { useFetchCustomerById } from "../api"


const { Text } = Typography


const DetailPage = () => {

  const router = useRouter();
  const { query } = router

  const [dataCustomerById, loadingcustomerbyid, fetchCustomerById] = useFetchCustomerById();

  const [Addresses, setaAdresses] = useState(null);
  let phones = []

  useEffect(() => {
    const fetchCustomer = async (id) => {

      const res = await fetchCustomerById(id)
      return res
    }

    if (query?.id) {
      const data = fetchCustomer(query?.id)
      console.log("datacustomer", data);
      if (data) {
        const AddressDefault = dataCustomerById?.addresses?.map((item, index) => {
          return (item)
        })
        setaAdresses(AddressDefault);
      }
    }
  }, [query])

  const form = useForm({
    initialValues: "",
    rules: {}
  })


  const FormatNumberPhone = (item) => {
    const phone = item?.phone
    let checkPhone = phone.match(/-/g)
    let checkCommaPhone = phone.match(/,/g)
    let match = phone.match(/^(\d{3}||\d{2})(\d{3})(\d{4})$/)
    if (match) {
      return match[1] + '-' + match[2] + '-' + match[3]
    } else if (checkPhone) {
      return phone
    } else if (checkCommaPhone) {
      const separatephone = phone.split(',')
      const formatphone = separatephone?.map((item) => {
        let match = item?.match(/^(\d{3}||\d{2})(\d{3})(\d{4})$/)
        if (match) {
          const numberphone = match[1] + '-' + match[2] + '-' + match[3]
          phones?.push(numberphone)
        }
      }
      )
    }
  }

  return (
    <>
      <div style={{ margin: 80 }}>
        <Row>
          <Breadcrumb>
            <Breadcrumb.Item href="/backoffice/customer">
              <HomeOutlined />
              <span>ข้อมูลลูกค้า</span>
            </Breadcrumb.Item>
            <Breadcrumb.Item>ข้อมูลลูกค้า</Breadcrumb.Item>
          </Breadcrumb>
        </Row>
        <Row style={{ marginBottom: 15 }}>
          {/* <Text style={{ fontSize: 22, fontWieght: 700 }}>ข้อมูลลูกค้า</Text> */}
          <h1 className='title-3'>ข้อมูลลูกค้า</h1>

        </Row>
        <Card className={styles.card}>
          <Form form={form} >
            <Row style={{ justifyContent: "space-between" }}>
              <Text style={{ fontSize: 18 }}>ข้อมูลลูกค้า</Text>
              <Col>
                <Text>วันที่สร้าง</Text>
              </Col>
            </Row>
            <Row style={{ justifyContent: "space-between" }}>
              <Text></Text>
              <Col>
                <Text type="secondary">{moment(dataCustomerById?.created_at).format("DD/MM/YYYY")}</Text>
              </Col>
            </Row>
            <Divider />

            <Row style={{ paddingLeft: 10 }} >

              <Col xs={24} sm={24} md={12} lg={12} xl={6} style={{ marginRight: 40 }}>
                <Text type="secondary">ผู้ติดต่อ</Text>
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={15} style={{ marginRight: 40 }}>
                <Text type="secondary">ชื่อบริษัท</Text>
              </Col>

              <Col xs={24} sm={24} md={12} lg={12} xl={6} style={{ marginRight: 40, marginBottom: "1rem" }}>
                <Text style={{ fontSize: "1rem" }}>{dataCustomerById?.contact_name}</Text>
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={15} style={{ marginRight: 40, marginBottom: "1rem" }}>
                <Text style={{ fontSize: "1rem" }}>{dataCustomerById?.customer_company_name}</Text>
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={20} style={{ marginRight: 40, marginTop: 5 }}>
                <Text></Text>
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={20} style={{ marginRight: 40, marginTop: 5 }}>
              </Col>

              <Col xs={24} sm={24} md={12} lg={12} xl={6} style={{ marginRight: 40, marginTop: 5 }}>
                <Text type="secondary">เครดิต</Text>
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={15} style={{ marginRight: 40, marginTop: 5 }}>
                <Text type="secondary">วงเงิน</Text>
              </Col>

              <Col xs={24} sm={24} md={12} lg={12} xl={6} style={{ marginRight: 40, marginBottom: "1rem" }}>
                <Text style={{ fontSize: "1rem" }}>{dataCustomerById?.credit === null || dataCustomerById?.credit === "" ? "-" : dataCustomerById?.credit + ' วัน'}</Text>
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={15} style={{ marginRight: 40 }}>
                <Text style={{ fontSize: "1rem" }}>{dataCustomerById?.financial_amount === null || dataCustomerById?.financial_amount === "" ? 0 : dataCustomerById?.financial_amount?.toLocaleString()}</Text>
              </Col>
            </Row>

            {/* address */}
            {dataCustomerById?.addresses?.map((item, index) => {
              return (
                <>
                  <Row style={{ marginTop: 15, paddingLeft: 10 }}>
                    <Text type="secondary" style={{ fontSize: 14 }}>ที่อยู่ {index + 1} {item?.is_default === true ? <Text style={{ fontSize: 14, color: "#FF7A00" }}>ค่าเริ่มต้น</Text> : null}</Text>
                  </Row><Row style={{ paddingTop: 15, paddingLeft: 10 }}>
                    <Row>
                      <Col xs={24} sm={24} md={12} lg={12} xl={13} style={{ marginRight: 40 }}>
                        <Text style={{ fontSize: "1rem" }}>{item?.address} ตำบล {item?.sub_district} อำเภอ {item?.district}
                          จังหวัด{item?.province} {item?.post_code}
                        </Text>
                      </Col>
                    </Row>
                  </Row><Row style={{ paddingTop: 5, paddingLeft: 10 }}>

                    <Col xs={24} sm={24} md={12} lg={12} xl={6} style={{ marginRight: 40 }}>
                      <Text type="secondary">เบอร์โทรศัพท์:</Text>
                    </Col>
                    <Col xs={24} sm={24} md={12} lg={12} xl={15} style={{ marginRight: 40 }}>
                      <Text type="secondary">ตำแหน่ง</Text>
                    </Col>
                  </Row><Row style={{ paddingTop: 5, paddingLeft: 10 }}>
                    <Col xs={24} sm={24} md={12} lg={12} xl={6} style={{ marginRight: 40 }}>
                      <Text style={{ fontSize: "1rem" }}>{FormatNumberPhone(item)}
                        <div>{phones?.map(item => " "+item)}</div>
                      </Text>
                    </Col>
                    <Col xs={24} sm={24} md={12} lg={12} xl={15} style={{ marginRight: 40 }}>
                      {item.latitude ?
                        <a href={`https://www.google.com/maps/search/?api=1&query=${item?.latitude},${item?.longitude}`} rel="noreferrer" target="_blank" >
                          <Button type="primary">เปิดแผนที่</Button>
                        </a> : '-'}


                    </Col>
                  </Row>
                </>
              )
            })}
            <Divider />
          </Form>
        </Card>

        <Row style={{ justifyContent: "end", marginTop: 30 }}>
          <Button style={{ borderRadius: 8 }} onClick={() => { router.push(`/backoffice/customer/edit?id=${query.id}`) }}>
            แก้ไขข้อมูล
          </Button>
          <Link href="/backoffice/customer">
            <Button type="primary" htmlType="button" style={{ borderRadius: 8, marginLeft: 10 }} >
              หน้าแรก
            </Button>
          </Link>
        </Row>
      </div>

    </>
  )
}
export default DetailPage;