import { useCallback, useState } from "react";
import {Row, Col, Radio} from 'antd'
import { Field, useFormList, getMessageReq, FormList } from "@/components/form";
import {
  useFetchAutoCompleteProvince,
  useFetchDistrict,
  useFetchSubDistrict,
  useCreateCustomer,
  useFetchCustomerById,
  usePutCustomer
} from "../api"

const FormAddress = (props) => {
  const {listCtl, item, index} = props

  const [autoCompleteProvince, loadingprovince, fetchautocompleteprovince] = useFetchAutoCompleteProvince();
  const [dataDistrict, loadingdistrict, fetchDistrict] = useFetchDistrict();
  const [dataSubDistrict, loadingsubdistrict, fetchSubDistrict] = useFetchSubDistrict();
  const [idcreatecustomer, loadingcreatecustomer, createCustomer] = useCreateCustomer();
  const [datacustomerbyid, loadingcustomerbyid, fetchCustomerById] = useFetchCustomerById();
  const [loadingputCustomer, putCustomer] = usePutCustomer();

  const [provinceIdstate, setProvinceId] = useState('');

  const onChangeItem = useCallback(async (index, name, value) => {
    if (name === "district") {
      const districtId = dataDistrict.find(item => item.name_th === value)
      console.log("districtId", districtId.id);
      await fetchSubDistrict(provinceIdstate, districtId.id)
    }
    listCtl.onChange(index, name, value)
  }, [listCtl.onChange])

  const onChangePhone = useCallback(
    async (index, value, name) => {
      const valuePhone = value.replace(/(?!-)[^0-9.]/g, "")
      const prev = listCtl.values[index]
      listCtl.changeListItem(index, {
        ...prev,
        values: {
          ...prev.values,
          phone: valuePhone,
        }
      })
    }, [listCtl.changeListItem, listCtl.values]
  )

  const onChangeLatLong = useCallback(
    async (index, value, name) => {
      const valueInt = value.replace(/(?!-)[^0-9.]/g, "")
      const prev = listCtl.values[index]
      if (name === 'latitude') {
        listCtl.changeListItem(index, {
          ...prev,
          values: {
            ...prev.values,
            latitude: valueInt,
          }
        })
      } else {
        listCtl.changeListItem(index, {
          ...prev,
          values: {
            ...prev.values,
            longitude: valueInt,
          }
        })
      }
    }, [listCtl.changeListItem, listCtl.values]
  )

  const onChangeSubDistrict = useCallback(
    async (index, value) => {
      console.log("value", value);
      const findValue = dataSubDistrict?.find(item => item.name_th === value)
      const prev = listCtl.values[index]
      listCtl.changeListItem(index, {
        ...prev,
        values: {
          ...prev.values,
          sub_district: value,
          post_code: findValue?.post_code,
        }
      })
    },
    [listCtl.changeListItem, listCtl.values]
  )

  const onChangeProvince = useCallback(
    async (index, value, data) => {
      setProvinceId(data?.id)
      await fetchDistrict(data?.id)
      fetchSubDistrict()
      const prev = listCtl.values[index]
      listCtl.changeListItem(index, {
        ...prev,
        values: {
          ...prev.values,
          province: value,
          district: '',
          sub_district: '',
          post_code: ''
        }
      })

    },
    [listCtl.changeListItem, listCtl.values]
  )

  const onChange = useCallback(async (e) => {
    listCtl.values.map((item, index) => {
      if (index === e.target.value) {
        listCtl.changeListItem(index, {
          ...item,
          values: {
            ...item.values,
            is_default: true,
          }
        })
      } else {
        listCtl.changeListItem(index, {
          ...item,
          values: {
            ...item.values,
            is_default: false,
          }
        })
      }
    })
  }, [listCtl.changeListItem, listCtl.values])

  return(
    <>
    <Row style={{ marginTop: 20 }}>
      <Col xs={26} sm={24} md={20} lg={12} xl={23}>
        <Field.Input
          name="address"
          placeholder="บ้านเลขที่, หมู่ที่, ซอย ตึก/อาคาร/บริษัท, ถนน"
          value={item.values.address}
          // required={!!item.rules.address.required}
          error={!!item.errors.address}
          onChange={(name, value) => onChangeItem(index, name, value)} />
      </Col>
    </Row><Row>
        <Col xs={24} sm={24} md={9} lg={12} xl={6} style={{ marginRight: 60 }}>
          <Field.Select
            name="province"
            placeholder="จังหวัด"
            value={item.values.province}
            // required={!!item.rules.province.required}
            options={autoCompleteProvince?.map((item) => ({
              label: item.name_th,
              value: item.name_th
            }))}
            error={!!item.errors.province}
            onChange={(name, value) => onChangeProvince(index, value, autoCompleteProvince?.find(item => item.name_th === value))} />
        </Col>
        <Col xs={26} sm={24} md={9} lg={12} xl={6} style={{ marginRight: 40 }}>
          <Field.Select
            name="district"
            placeholder="อำเภอ/ เขต"
            value={item.values.district}
            // required={!!item.rules.province.required}
            options={dataDistrict?.map((item) => ({
              label: item.name_th,
              value: item.name_th
            }))}
            error={!!item.errors.district}
            onChange={(name, value) => onChangeItem(index, name, value)} />
        </Col>
        <Col xs={24} sm={24} md={9} lg={12} xl={5} style={{ marginRight: 40 }}>
          <Field.Select
            name="sub_district"
            placeholder="ตำบล/ แขวง"
            value={item.values.sub_district}
            // required={!!item.rules.province.required}
            error={!!item.errors.sub_district}
            options={dataSubDistrict?.map((item) => ({
              label: item.name_th,
              value: item.name_th
            }))}
            onChange={(name, value) => onChangeSubDistrict(index, value)} />
        </Col>
        <Col xs={24} sm={24} md={9} lg={12} xl={2} style={{ marginRight: 40 }}>
          <Field.Input
            name="post_code"
            value={item.values.post_code}
            error={!!item.errors.post_code}
            onChange={(name, value) => onChangeItem(index, name, value)}
            disabled />
        </Col>
      </Row><Row>
        <Col xs={24} sm={24} md={9} lg={12} xl={6} style={{ marginRight: 60 }}>
          <Field.Input
            maxLength={10}
            name="phone"
            label="เบอร์โทรศัพท์"
            placeholder="เบอร์โทรศัพท์"
            thousandSeparator={false}
            value={item.values.phone}
            // required={!!item.rules.province.required}
            error={!!item.errors.phone}
            onChange={(name, value) => onChangePhone(index, value, name)} />
        </Col>
        <Col xs={26} sm={24} md={9} lg={12} xl={6} style={{ marginRight: 40, marginTop: 7 }}>
          <Field.Input
            name="latitude"
            thousandSeparator={false}
            placeholder="ละติจูด"
            value={item.values.latitude}
            // required={!!item.rules.province.required}
            error={!!item.errors.latitude}
            onChange={(name, value) => onChangeLatLong(index, value, name)} />
        </Col>

        <Col xs={24} sm={24} md={9} lg={12} xl={5} style={{ marginRight: 40, marginTop: 7 }}>
          <Field.Input
            name="longitude"
            thousandSeparator={false}
            placeholder="ลองจิจูด"
            value={item.values.longitude}
            // required={!!item.rules.province.required}
            error={!!item.errors.longitude}
            onChange={(name, value) => onChangeLatLong(index, value, name)} />
        </Col>
      </Row><Row>
        <Col>

          <Radio
            value={index}
            name="is_default"
            defaultValue={false}
          >
            ตั้งเป็นที่อยู่เริ่มต้น
          </Radio>
        </Col>
      </Row>
      </>
  )
}
export default FormAddress