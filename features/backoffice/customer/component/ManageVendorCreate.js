import { useCallback, useState } from "react";
import { Field, useFormList, getMessageReq, FormList } from "@/components/form";
import { Typography, Row, Card, Col, Divider, Button, Breadcrumb, Radio, Collapse } from "antd"
import styles from "@/features/backoffice/order/styles/task.module.css"
import { useRouter } from "next/router";
import { CaretDownOutlined, HomeOutlined, PlusOutlined } from "@ant-design/icons";
import Image from "next/image";
import Link from "next/link";
import {
  useFetchAutoCompleteProvince,
  useFetchDistrict,
  useFetchSubDistrict,
  useCreateCustomer,
  useFetchCustomerById,
  usePutCustomer
} from "../api"
import moment from "moment";
import FormAddress from './FormAddress'


const { Text } = Typography
const { Panel } = Collapse;

const INIT_FORM = [{
  contact_name: '',
  customer_company_name: '',
  type: "ลูกค้าประจำ",
  credit: '',
  number: '',
}]

const RUlE_FORM = {
  contact_name: {
    required: getMessageReq('contact_name', 'ชื่อผู้ติดต่อ', 'th')
  },
  customer_company_name: {
    required: getMessageReq('customer_company_name', 'ชื่อบริษัท', 'th')
  },
}
const RUlE_ITEM = {
  address: {
    required: getMessageReq('address', 'ที่อยู่', 'th')
  },
  province: {
    required: getMessageReq('province', 'จังหวัด', 'th')
  },
  district: {
    required: getMessageReq('district', 'อำเภอ', 'th')
  },
  sub_district: {
    required: getMessageReq('sub_district', 'ตำบล', 'th')
  },
  phone: {
    required: getMessageReq('phone', 'เบอร์โทรศัพท์', 'th')
  },
}

const KEYS_LIST = {
  ADDRESS: 'addresses'
}


const FormCustomerCreate = () => {

  const router = useRouter();
  const { query } = router

  const { pathname } = useRouter();
  const pathCreate = "/backoffice/customer/create";
  const pathEdit = "/backoffice/customer/edit";

  const [autoCompleteProvince, loadingprovince, fetchautocompleteprovince] = useFetchAutoCompleteProvince();
  const [dataDistrict, loadingdistrict, fetchDistrict] = useFetchDistrict();
  const [dataSubDistrict, loadingsubdistrict, fetchSubDistrict] = useFetchSubDistrict();
  const [idcreatecustomer, loadingcreatecustomer, createCustomer] = useCreateCustomer();
  const [datacustomerbyid, loadingcustomerbyid, fetchCustomerById] = useFetchCustomerById();
  const [loadingputCustomer, putCustomer] = usePutCustomer();

  const [btn, setBtn] = useState('');
  const [Index, setIndex] = useState(1);
  const [provinceIdstate, setProvinceId] = useState('');

  let nowDate = moment().format('DD/MM/YYYY');


  const initData = [
    {
      address: '',
      province: '',
      district: '',
      sub_district: '',
      post_code: '',
      is_default: true,
      latitude: '',
      longitude: '',
      phone: '',
    }
  ]

  const form = useFormList({
    initialValues: ({
      contact_name: '',
      customer_company_name: '',
      credit: '',
      financial_amount: '',
    }),
    rules: RUlE_FORM
  },
    {
      initialValues: initData.map(item => ({
        values: {
          address: item.address,
          province: item.province,
          district: item.district,
          sub_district: item.sub_district,
          post_code: item.post_code,
          is_default: item.is_default,
          latitude: item.latitude,
          longitude: item.longitude,
          phone: item.phone,
        },
        errors: {},
        rules: RUlE_ITEM
      }))
    })

  const { listCtl } = form

  const buildBody = useCallback((values, next) => {
    console.log("BodyValues", values);
    const body = {
      contact_name: values?.contact_name,
      credit: values?.credit,
      id: query.id,
      customer_company_name: values?.customer_company_name,
      financial_amount: Number(values?.financial_amount),
      type: values?.type,
      addresses: values.addresses
    }
    next(body, values)
  }, [])




  const handlerSubmit = useCallback(async (body, values) => {
    console.log('body', body)
    console.log('values', values)

    if (pathname === pathCreate) {
      const { data } = await createCustomer({ ...body })
      console.log("IDD", data);

      if (data) {
        const { success } = await fetchCustomerById(data)
        if (success) {
          router.push(`/backoffice/customer/detail?id=${data}`)
          console.log("บันทึก")
        }
      }
    }
  })

  const onClickAddItem = useCallback(() => {
    setIndex((Index) => Index + 1)
    listCtl.addListItem({
      values: {
        address: '',
        province: '',
        district: '',
        sub_district: '',
        post_code: '',
        is_default: false,
        latitude: '',
        longitude: '',
        phone: '',
      },
      // rules: RUlE_ITEM,
      errors: {}
    })
  }, [listCtl.addListItem])

  const onChangeItem = useCallback(async (index, name, value) => {
    if (name === "district") {
      const districtId = dataDistrict.find(item => item.name_th === value)
      console.log("districtId", districtId.id);
      await fetchSubDistrict(provinceIdstate, districtId.id)
    }
    listCtl.onChange(index, name, value)
  }, [listCtl.onChange])

  const onChangePhone = useCallback(
    async (index, value, name) => {
      const valuePhone = value.replace(/(?!-)[^0-9,]/g, "")
      const prev = listCtl.values[index]
      listCtl.changeListItem(index, {
        ...prev,
        values: {
          ...prev.values,
          phone: valuePhone,
        }
      })
    }, [listCtl.changeListItem, listCtl.values]
  )

  const onChangeLatLong = useCallback(
    async (index, value, name) => {
      const valueInt = value.replace(/(?!-)[^0-9.]/g, "")
      const prev = listCtl.values[index]
      if (name === 'latitude') {
        listCtl.changeListItem(index, {
          ...prev,
          values: {
            ...prev.values,
            latitude: valueInt,
          }
        })
      } else {
        listCtl.changeListItem(index, {
          ...prev,
          values: {
            ...prev.values,
            longitude: valueInt,
          }
        })
      }
    }, [listCtl.changeListItem, listCtl.values]
  )

  const onChangeSubDistrict = useCallback(
    async (index, value) => {
      console.log("value", value);
      const findValue = dataSubDistrict?.find(item => item.name_th === value)
      const prev = listCtl.values[index]
      listCtl.changeListItem(index, {
        ...prev,
        values: {
          ...prev.values,
          sub_district: value,
          post_code: findValue?.post_code,
        }
      })
    },
    [listCtl.changeListItem, listCtl.values]
  )

  const onChangeProvince = useCallback(
    async (index, value, data) => {
      setProvinceId(data?.id)
      await fetchDistrict(data?.id)
      fetchSubDistrict()
      const prev = listCtl.values[index]
      listCtl.changeListItem(index, {
        ...prev,
        values: {
          ...prev.values,
          province: value,
          district: '',
          sub_district: '',
          post_code: ''
        }
      })

    },
    [listCtl.changeListItem, listCtl.values]
  )

  const onChange = useCallback(async (e) => {
    listCtl.values.map((item, index) => {
      if (index === e.target.value) {
        listCtl.changeListItem(index, {
          ...item,
          values: {
            ...item.values,
            is_default: true,
          }
        })
      } else {
        listCtl.changeListItem(index, {
          ...item,
          values: {
            ...item.values,
            is_default: false,
          }
        })
      }
    })
  }, [listCtl.changeListItem, listCtl.values])


  return (
    <>
      <div style={{ margin: 50 }}>
        <Row>
          <Breadcrumb>
            <Breadcrumb.Item href="/backoffice/customer">
              <HomeOutlined />
              <span>ข้อมูลลูกค้า</span>
            </Breadcrumb.Item>
            <Breadcrumb.Item>สร้างลูกค้า</Breadcrumb.Item>
          </Breadcrumb>
        </Row>
        <Row style={{ justifyContent: "space-between", marginBottom: 15 }}>
          {/* <Text style={{ fontSize: 22, fontWieght: 800 }}>{pathname === pathCreate ? "สร้างข้อมูลลูกค้า" : "ข้อมูลลูกค้า"}</Text> */}
          <h1 className='title-3'>สร้างข้อมูลลูกค้า</h1>
        </Row>
        <FormList form={form} handlerSubmit={[buildBody, handlerSubmit]} listCtls={{ [KEYS_LIST.ADDRESS]: listCtl }}>
          <Card className={styles.card}>
            <div div style={{ marginLeft: 90, marginRight: 40 }}>
              <Row style={{ justifyContent: "space-between" }}>
                <Text style={{ fontSize: 18 }}>ข้อมูลลูกค้า</Text>
                <Col>
                  <Text style={{ paddingRight: 60 }}>วันที่สร้าง</Text>
                </Col>
              </Row>
              <Row style={{ justifyContent: "space-between" }}>
                <Text type="secondary"></Text>
                <Col>
                  <Text type="secondary" style={{ paddingRight: 60 }}>{nowDate}</Text>
                </Col>
              </Row>
              <div style={{ paddingRight: 65 }}><Divider /></div>

              <div>
                <Row gutter={16} style={{ paddingTop: 5 }} >
                  <Col xs={24} sm={24} md={12} lg={24} xl={12}>
                    <Field.Input
                      label="ชื่อผู้ติดต่อ"
                      name="contact_name"
                      placeholder="กรุณากรอกชื่อผู้ติดต่อ"
                    />
                  </Col>
                  <Col xs={24} sm={24} md={12} lg={24} xl={12}>
                    <Field.Input
                      label="ชื่อบริษัท"
                      name="customer_company_name"
                      placeholder="กรุณากรอกชื่อบริษัท"
                    />
                  </Col>
                </Row>
                <Row gutter={16} style={{ paddingTop: 5 }} >
                  <Col xs={24} sm={12} md={12} lg={12} xl={12}>
                    <Field.Input
                      label="เครดิต"
                      name="credit"
                      placeholder="เครดิต (วัน)"
                    />
                  </Col>
                  <Col xs={24} sm={12} md={12} lg={12} xl={12} >
                    <Field.Number
                      label="วงเงิน"
                      name="financial_amount"
                      id='number'
                      placeholder="วงเงิน"
                    />
                  </Col>
                </Row>
                <Row>
                  <Radio.Group onChange={onChange} style={{ width: '100%' }} defaultValue={0}>
                    {listCtl.values?.map?.((item, index) => (
                      <>
                        <section style={{ paddingBottom: 20 }}>
                          <Row style={{ justifyContent: "space-between", paddingRight: 50, paddingTop: 24, borderTop: "1px solid #f0f0f0" }}>
                            <Col>
                              <Text style={{ fontSize: "1rem", display: 'flex' }}>ที่อยู่ {index + 1} {index === 0 ? <div style={{ color: '#E00000', marginLeft: 3 }}>*</div> : '(ตัวเลือก)'}
                              </Text>
                              {/* <Collapse

                                // className={styles.collapse_styles}
                                expandIcon={({ isActive }) => (
                                  <CaretDownOutlined rotate={isActive ? 0 : -90} />
                                )}
                                bordered={false}
                                defaultActiveKey={[1]}
                              >
                                <Panel header={'ที่อยู่ '+index} key={1} style={{ width: 1080 }}>
                                  <FormAddress listCtl={listCtl} item={item} index={index}/>
                              </Panel>
                            </Collapse> */}
                          </Col>
                          {index === 0 ? null :
                            <div style={{ display: "flex" }}>
                              <Image
                                src="/images/icon_delete.svg"
                                alt="bin"
                                width={20}
                                height={20}
                              />
                              <Col>

                                <Button type="link" style={{ color: "black" }} onClick={() => listCtl.removeListItem(index)}>
                                  ลบที่อยู่
                                </Button>

                              </Col>
                            </div>
                          }
                        </Row>
                        <Row style={{ marginTop: 20 }}>
                          <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                            <Field.Input
                              name="address"
                              placeholder="บ้านเลขที่, หมู่ที่, ซอย ตึก/อาคาร/บริษัท, ถนน"
                              value={item.values.address}
                              // required={!!item.rules.address.required}
                              error={!!item.errors.address}
                              onChange={(name, value) => onChangeItem(index, name, value)}
                            />
                          </Col>
                        </Row>
                        <Row gutter={40}>
                          <Col xs={24} sm={24} md={6} lg={6} xl={7}>
                            <Field.Select
                              name="province"
                              placeholder="จังหวัด"
                              value={item.values.province}
                              // required={!!item.rules.province.required}
                              options={autoCompleteProvince?.map((item) => ({
                                label: item.name_th,
                                value: item.name_th
                              }))}
                              error={!!item.errors.province}
                              onChange={(name, value) => onChangeProvince(index, value, autoCompleteProvince?.find(item => item.name_th === value))}
                            />
                          </Col>
                          <Col xs={26} sm={24} md={6} lg={6} xl={7}>
                            <Field.Select
                              name="district"
                              placeholder="อำเภอ/ เขต"
                              value={item.values.district}
                              // required={!!item.rules.province.required}
                              options={dataDistrict?.map((item) => ({
                                label: item.name_th,
                                value: item.name_th
                              }))}
                              error={!!item.errors.district}
                              onChange={(name, value) => onChangeItem(index, name, value)}
                            />
                          </Col>
                          <Col xs={24} sm={24} md={6} lg={6} xl={7}>
                            <Field.Select
                              name="sub_district"
                              placeholder="ตำบล/ แขวง"
                              value={item.values.sub_district}
                              // required={!!item.rules.province.required}
                              error={!!item.errors.sub_district}
                              options={dataSubDistrict?.map((item) => ({
                                label: item.name_th,
                                value: item.name_th
                              }))}
                              onChange={(name, value) => onChangeSubDistrict(index, value)}
                            />
                          </Col>
                          <Col xs={24} sm={24} md={6} lg={4} xl={3}>
                            <Field.Input
                              name="post_code"
                              value={item.values.post_code}
                              error={!!item.errors.post_code}
                              onChange={(name, value) => onChangeItem(index, name, value)}
                              disabled
                            />
                          </Col>
                        </Row>
                        <Row gutter={40}>
                          <Col xs={24} sm={24} md={8} lg={6} xl={7}>
                            <Field.Input
                              // maxLength={10}
                              name="phone"
                              label="เบอร์โทรศัพท์"
                              placeholder="เบอร์โทรศัพท์"
                              thousandSeparator={false}
                              value={item.values.phone}
                              // required={!!item.rules.province.required}
                              error={!!item.errors.phone}
                              onChange={(name, value) => onChangePhone(index, value, name)}
                            />
                          </Col>
                          <Col xs={26} sm={24} md={8} lg={6} xl={7}>
                            <Field.Input
                              name="latitude"
                              thousandSeparator={false}
                              placeholder="ละติจูด"
                              value={item.values.latitude}
                              // required={!!item.rules.province.required}
                              error={!!item.errors.latitude}
                              onChange={(name, value) => onChangeLatLong(index, value, name)}

                            />
                          </Col>

                          <Col xs={24} sm={24} md={8} lg={6} xl={7}>
                            <Field.Input
                              name="longitude"
                              thousandSeparator={false}
                              placeholder="ลองจิจูด"
                              value={item.values.longitude}
                              // required={!!item.rules.province.required}
                              error={!!item.errors.longitude}
                              onChange={(name, value) => onChangeLatLong(index, value, name)}
                            />
                          </Col>
                        </Row>
                        <Row>
                          <Col>

                            <Radio
                              value={index}
                              name="is_default"
                              defaultValue={false}
                            >
                              ตั้งเป็นที่อยู่เริ่มต้น
                            </Radio>
                          </Col>
                        </Row>
                      </section>
                      </>
                    ))}
                </Radio.Group>
              </Row>

              <Row style={{ justifyContent: "end", paddingRight: 50 }}>
                <Button className={styles.btn} htmlType="button"
                  onClick={onClickAddItem}
                  disabled={listCtl.values.length >= 5 ? true : false}
                  icon={<PlusOutlined />}
                >
                  เพิ่มที่อยู่จัดส่ง
                </Button>
              </Row>
              <Divider />

              <Row style={{ justifyContent: "end", marginTop: 30, paddingRight: 50 }}>
                <Link href="/backoffice/customer">
                  <Button style={{ borderRadius: 8 }}>
                    ยกเลิก
                  </Button>
                </Link>
                {/* <Link href="/backoffice/customer/detail"> */}
                <Button type="primary" htmlType="submit" style={{ borderRadius: 8, marginLeft: 10 }}
                  onClick={() => setBtn("1")}
                >
                  {pathname === pathEdit ? "บันทึกข้อมูล" : "สร้างข้อมูลลูกค้า"}
                </Button>
                {/* </Link> */}
              </Row>
            </div>
          </div>
        </Card>

      </FormList>
    </div>
    </>
  )
}

export default FormCustomerCreate