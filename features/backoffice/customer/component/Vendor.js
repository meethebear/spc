import React, { useCallback, useState, useEffect, useMemo } from "react";
import { Typography, Row, Col, Button, Card, Pagination } from "antd";
import { Field, Form, useForm } from "@/components/form";
import { SearchOutlined, PlusOutlined } from "@ant-design/icons";
import moment from "moment";
import Link from "next/link";
import { useSelector } from "react-redux";
import VendorCardData from "./CardVendor";
import { useFetchCustomer } from "../api"


const { Text } = Typography;
let timeout


const VendorPage = () => {

  const [sectionTable, setSectionTable] = useState(false)

  const customer = useSelector((state) => state.customer);
  const search = useMemo(() => customer?.customer?.filter || {}, [customer])

  const [dataCustomer, loadingudatacustomer, fetchCustomer] = useFetchCustomer();

  console.log("data", dataCustomer);

  const dataTable = dataCustomer?.data

  const defaultValue = useCallback(async (body = {}) => {
    await fetchCustomer({ ...search, ...body })
  }, [fetchCustomer, search])

  const form = useForm({
    initialValues: {},
    rules: {},
    onValuesUpdate: values => {
      if (timeout) clearTimeout(timeout)
      timeout = setTimeout(() => {
        const body = {
          date: values?.date ? moment(values?.date).format('YYYY-MM-DD'): null,
          name: values?.input,
          type: values?.type,
          page: values?.page || search?.page,
          per_page: values?.per_page || search?.per_page,
        };
        setSectionTable(true)
        fetchCustomer(body);
      }, 600)
    }
  })

  return (
    <div style={{ margin: 40 }}>
      <Row style={{ justifyContent: "space-between", marginBottom: 15 }}>
        <div style={{ display: "flex", alignItems: "baseline" }}>
          <h1 className='title-3'>ลูกค้า </h1>
          <Text type="secondary" style={{ fontSize: 14, paddingLeft: 5 }}>({dataCustomer?.data?.pagination?.total})</Text>
        </div>
        <Link href="/backoffice/customer/create">
          <Button style={{ backgroundColor: "#FF900E", borderRadius: 8, color: "#FAFAFA" }} icon={<PlusOutlined />}>
            สร้างข้อมูลลูกค้า
          </Button>
        </Link>
      </Row>
      <Form form={form}>
        <Card style={{ backgroundColor: "#F0F0F0", borderRadius: 8 }}>
          <Row gutter={16} style={{ marginTop: 25 }}>
            <Col xs={24} sm={24} md={8} lg={12} xl={6}>
              <Field.Input
                name="input"
                prefix={<SearchOutlined
                style={{ color: "#afafaf" }} />}
                placeholder="ค้นหาชื่อผู้ติดต่อ/ บริษัท"
              />
            </Col>
            {/* <Col span={2}/> */}

            {/* <Col xs={24} sm={24} md={8} lg={12} xl={4}>
              <Field.Select
                name='type'
                placeholder="ประเภททั้งหมด"
                options={[
                  {
                    label: "ลูกค้าประจำ",
                    value: "ลูกค้าประจำ",
                  }
                ]}
                allowClear
              />
            </Col> */}
            <Col xs={24} sm={24} md={8} lg={12} xl={4}>
              <Field.DatePicker
                name='date'
                allowClear
                format="DD/MM/YYYY"
              />
            </Col>
          </Row>
        </Card>
        <div style={{ marginTop: 30 }}>
        <VendorCardData
          dataCustomers={dataTable}
          defaultValue={defaultValue}
          loading={loadingudatacustomer}
        />
        </div>
      </Form>
    </div>
  );
};

export default VendorPage;