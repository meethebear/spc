import { Button, Col, Row, Typography, Dropdown, Menu, Pagination, Modal, Spin, Table } from "antd";
import { useState, useCallback, useEffect, useMemo } from "react";
import { MenuOutlined, MoreOutlined, InfoCircleOutlined, ArrowDownOutlined } from "@ant-design/icons";
import moment from "moment";
import DropDown from '@/components/DropDown'
import Link from 'next/link'
import { useRouter } from "next/router";
import PropTypes from 'prop-types'
import { useSelector } from "react-redux";
import { useDeleteCustomer, useFetchCustomer } from "../api"

const { Text } = Typography;
const { confirm } = Modal;


const VendorCardData = (props) => {

  const { dataCustomers, defaultValue, loading } = props

  const customer = useSelector((state) => state.customer);
  const search = useMemo(() => customer?.customer?.filter || {}, [customer])

  console.log("dataCustomers", search);

  const [loadingDelete, deleteCustomer] = useDeleteCustomer();
  const [dataCustomer, loadingudatacustomer, fetchCustomer] = useFetchCustomer();

  const [current, setCurrent] = useState(1);
  const [table, setTable] = useState(false);

  const router = useRouter();

  const handleTableChange = (page,per_page) => {
    fetchCustomer({
      date: search?.date,
      name: search?.name,
      type: search?.type,
      page: page,
      per_page: per_page
    })
  };


  const confirmDelete = useCallback(
    (id) => {
      const body = {
        id: id
      }
      console.log("recode :>", body);
      confirm({
        title: "คุณแน่ใจใ่ช่ไหม ที่จะลบข้อมูลดังกล่าว ?",
        icon: <InfoCircleOutlined style={{ color: "#EE2524" }} />,
        content: `ข้อมูลนี้จะถูกลบโดยทันที และจะไม่สามารถนำกลับมาได้`,
        okText: "ลบข้อมูล",
        okType: "danger",
        cancelText: "ยกเลิก",

        async onOk() {
          const { success } = await deleteCustomer(body);
          console.log("success", success);
          if (success) {
            await defaultValue();
          }
        }
      });
    }, [deleteCustomer, defaultValue]
  );

  const columns = [
    
    {
      title: 'รายชื่อลูกค้า',
      dataIndex: "customer_company_name",
      render: (text, record, index) => (
        <>
        <div>{record?.customer_company_name}</div>
        <div><Text type="secondary">{record?.contact_name}</Text></div>
        </>
      )
    },
    // {
    //   title: 'ประเภทลูกค้า',
    //   dataIndex: "type",
    //   width: '15%',
    //   align: "center"
    // },
    {
      title: 'วันที่สร้าง',
      dataIndex: "created_at",
      width: '15%',
      align: "center",
      render: (text, record, index) => (
        moment(record.created_at).format("DD/MM/YYYY")
      )
    },
    {
      title: '',
      width: '20%',
      render: (text, record, index) => (
        <div className='flex align-center gap3'>
          <Link href={`/backoffice/customer/detail?id=${record.id}`}>
          <Button type="primary" icon={<MenuOutlined/>}>
            {'รายละเอียด'}
          </Button>
          </Link>
          <DropDown
            icon='more'
            items={[
              {
                key: 1,
                label: <div className='p-dd-item'>แก้ไขข้อมูลผู้ใช้งาน</div>,
                onClick: () => { editById(record.id); }
              },
              {
                key: 2,
                label: <div className='p-dd-item'>ลบข้อมูลผู้ใช้งาน</div>,
                onClick: () => { confirmDelete(record.id) }
              }
            ]}
          />
        </div>
      )
    }
  ]

  const editById = (id) => {
    console.log(id);
    router.push(`/backoffice/customer/edit?id=${id}`)
  }

  return (
    <>
      <Table
        dataSource={dataCustomers?.data}
        columns={columns}
        total={dataCustomers?.pagination?.total}
        loading={loadingudatacustomer}
        pageSize = {dataCustomers?.pagination?.per_page}
        pagination={false}
      />
      <br/>
      <Pagination
      page={dataCustomers?.pagination?.page}
      defaultCurrent={1}
      // pageSize={users?.users?.data?.pagination?.per_page}
      total={dataCustomers?.pagination?.total}
      onChange={handleTableChange}
      />
    </>

  );

}

VendorCardData.propTypes = {
  dataCustomers: PropTypes.object,
  defaultValue: PropTypes.func,
  loading: PropTypes.any
}

export default VendorCardData;