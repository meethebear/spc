import { useState, useEffect, useCallback } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector, useDispatch } from "react-redux";
import { resError, resOK } from "@/utils/response";

const useFetchAutoCompleteProvince = () => {
  const name = 'fetchGetProvinceAutocomplete'
  const API = useAPI(name, 'none')
  const { tasksRunning } = useSelector(selector(['tasksRunning']))
  const [result, useResult] = useState();

  const fetchGetProvinceAutocomplete = useCallback(async (filter = {}) => {
    try {
      API.begin()
      const { data } = await API.get('/api/v1/master/province', { params: { ...filter } })
      const res = resOK(data)
      useResult(res)
    } catch (error) {
      const e = resError(error)
      API.error(e)
    } finally {
      API.end()
    }
  }, [])

  useEffect(() => {
    fetchGetProvinceAutocomplete()
  }, [])

  return [result?.data, tasksRunning[name]?.loading, fetchGetProvinceAutocomplete]

}

export default useFetchAutoCompleteProvince 