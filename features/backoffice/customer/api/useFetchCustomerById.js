import { useState, useEffect, useCallback } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector } from "react-redux";
import { resError, resOK } from "@/utils/response";

const useFetchCustomerById = () => {
  const name = "fetchCustomerById";
  const API = useAPI(name, "overlay");
  const [result, useResult ] = useState();
  const { tasksRunning } = useSelector(
    selector(["tasksRunning"])
  );

  const fetchCustomerById = useCallback(async (id) => {
    try {
      API.begin();
      const { data } = await API.get(`/api/v1/customer/${id}`);
      const res = resOK(data);
      useResult(res)
      return res;
    } catch (error) {
      const e = resError(error);
      API.error(e);
    } finally {
      API.end();
    }
    return {
      success: false
    }
  }, []);

  return [result?.data,tasksRunning?.[name]?.loading, fetchCustomerById];
};

export default useFetchCustomerById;
