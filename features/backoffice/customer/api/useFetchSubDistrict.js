import { useState, useEffect, useCallback } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector } from "react-redux";
import { resError, resOK } from "@/utils/response";

const useFetchSubDistrict = () => {
  const name = "fetchSubDistrict";
  const API = useAPI(name, "none");
  const [result, useResult ] = useState();
  const { tasksRunning } = useSelector(
    selector(["tasksRunning"])
  );

  const fetchSubDistrict = useCallback(async (province_id,sub_district_id) => {
    try {
      API.begin();
      const { data } = await API.get(`/api/v1/master/sub_district/${province_id}/${sub_district_id}`);
      const res = resOK(data);
      useResult(res)
      return res;
    } catch (error) {
      const e = resError(error);
      API.error(e);
    } finally {
      API.end();
    }
    return {
      success: false
    }
  }, []);

  return [result?.data,tasksRunning?.[name]?.loading, fetchSubDistrict];
};

export default useFetchSubDistrict;
