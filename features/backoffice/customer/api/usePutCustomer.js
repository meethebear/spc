import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector } from "react-redux";
import { resError, resOK } from "@/utils/response";
import { message } from 'antd'

const usePutCustomer = () => {
  const name = "useUpdateCustomer";
  const API = useAPI(name, 'none')
  const { tasksRunning } = useSelector(selector(["tasksRunning"]));

  const editCustomer = async (body) => {
    console.log(body);
    try {
      API.begin();
      const { data } = await API.put(`/customer`, body)
      const res = resOK(data)
      message.success(res.message)
      return res
    } catch (error) {
      const e = resError(error);
      API.error(e);
      message.error(e?.errorMessage);
    } finally {
      API.end();
    } 
    return {
      success: false
    }
  };

  return [tasksRunning?.[name]?.loading, editCustomer];
};

export default usePutCustomer;