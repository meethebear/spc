import { useCallback, useEffect } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector, useDispatch } from "react-redux";
import { resError, resOK } from "@/utils/response";
import { actionIndexCustomer } from '@/actions/customer.action';

const useFetchCustomer = () => {
  const name = 'fetchCustomerIndex'
  const API = useAPI(name, 'none')
  const { customer, tasksRunning } = useSelector(selector(['customer', 'tasksRunning']))
  const useThunkDispatch = () => useDispatch()
  const dispatch = useThunkDispatch()

  const fetchCustomerIndex = useCallback(async (filter = {}) => {
    try {
      API.begin()
      const { data } = await API.get('/api/v1/customer', { params: { ...filter } })
      const res = resOK(data)
      dispatch(actionIndexCustomer({
        filter: { page: res.data.page, per_page: res.data.per_page, ...filter, }, data: res.data
      }))
    } catch (error) {
      const e = resError(error)
      API.error(e)
    } finally {
      API.end()
    }
  }, [])

  useEffect(() => {
    fetchCustomerIndex({ page: 1, per_page: 10 })
  }, [])

  return [customer?.customer, tasksRunning?.[name]?.loading, fetchCustomerIndex]
}

export default useFetchCustomer