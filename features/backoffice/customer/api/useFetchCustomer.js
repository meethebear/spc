import { useState, useEffect, useCallback } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector, useDispatch } from "react-redux";
import { resError, resOK } from "@/utils/response";
import { actionIndexCustomer } from '@/actions/customer.action';

const useFetchCustomer = () => {
  const name = 'fetchCustomer'
  const API = useAPI(name, 'none')
  const { customer, tasksRunning } = useSelector(selector(['customer', 'tasksRunning']))
  const useThunkDispatch = () => useDispatch()
  const dispatch = useThunkDispatch()

  const fetchCustomer = useCallback(async (filter = {}) => {
    try {
      API.begin()
      const { data } = await API.get('/api/v1/customer', { params: { ...filter }} , {auth: false }) 
      const res = resOK(data)
      dispatch(actionIndexCustomer({
        filter: { page: res.data.page, page_size: res.data.page_size, ...filter, }, data: res.data
      }))
    } catch (error) {
      const e = resError(error)
      API.error(e)
    } finally {
      API.end()
    }
  }, [])

  return [customer, tasksRunning?.[name]?.loading, fetchCustomer]
}

export default useFetchCustomer