import { useCallback, useEffect } from 'react'
import { useRouter } from 'next/router'
import { Form, useForm, Field, getMessageReq } from '@/components/form'
import Button from '@/components/Button'
import { DEPARTMENT } from '@/utils/constant'
import PropTypes from 'prop-types'
import usePostAPI from '@/utils/hook/usePostAPI'
import checkAllowedPassword from '@/utils/allowedPassword'
import Link from 'next/link'
import { usePutUser } from '../api'

const RUlE_FORM = {
  full_name: {
    required: getMessageReq('Full name', 'ชื่อ - นามสกุล', 'th')
  },
  phone: {
    required: getMessageReq('Phone', 'เบอร์ติดต่อ', 'th')
  },
  rank: {
    required: getMessageReq('Rank', 'ตำแหน่ง', 'th')
  },
  department: {
    required: getMessageReq('Department', 'ตำแหน่ง', 'th')
  },
  username: {
    required: getMessageReq('Username', 'ชื่อผู้ใช้งาน', 'th')
  },
}

const FormUserManage = props => {
  const { init, dataEdit } = props

  const [ loadingPutUser, putUser] = usePutUser();

  const { replace } = useRouter();
  const router = useRouter();
  const { query } = router
  const { pathname } = useRouter();
  const pathCreate = "/backoffice/user/create"
  const pathEdit = "/backoffice/user/edit"

  console.log("dataEdit",dataEdit);

  const form = useForm({
    initialValues: ({
      department: dataEdit?.data?.department,
      full_name: dataEdit?.data?.full_name,
      password: dataEdit?.data?.password,
      phone: dataEdit?.data?.phone,
      rank: dataEdit?.data?.rank,
      role: dataEdit?.data?.role,
      username: dataEdit?.data?.username, 
    }),
    rules: RUlE_FORM
  })

  const [apiCreate, loadingCreate] = usePostAPI('createUser', '/user/create')

  const handlerSubmit = useCallback(async (values) => {
    const body = {
      department: values.department,
      full_name: values.full_name,
      // email: values.email,
      phone: values.phone,
      role: "EMPLOYEE",
      id: query?.id
    }
    console.log("bodyEdit",body);
      const { success } = await putUser({ ...body })
      if (success) {
        router.push(`/backoffice/user/${query.id}`)
      }
  }, [apiCreate])

  return (
    <div className='bg-card-form' style={{ maxWidth: '1032px' }}>
      <Form form={form} handlerSubmit={handlerSubmit} >
        <div>
          <h2>ข้อมูลผู้ใช้งาน</h2>
        </div>
        <hr />
        <div className='grid-auto-fill-2 gap1' style={{ marginTop: '1rem' }}>
          <div className='span-2'>
            <Field.Input
              label='ชื่อ - นามสกุล'
              name='full_name'
              placeholder='กรุณากรอกชื่อ - นามสกุล'
            />
          </div>
          <div className='span-2'>
            <Field.Number
              label='เบอร์ติดต่อ'
              name='phone'
              allowNegative={false}
              allowLeadingZeros
              placeholder='กรุณากรอกหมายเลขโทรศัพท์'
              thousandSeparator={false}
            />
          </div>
          <div className='span-2'>
            <Field.Input
              label='ตำแหน่ง'
              name='rank'
              placeholder='กรุณากรอกตำแหน่ง'
            />
          </div>
          <div className='span-2'>
            <Field.Select
              label='สิทธิ'
              name='department'
              options={DEPARTMENT}
              placeholder='กรุณาเลือกสิทธิการใช้งาน'
            />
          </div>
        </div>
        <hr />
        <br />
        <div className='flex justify-end'>
          <div className='grid-auto-fill-05 gap1 align-end' style={{ width: '100%', maxWidth: 320 }}>
            <div className='span-2'>
              <Link href={"/backoffice/user"}>
              <Button htmlType='button' type='default' className='w-100'>
                ยกเลิก
              </Button>
              </Link>
            </div>
            <div className='span-3'>
              <Button htmlType='submit' className='w-100'>
                {pathname === pathCreate ? "สร้างผู้ใช้งาน" : "แก้ไขผู้ใช้งาน"}
              </Button>
            </div>
          </div>
        </div>
      </Form>
    </div>
  )
}

const initialValues = {
  department: "OFFICER",
  full_name: "ชื่อ - นามสกุล",
  password: "P@ssw0rd",
  phone: "0999999999",
  rank: "ตำแหน่ง",
  role: "EMPLOYEE",
  username: "user",
}

const getInit = (init) => {
  if (init) {
    const result = {}
    Object.keys(initialValues).forEach((key) => {
      const value = result[key]
      result[key] = value
    })
    return {
      ...result,

    }
  }

  return initialValues
}

FormUserManage.propTypes = {
  init: PropTypes.object,
  dataEdit: PropTypes.object
}

export default FormUserManage
