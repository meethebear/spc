import PropTypes from 'prop-types'
import styles from '../styles/LabelBoxInfo.module.css'

const LabelBoxInfo = props => {
  const { label, value } = props
  return (
    <div>
      <label className={styles.label}>{label}</label>
      <p className={styles.value}>{value}</p>
    </div>
  )
}

LabelBoxInfo.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
}

export default LabelBoxInfo