import React, { useCallback } from 'react'
import { SearchOutlined } from '@ant-design/icons';
import { Form, useForm, Field } from '@/components/form'
import Button from '@/components/Button'
import moment from "moment";
import { DEPARTMENT } from '@/utils/constant'
import { useFetchUser } from '../api';
import { Col, Row } from 'antd';

let timeout

const FormSearch = props => {
  const { init } = props

  const [datausers, loadindfetchUser, fetchUser] = useFetchUser();

  const form = useForm({
    initialValues: getInit(init),
    rules: {},
    onValuesUpdate: values => {
      if (timeout) clearTimeout(timeout)
      timeout = setTimeout(() => {
        const body = {
          date: values?.date ? moment(values?.date).format('YYYY-MM-DD'): null,
          name: values?.name,
          department: values?.department,
          page: 1,
          per_page: 10,
        };
        fetchUser(body);
      }, 600)
    }
  })

  const handlerSubmit = useCallback((values) => {
    console.log('values', values)
  }, [])

  return (
    <div className='card-filter-bar'>
      <Form form={form} handlerSubmit={handlerSubmit}>
        {/* <div className='grid-auto-fill-05 gap2'> */}
        <Row style={{ justifyContent: 'space-between' }}>
        <Col xs={26} sm={24} md={7} lg={12} xl={6}>
          <div className='span-4'>
            <Field.Input
              name='name'
              placeholder='ค้นหาชื่อผู้ใช้งาน'
              prefix={<SearchOutlined style={{ color: '#808080' }} />}
            />
          </div>
          </Col>
          <Col xs={26} sm={24} md={7} lg={12} xl={6}>
          <div className='span-2'>
            <Field.Select
              name='department'
              placeholder='สิทธิทั้งหมด'
              options={DEPARTMENT}
              allowClear
            />
          </div>
          </Col>
          <Col xs={26} sm={24} md={7} lg={12} xl={6}>
          <div className='span-2'>
            <Field.DatePicker
              name='date'
            />
          </div>
          </Col>
        {/* </div> */}
        </Row>
      </Form>
    </div>
  )
}

const initialValues = {
  name: '',
  role: ''
}

const getInit = (init) => {
  if (init) {
    const result = {}
    Object.keys(initialValues).forEach((key) => {
      const value = result[key]
      result[key] = value
    })
    console.log("result",result)
    return {
      ...result,

    }
  }

  return initialValues
}

export default FormSearch
