import { useCallback, useRef } from 'react'
import Modal from '@/components/modals'
import Button from '@/components/Button'
import FormResetPassword from './FormResetPassword'
import PropTypes from 'prop-types'
import checkAllowedPassword from '@/utils/allowedPassword'
import usePatchAPI from '@/utils/hook/userPatchAPI'
import { Form, Field, useForm, getMessageReq } from '@/components/form'
import { useRouter } from 'next/router'


const ModalResetPassword = props => {
  const { open, onCancel } = props
  const refSubmit = useRef()
  const [apiResetPassword] = usePatchAPI('resetPassword', '/admin/user/set_password')
  const { query } = useRouter();
  const router = query
  const init = null


  const form = useForm({
    initialValues: getInit(init),
    rules: {
      password_ph: {
        required: getMessageReq('Password', 'รหัสผ่าน', 'th'),
        isAllowed: {
          func: (value) => {
            return checkAllowedPassword(value)
          },
          msg: 'รหัสผ่านควรประกอบด้วยอักษรตัวพิมพ์ใหญ่ ตัวพิมพ์เล็ก ตัวเลข และมีความยาวอย่างน้อย 8 ตัวอักษร'
        }
      },
      password_confirm_ph: {
        required: getMessageReq('Confirm Password', 'ยืนยันรหัสผ่าน', 'th'),
        isAllowed: {
          func: (value, values) => values.password_ph === value,
          msg: 'รหัสผ่านไม่ตรงกัน!'
        }
      },
    }
  })

  const {handlerReset} = form

  const onSubmit = useCallback(async (values) => {
    const body = {
      id: query.id,
      password: values?.password_ph
    }
    console.log("body", body);
    const { success } = await apiResetPassword(body)
    if (success) {
      onCancel();
      handlerReset();

    }
  }, [apiResetPassword])

  const onClickSave = useCallback(() => {
    refSubmit?.current?.click()
  }, [refSubmit])



  return (
    <Modal
      title='แก้ไขรหัสผ่านผู้ใช้งาน'
      visible={open}
      onCancel={onCancel}
      centered={false}
      footer={[
        <Button key='c' type='default' onClick={onCancel}>ยกเลิก</Button>,
        <Button key='s' onClick={onClickSave}>บันทึก</Button>
      ]}
    >
      <Form form={form} handlerSubmit={onSubmit}>
        <FormResetPassword
          refSubmit={refSubmit}
        />
      </Form>
    </Modal>
  )
}
const initialValues = {
  department: "OFFICER",
  full_name: "ชื่อ - นามสกุล",
  password: "P@ssw0rd",
  phone: "0999999999",
  rank: "ตำแหน่ง",
  role: "EMPLOYEE",
  username: "user",
}

const getInit = (init) => {
  if (init) {
    const result = {}
    Object.keys(initialValues).forEach((key) => {
      const value = result[key]
      result[key] = value
    })
    return {
      ...result,

    }
  }

  return initialValues
}

ModalResetPassword.propTypes = {
  open: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired
}

export default ModalResetPassword