import { useCallback } from 'react'
import { Form, Field, useForm, getMessageReq } from '@/components/form'
import checkAllowedPassword from '@/utils/allowedPassword'
import usePatchAPI from '@/utils/hook/userPatchAPI'
import PropTypes from 'prop-types'
import { useRouter } from 'next/router'

const FormResetPassword = props => {
  const { refSubmit, init } = props
  const { query } = useRouter();

  console.log("ID >>>>",query.id);



  return (
    <>
      <div>
      รหัสผ่านที่ดีควรจะผสมกันระหว่างอักษรตัวพิมพ์ใหญ่ ตัวพิมพ์เล็ก ตัวเลข และมีความยาวอย่างน้อย 8 ตัวอักษร
      </div>
      <Field.Password
        label='รหัสผ่านใหม่'
        name='password_ph'
      />
      <Field.Password
        label='ยืนยันรหัสผ่านใหม่'
        name='password_confirm_ph'
      />

      <button ref={refSubmit} type="submit" hidden ></button>
    </>
  )
}

const initialValues = {
  department: "OFFICER",
  full_name: "ชื่อ - นามสกุล",
  password: "P@ssw0rd",
  phone: "0999999999",
  rank: "ตำแหน่ง",
  role: "EMPLOYEE",
  username: "user",
}

const getInit = (init) => {
  if (init) {
    const result = {}
    Object.keys(initialValues).forEach((key) => {
      const value = result[key]
      result[key] = value
    })
    return {
      ...result,

    }
  }

  return initialValues
}

FormResetPassword.propTypes = {
  refSubmit: PropTypes.any.isRequired,
  init: PropTypes.object
}

export default FormResetPassword