import { useCallback } from 'react'
import { useRouter } from 'next/router'
import { Form, useForm, Field, getMessageReq } from '@/components/form'
import Button from '@/components/Button'
import { DEPARTMENT } from '@/utils/constant'
import PropTypes from 'prop-types'
import usePostAPI from '@/utils/hook/usePostAPI'
import checkAllowedPassword from '@/utils/allowedPassword'
import Link from 'next/link'

const RUlE_FORM = {
  full_name: {
    required: getMessageReq('Full name', 'ชื่อ - นามสกุล', 'th')
  },
  phone: {
    required: getMessageReq('Phone', 'เบอร์ติดต่อ', 'th')
  },
  rank: {
    required: getMessageReq('Rank', 'ตำแหน่ง', 'th')
  },
  department: {
    required: getMessageReq('Department', 'ตำแหน่ง', 'th')
  },
  username: {
    required: getMessageReq('Username', 'ชื่อผู้ใช้งาน', 'th')
  },
  password_ph: {
    required: getMessageReq('Password', 'รหัสผ่าน', 'th'),
    isAllowed: {
      func: (value) => {
        return checkAllowedPassword(value)
      },
      msg: 'รหัสผ่านควรประกอบด้วยอักษรตัวพิมพ์ใหญ่ ตัวพิมพ์เล็ก ตัวเลข และมีความยาวอย่างน้อย 8 ตัวอักษร'
    }
  },
  password_confirm_ph: {
    required: getMessageReq('Confirm Password', 'ยืนยันรหัสผ่าน', 'th'),
    isAllowed: {
      func: (value, values) => values.password_ph === value,
      msg: 'รหัสผ่านไม่ตรงกัน!'
    }
  },
}

const FormUserManage = props => {
  const { init } = props
  const { replace } = useRouter();
  const { pathname } = useRouter();
  const pathCreate = "/backoffice/user/create"

  const form = useForm({
    initialValues: getInit(init),
    rules: RUlE_FORM
  })

  const [apiCreate, loadingCreate] = usePostAPI('createUser', '/user/create')

  const handlerSubmit = useCallback(async (values) => {
    const body = {
      "department": values.department,
      "full_name": values.full_name,
      "password": values.password_ph,
      "phone": values.phone,
      "rank": values.rank,
      "role": "EMPLOYEE",
      "username": values.username
    }
    console.log("body",body);
    const { success, data } = await apiCreate(body)
    if (success) {
      console.log(data)
      replace(`/backoffice/user/${data}?save=success`)
    }
  }, [apiCreate])

  return (
    <div className='bg-card-form' style={{ maxWidth: '1032px' }}>
      <Form form={form} handlerSubmit={handlerSubmit} >
        <div>
          <h2>ข้อมูลผู้ใช้งาน</h2>
        </div>
        <hr />
        <div className='grid-auto-fill-2 gap1' style={{ marginTop: '1rem' }}>
          <div className='span-2'>
            <Field.Input
              label='ชื่อ - นามสกุล'
              name='full_name'
              placeholder='กรุณากรอกชื่อ - นามสกุล'
            />
          </div>
          <div className='span-2'>
            <Field.Number
              label='เบอร์ติดต่อ'
              name='phone'
              allowNegative={false}
              allowLeadingZeros
              maxLength={10}
              placeholder='กรุณากรอกหมายเลขโทรศัพท์'
              thousandSeparator={false}
            />
          </div>
          <div className='span-2'>
            <Field.Input
              label='ตำแหน่ง'
              name='rank'
              placeholder='กรุณากรอกตำแหน่ง'
            />
          </div>
          <div className='span-2'>
            <Field.Select
              label='สิทธิ'
              name='department'
              options={DEPARTMENT}
              placeholder='กรุณาเลือกสิทธิการใช้งาน'
            />
          </div>
          <div className='span-2'>
            <div className='flex column'>
              <Field.Input
                label='ชื่อผู้ใช้งาน (สำหรับเข้าสู่ระบบ)'
                name='username'
                placeholder='กรุณากรอกชื่อผู้ใช้งาน'
              />
              <Field.Password
                label='รหัสผ่าน'
                name='password_ph'
                placeholder='กรุณากรอกรหัสผ่าน'
              />
              <Field.Password
                label='ยืนยันรหัสผ่าน'
                name='password_confirm_ph'
                placeholder='กรุณากรอกรหัสผ่านอีกครั้ง'
              />
            </div>
          </div>
        </div>
        <hr />
        <br />
        <div className='flex justify-end'>
          <div className='grid-auto-fill-05 gap1 align-end' style={{ width: '100%', maxWidth: 320 }}>
            <div className='span-2'>
              <Link href={"/backoffice/user"}>
              <Button htmlType='button' type='default' className='w-100'>
                ยกเลิก
              </Button>
              </Link>
            </div>
            <div className='span-3'>
              <Button htmlType='submit' className='w-100'>
                {pathname === pathCreate ? "สร้างผู้ใช้งาน" : "แก้ไขผู้ใช้งาน"}
              </Button>
            </div>
          </div>
        </div>
      </Form>
    </div>
  )
}

const initialValues = {
  department: "",
  full_name: "",
  password: "",
  phone: "",
  rank: "",
  role: "",
  username: "",
}

const getInit = (init) => {
  if (init) {
    const result = {}
    Object.keys(initialValues).forEach((key) => {
      const value = result[key]
      result[key] = value
    })
    return {
      ...result,

    }
  }

  return initialValues
}

FormUserManage.propTypes = {
  init: PropTypes.object,
  dataEdit: PropTypes.object
}

export default FormUserManage
