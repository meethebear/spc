import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import FormUserManage from '../components/FormUserManage'
import FormUserCreate from '../components/FormUserCreate'
import { useFetchUserById } from "../api"
import { Row, Breadcrumb } from "antd"
import { HomeOutlined } from "@ant-design/icons";

const UserManageCreate = () => {

  const {query} = useRouter();

  const [dataEdit, setDataEdit] = useState(null)

  const [dataUserById, loadingUserVyId, fetchUserById] = useFetchUserById();

  console.log("dataUserById",query.id);

  useEffect(()=>{
    if(pathEdit){
    const fetchCustomerEdit = async(id)=>{
     const res = await fetchUserById(id);
     console.log("res",res);
     setDataEdit(res);
    }
    if(query?.id) {
      fetchCustomerEdit(query?.id)
     }
  } 
  },[query])

  const { pathname } = useRouter();
  const pathCreate = "/backoffice/user/create"
  const pathEdit = "/backoffice/user/edit"
  return (
    <div style={{ margin: '30px 80px' }}>
       <Row>
          <Breadcrumb>
            <Breadcrumb.Item href="/backoffice/user">
              <HomeOutlined />
              <span>ข้อมูลผู้ใช้งาน</span>
            </Breadcrumb.Item>
            <Breadcrumb.Item>{pathname === pathCreate ? "สร้างข้อมูลผู้ใช้งาน" : "แก้ไขข้อมูลผู้ใช้งาน"}</Breadcrumb.Item>
          </Breadcrumb>
        </Row>
       <div className='flex justify-between'>
        <h1 className='title-3'>{pathname === pathCreate ? "สร้างข้อมูลผู้ใช้งาน" : "แก้ไขข้อมูลผู้ใช้งาน"}</h1>
      </div>
     
      {pathname===pathEdit ? dataEdit && <FormUserManage dataEdit={dataEdit}/> : <FormUserCreate/>}
    </div>
  )
}

export default UserManageCreate