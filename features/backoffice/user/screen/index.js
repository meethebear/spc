import React, { useCallback, useState, useEffect, useMemo } from "react";
import Link from 'next/link'
import { PlusOutlined } from '@ant-design/icons';
// import Table from '@/components/TableWithPagination'
import DropDown from '@/components/DropDown'
import Button from '@/components/Button'
import FormSearch from '../components/FormSearch'
import moment from "moment";
import { Menu, Typography, Table, Pagination } from 'antd';
import { useRouter } from 'next/router';
import { useSelector } from "react-redux";
import { useFetchUser, useDeleteUser } from '../api';

const { SubMenu } = Menu;
const { Text } = Typography


const UserManage = () => {

  const [datausers, loadindfetchUser, fetchUser] = useFetchUser();
  const [loadindDeleteUser, DeleteUser] = useDeleteUser();

  const router = useRouter();

  const user = useSelector((state) => state.user);
  const users = useSelector((state) => state.users);
  const search = useMemo(() => users?.users?.filter || {}, [users])

  console.log('user',user);
  const defaultValue = useCallback(async (body = {}) => {
    await fetchUser({ ...search, ...body })
  }, [fetchUser, search])


  const handlerpage = (page) => {
    console.log(page);
    fetchUser({
      department: search?.department || null,
      name: search?.name || null,
      date: search?.date ? moment(search?.date).format('YYYY-MM-DD'): null,
      page: page,
      per_page: 10,
      total: users?.users?.data?.pagination?.total
    })

  }

  const RemoveUser = useCallback(async(id) => {
    const body = {
      id: id
    }
    DeleteUser(body);
    const { success } = fetchUser({
      department: search?.department || null,
      name: search?.name || null,
      date: search?.date ? moment(search?.date).format('YYYY-MM-DD'): null,
      page: 1,
      per_page: 10,
      total: users?.users?.data?.pagination?.total
    })
    if (success) {
      await defaultValue();
    }
  }, [DeleteUser, fetchUser, defaultValue])

  const columns = [
    {
      title: 'รายชื่อผู้ใช้งาน',
      dataIndex: "full_name",
      width: '20%'
    },
    {
      title: 'สิทธิผู้ใช้งาน',
      dataIndex: "department",
      width: '15%',
      align: 'center',
    },
    {
      title: 'วันที่สร้าง',
      dataIndex: "created_at",
      width: '15%',
      align: 'center',
      render: (text, record, index) => (
        moment(record.created_at).format("DD/MM/YYYY")
      )
    },
    {
      title: '',
      width: '20%',
      render: (text, record, index) => (
        <div className='flex align-center gap3'>
          <Link href={`/backoffice/user/${record.id}`}>
            <Button>
              {'รายละเอียด'}
            </Button>
          </Link>
          <DropDown
            icon='more'
            items={[
              {
                key: 1,
                label: <div className='p-dd-item'>แก้ไขข้อมูลผู้ใช้งาน</div>,
                onClick: () => { EditUser(record); }
              },
              {
                key: 2,
                label: <div className='p-dd-item'>ลบข้อมูลผู้ใช้งาน</div>,
                onClick: () => { RemoveUser(record.id) }
              }
            ]}
          />
        </div>
      )
    }
  ]

  const EditUser = (record) => {
    router.push(`/backoffice/user/edit?id=${record.id}`)
  }

  return (
    <div style={{ margin: 80 }}>
      <div className='flex justify-between'>
        <h1 className='title-3'>ผู้ใช้งานระบบ</h1>
        <div>
          <Link href={'/backoffice/user/create'}>
            <a className='ant-btn ant-btn-default ant-btn-lg btn-orange border-radius-6'><PlusOutlined /> สร้างข้อมูลผู้ใช้งาน</a>
          </Link>
        </div>
      </div>
      <FormSearch />
      <br />
      <div>
      <Table
        dataSource={users?.users?.data?.data}
        columns={columns}
        current={1}
        pageSize={10}
        loading={loadindfetchUser}
        pagination={false}
        scroll={{
          x: false,
          y: false,
        }}
      />
      </div>
      <br />
      <Pagination
        page={users?.users?.data?.pagination?.page}
        defaultCurrent={1}
        // pageSize={users?.users?.data?.pagination?.per_page}
        total={users?.users?.data?.pagination?.total}
        onChange={handlerpage}
      />
    </div>
  )
}

export default UserManage