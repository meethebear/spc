import { useState, useEffect } from 'react'
import LabelBoxInfo from '../components/LabelBoxInfo'
import Button from '@/components/Button'
import ModalResetPassword from '../components/ModalResetPassword'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { useFetchUserById } from "../api"

const INIT_MODAL = { open: false, id: null }
const InfoUser = () => {

  const [dataUserById, loadingUserVyId, fetchUserById] = useFetchUserById();

  const {query} = useRouter();

  useEffect(()=>{
    const fetchUser = async(id)=>{

     const res = await fetchUserById(id)
     return res
    }

    if(query?.id) {
     const data = fetchUser(query?.id)
     console.log("datacustomer",data);
    }
  },[query])

  console.log("dataUserById",dataUserById);

  const [modalResetPassword, setModalResetPassword] = useState(INIT_MODAL)
  return (
    <div style={{ margin: '34px 80px' }} className='flex column align-center'>
      <div className='flex justify-between w-100' style={{ maxWidth: '1032px' }}>
        <h1 className='title-3'>รายละเอียดผู้ใช้งาน</h1>
      </div>
      <div className='bg-card-form w-100' style={{ maxWidth: '1032px' }}>
        <div>
          <h2>ข้อมูลผู้ใช้งาน</h2>
        </div>
        <hr />
        <div className='grid-auto-fill-2 gap1' style={{ marginTop: '1rem' }}>
          <LabelBoxInfo
            label='ชื่อ-นามสกุล'
            value={dataUserById?.full_name}
          />
          <LabelBoxInfo
            label='เบอร์ติดต่อ'
            value={dataUserById?.phone}
          />
          <LabelBoxInfo
            label='ตำแหน่ง'
            value={dataUserById?.rank}
          />
          <LabelBoxInfo
            label='สิทธิ'
            value={dataUserById?.department}
          />
          <LabelBoxInfo
            label='ชื่อผู้ใช้งาน'
            value={dataUserById?.username}
          />
        </div>
        <hr />
        <div className='flex justify-end' style={{ marginTop: 20 }}>
          <div className='grid-auto-fill-05 gap1 align-end' style={{ width: '100%', maxWidth: 580 }}>
            <div className='span-3'>
              <Button htmlType='button' type='default' className='w-100'onClick={() => setModalResetPassword({ open: true, id: null })}>
                แก้ไขรหัสผ่าน
              </Button>
            </div>
            <div className='span-3'>
              <Link href={`/backoffice/user/edit?id=${query.id}`}>
              <Button htmlType='button' type='default' className='w-100'>
                แก้ไขข้อมูลผู้ใช้
              </Button>
              </Link>
            </div>
            <div className='span-3'>
              <Link href={"/backoffice/user/"}>
              <Button htmlType='submit' className='w-100'>
                กลับหน้าแรก
              </Button>
              </Link>
            </div>
          </div>
        </div>
      </div>

      <ModalResetPassword
        open={modalResetPassword.open}
        onCancel={() => setModalResetPassword(INIT_MODAL)}
        id={modalResetPassword.id}
      />
    </div>
  )
}

export default InfoUser
