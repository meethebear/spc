import { useState, useEffect, useCallback } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector } from "react-redux";
import { resError, resOK } from "@/utils/response";

const useFetchUserById = () => {
  const name = "fetchUserById";
  const API = useAPI(name, "overlay");
  const [result, useResult ] = useState();
  const { tasksRunning } = useSelector(
    selector(["tasksRunning"])
  );

  const fetchUserById = useCallback(async (id) => {
    try {
      API.begin();
      const { data } = await API.get(`/user/${id}`);
      const res = resOK(data);
      useResult(res)
      return res;
    } catch (error) {
      const e = resError(error);
      API.error(e);
    } finally {
      API.end();
    }
    return {
      success: false
    }
  }, []);

  return [result?.data,tasksRunning?.[name]?.loading, fetchUserById];
};

export default useFetchUserById;
