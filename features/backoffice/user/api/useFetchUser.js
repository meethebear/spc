import { useCallback, useEffect } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector, useDispatch } from "react-redux";
import { resError, resOK } from "@/utils/response";
import { actionIndexUser } from '@/actions/users.action';

const useFetchUser = () => {
  const name = 'fetchUser'
  const API = useAPI(name, 'none')
  const { customer, tasksRunning } = useSelector(selector(['customer', 'tasksRunning']))
  const useThunkDispatch = () => useDispatch()
  const dispatch = useThunkDispatch()

  const fetchUser = useCallback(async (filter = {}) => {
    try {
      API.begin()
      const { data } = await API.get('/user', { params: { ...filter } })
      const res = resOK(data)
      dispatch(actionIndexUser({
        filter: { page: res.data.page, per_page: res.data.per_page, ...filter, }, data: res.data
      }))
    } catch (error) {
      const e = resError(error)
      API.error(e)
    } finally {
      API.end()
    }
  }, [])

  useEffect(() => {
    fetchUser({ page: 1, per_page: 10 })
  }, [])

  return [customer?.customer, tasksRunning?.[name]?.loading, fetchUser]
}

export default useFetchUser