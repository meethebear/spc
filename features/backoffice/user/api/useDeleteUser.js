import { useCallback } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector, useDispatch } from "react-redux";
import { resError, resOK } from "@/utils/response";
import { message } from 'antd';


const useDeleteUser = () => {
  const name = 'deleteDataUser'
  const API = useAPI(name, 'overlay')
  const useThunkDispatch = () => useDispatch()
  const dispatch = useThunkDispatch()
  const { tasksRunning } = useSelector(selector(["tasksRunning"]));

  const deleteDataUser = useCallback(async (values) => {
    try {
      API.begin()
      const { data } = await API.delete(`/user`, { data: values }, { auth: true })
      const res = resOK(data)
      message.success(res.message)
      return res
    } catch (error) {
      const e = resError(error)
      message.error(e.errorMessage)
      API.error(e)
    } finally {
      API.end()
    }
  }, [])

  return [tasksRunning?.[name]?.loading, deleteDataUser]
}
export default useDeleteUser