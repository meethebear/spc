import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector } from "react-redux";
import { resError, resOK } from "@/utils/response";
import { message } from 'antd'

const usePutUser = () => {
  const name = "useUpdateUser";
  const API = useAPI(name, 'none')
  const { tasksRunning } = useSelector(selector(["tasksRunning"]));

  const editUser = async (body) => {
    console.log("bodyAPI",body);
    try {
      API.begin();
      const { data } = await API.put(`/user`, body)
      const res = resOK(data)
      message.success("บันทึกแล้ว")
      return res
    } catch (error) {
      const e = resError(error);
      API.error(e);
      message.error(e?.errorMessage);
    } finally {
      API.end();
    } 
    return {
      success: false
    }
  };

  return [tasksRunning?.[name]?.loading, editUser];
};

export default usePutUser;