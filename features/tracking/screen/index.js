import { Fragment, useCallback } from "react";
import Image from "next/image";
import SearchBar from '../components/SearchBar'

const Home = () => {
  return (
    <Fragment>
      <SearchBar

      />
      <div className="flex column align-center">
        <Image
          src='/images/transportation_tracking.png'
          alt="transportation_tracking"
          width={100}
          height={100}
        />
        <h1 className='text-size-15 text-weight-500'>ไม่พบข้อมูลที่ตรงกัน</h1>
        <p className="text-subtext2">กรุณาตรวจสอบข้อมูลเลขที่/รหัสการสั่งซื้อให้ถูกต้อง</p>
      </div>

    </Fragment>
  );
};

export default Home;
