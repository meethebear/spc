import { Fragment, useCallback, useMemo } from "react";
import Image from "next/image";
import { Steps, Typography } from 'antd';
import Table from '@/components/Table'
import SearchBar from '../components/SearchBar'
import LabelBoxInfo from '@/features/backoffice/order/components/LabelBoxInfo.js'
import stf from '@/utils/stringFormat'
import useGetAPI from '@/utils/hook/useGetAPI'
import { STATUS_ORDER_MAP_STEP } from '@/utils/constant'
const { Link } = Typography

const { Step } = Steps;

const Text = props => {
  const { text } = props
  if (text || text === 0) {
    return text
  }

  return '-'
}

const Info = props => {
  const { code } = props
  const [info, loading] = useGetAPI('tracking', `/order/tracking/${code}`, true, false)

  const columns = [
    {
      title: 'ชื่อสินค้า',
      dataIndex: 'name',
      key: 'name',
      width: '60%'
    },
    {
      title: 'จำนวน',
      dataIndex: 'quantity',
      key: 'quantity',
      align: 'center',
    },
    {
      title: 'หน่วย',
      dataIndex: 'unit',
      key: 'unit',
      align: 'center',
    },
    {
      title: 'ราคา',
      dataIndex: 'amount',
      key: 'amount',
      align: 'center',
      render: (value) => stf(value).normal(2) + '฿'
    },
  ]

  const currentStep = useMemo(() => {
    if (!info) {
      return 1
    }
    return STATUS_ORDER_MAP_STEP[info.status]
  }, [info])

  return (
    <Fragment>
      <SearchBar
        init={{ search: code }}
      />

      <div className="bg-card-tracking">
        <Steps current={currentStep}>
          <Step
            title="สร้างรายการจัดส่ง"
            description={`วันที่: ${stf(info?.updated_status?.PRODUCTION_PENDING?.at).date('DD/MM/YYYY HH:mm')}`}
          />
          <Step
            title="จัดเตรียมสินค้า"
            description={`วันที่: ${stf(info?.updated_status?.PRODUCTION_FINISHED?.at).date('DD/MM/YYYY HH:mm')}`}
          />
          <Step
            title="จัดส่งสินค้าสำเร็จ"
            description={`วันที่: ${stf(info?.updated_status?.TRANSPORTATION_FINISHED?.at || info?.updated_status?.TRANSPORTATION_FAILED?.at).date('DD/MM/YYYY HH:mm')}`}
          />
        </Steps>
      </div>
      <br />
      <br />
      <div className='bg-card-tracking'>
      <div className='flex justify-between wrap'>
        <div style={{ minWidth: '200px' }}>
          <h2 className='mb-0'>{info?.code}</h2>
          <p className='text-desc-form mb-10'>รหัสงาน</p>
        </div>
        <div>
          <h2 className='mb-0'>จัดส่งภายในวันที่</h2>
          <p className='text-desc-form mb-10 text-end'><Text text={stf(info?.date_to_transportation).date('DD/MM/YYYY')} /></p>
        </div>
      </div>
      <hr />
      <div>
        <h2>ข้อมูลลูกค้า</h2>
      </div>
      <div className='flex column gap1'>
        <LabelBoxInfo
          label='ชื่อบริษัท'
          value={<Text text={info?.customer?.customer_company_name} />}
        />
        <LabelBoxInfo
          label='ผู้ติดต่อ'
          value={<Text text={info?.customer?.contact_name} />}
        />
        <LabelBoxInfo
          label='ที่อยู่ลูกค้า'
          value={<Text text={`${info?.address || ''} ${info?.sub_district || ''} ${info?.district || ''} ${info?.province || ''} ${info?.post_code || ''}`} />}
        />
        <LabelBoxInfo
          label='Lat/Long'
          value={info?.latitude && info?.longitude && (
            <Link href={`https://www.google.com/maps/search/?api=1&quantityuery=${info.latitude},${info.longitude}`} target='_blank'>(ละติจูด) ,(ลองจิจูด)</Link>
          )}
        />
      </div>
      <br />
      <hr />
      <div style={{ marginTop: '1rem' }}>
        <h2>รายละเอียดสินค้า</h2>
      </div>
      <div>
        <Table
          columns={columns}
          data={info?.products || []}
          pagination={false}
          bordered
          scroll={{ x: 500 }}
        />
      </div>
      <br />
    </div>
    <br />

    <br />

    </Fragment>
  );
};

export default Info;
