import { Fragment, useCallback } from "react";
import { Button } from "antd";
import { Form, Field, useForm, getMessageReq } from "@/components/form";
import styles from "../styles/Tracking.module.css";


const initialValues = {
  search: "",
};
const rules = {
  search: {
    required: getMessageReq('Code Tracking', 'Code Tracking'),
  },
};

const SearchBar = props => {
  const { init } = props
  const form = useForm({ initialValues: init || initialValues, rules });

  const onSearch = useCallback(async (values) => {
    location.assign('/tracking/' + values.search)
  }, []);

  return (
    <Fragment>
      <Form form={form} handlerSubmit={onSearch}>
        <div className={styles.input_search_bar}>
          <Field.Input
            name="search"
            className={styles.input_search}
            // placeholder="Maximum 30 tracking numbers, support “, “ ”Space”..."
            placeholder="140987654321"
            allowClear
          />
          <Button htmlType="submit" className={`btn-orange ${styles.btn_search} `}>แสดงข้อมูล</Button>
        </div>
      </Form>
    </Fragment>
  );
};

export default SearchBar;
