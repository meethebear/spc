import { Fragment, useCallback, useEffect, useState } from "react";
import Image from "next/image";
import { useSelector } from "react-redux";
import { useForm, Form, Field } from "@/components/form";
import { Button, Typography, Alert } from "antd";
import style from "../styles/login.module.scss";
import { PasswordLock, User } from "@/components/icons";
import { useLogin } from "../api/useLoginApi";
import { useRouter } from "next/router";
import selector from "@/utils/selector";
import moment from 'moment'
import PropTypes from 'prop-types'

const { Title, Text } = Typography;

const initialValues = {
  username: "",
  password: "",
};
const rules = {
  username: {
    required: true,
  },
  password: {
    required: true,
  },
};

const Login = props => {
  const { error } = props
  const [errorMsg, setErrorMsg] = useState(error.message || '')
  const { replace } = useRouter();
  const { user } = useSelector(
    selector(["user"])
  );
  const login = useLogin();

  const form = useForm({
    initialValues,
    rules,
    onValuesUpdate: () => {
      if (errorMsg) {
        setErrorMsg('')
      }
    }
  });

  useEffect(() => {
    if (user.token) {
      replace("/backoffice");
    }
  }, [user.token, replace]);

  const onLogin = useCallback(() => {
    document.getElementById('form-login-photon').submit()
  }, [login, replace])

  return (
    <Fragment>
      <div className={style.login_pane}>
        <div className={style.login_top_bg}>
          <Image src="/images/bg_login.png" alt="" width={1440} height={1000} />
        </div>
        {/* <img src="/images/bg_login_grid.png" alt="" className={style.bottom_login_img} /> */}
        <div className={style.login_logo}>
          <Image
            src="/images/logo.svg"
            alt="photon-express"
            width={200}
            height={100}
          />
        </div>
        <div className={style.login_card}>
          <div className={style.login_title}>
            <Title
              level={1}
              style={{ fontWeight: 800 }}
              className={style.header_text}
            >
              เข้าสู่ระบบ
            </Title>
            <Text className={style.subheader_text}>
              กรุณาระบุข้อมูลเพื่อดำเนินการต่อ
            </Text>
          </div>
          {!!errorMsg && (
            <div className={style.box_alert_error}>
              <Alert message={errorMsg} type="error" />
            </div>
          )}
          <Form
            form={form}
            handlerSubmit={onLogin}
            id={"form-login-photon"}
            className={style.formlogin}
            method={'POST'}
            action={`/api/login`}
          >
            <Field.Input
              type="text"
              name="username"
              label="ชื่อผู้ใช้งาน"
              placeholder="ชื่อผู้ใช้"
              star={false}
              prefix={<User />}
            />
            <Field.Password
              name="password"
              label="รหัสผ่าน"
              placeholder="รหัสผ่าน"
              star={false}
              prefix={<PasswordLock />}
            />
            {/* <Button type="link">ลืมรหัสผ่าน?</Button> */}
            <Button
              htmlType="submit"
              disabled={form.submitting}
              className={style.buttonlogin}
              block
            >
              <p className={style.buttonlogintext}>เข้าสู่ระบบ</p>
            </Button>
            <div className={style.login_title}>
              <Text>&copy; {moment().format('YYYY')} All Rights Reserved</Text>
            </div>
          </Form>
        </div>
      </div>
    </Fragment>
  );
};

Login.propTypes = {
  error: PropTypes.object
}

export default Login;
