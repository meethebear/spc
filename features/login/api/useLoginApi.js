import { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import useAPI from '@/utils/hook/useAPI'
import { resOK, resError } from '@/utils/response'
import { userLogin } from '@/actions/user.action'
// import { userRefreshToken } from '@/app/actions/user.action'

export const useLogin = () => {
  const dispatch = useDispatch()
  const API = useAPI('login', 'overlay')
  const login = useCallback(async (values) => {
    try {
      API.begin()
      const body = {
        username: values.username,
        password: values.password
      }
      const { data } = await API.post('/user/sign_in', body, { auth: false })
      const res = resOK(data)
      if (res.success) {
        dispatch(userLogin(res.data))
      }
      return res
    } catch (error) {
      const e = resError(error)
      API.error(e.errorMessages, e.errorMessage, e.errorCode, e.data)
      return { success: false, ...e }
    } finally {
      API.end()
    }
  }, [API, dispatch])

  return login
}

// export const useRefaceToken = () => {
//   const dispatch = useDispatch()
//   const API = useAPI('reface_token', 'none')
//   const reface_token = useCallback(async (values) => {
//     try {
//       API.begin()
//       const { data } = await API.get('/users/refresh_token')
//       const res = resOK(data)
//       if (res.success) {
//         dispatch(userRefreshToken(res.data))
//       }
//       return res
//     } catch (error) {
//       const e = resError(error)
//       API.error(e.errorMessages, e.errorMessage, e.errorCode, e.data)
//       return { success: false, ...e }
//     } finally {
//       API.end()
//     }
//   }, [API, dispatch])

//   return reface_token
// }
