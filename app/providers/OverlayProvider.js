import PropTypes from 'prop-types'
// import Spin from 'antd/lib/spin'
import { LoadingOutlined } from '@ant-design/icons'
import styles from '@/styles/Overlay.module.css'

const OverlayProvider = props => {
  const { loading, children, 
    // ...propsSpin
  } = props
  const antIcon = <LoadingOutlined style={{ fontSize: 34 }} spin size={'large'} />
  return (
    // <Spin spinning={true} indicator={antIcon} className='123' {...propsSpin}>
    //   {children}
    // </Spin>
    <>
      {loading && (
        <div className={styles.container}>
          {antIcon}
        </div>
      )}
      <div className={loading ? styles.content : ''}>
        {children}
      </div>
    </>
  )
}

OverlayProvider.propTypes = {
  loading: PropTypes.bool,
  prefixCls: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
  size: PropTypes.oneOf(['small', 'large', 'default']),
  tip: PropTypes.string,
  delay: PropTypes.number,
  wrapperClassName: PropTypes.string,
  indicator: PropTypes.element,
  children: PropTypes.node
}

OverlayProvider.defaultProps = {
  loading: false
}

export default OverlayProvider
