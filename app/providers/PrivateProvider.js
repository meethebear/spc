import React, { useEffect, useMemo } from 'react'
import { useStore, useSelector } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import Spin from 'antd/lib/spin'
import Page404 from '../../pages/404'
import useRefreshToken from '@/utils/hook/useRefreshToken'
import pm from '@/utils/permission'
import { useRouter } from 'next/router'

const PContent = props => {
  const { children, roles, session, department } = props
  const userStore = useSelector(state => state?.user || {})
  const user = useMemo(() => userStore?.token ? userStore : session || {}, [session, userStore])
  const his = useRouter()
  const token = user?.token || null
  useRefreshToken()

  useEffect(() => {
    if (!token) {
      his.replace('/login')
    }
  }, [his, token])

  if (!token) {
    return null
  }

  return (
    <>
      {children}
    </>
  )
}

const PrivateProvider = props => {
  const store = useStore()
  return (
    <PersistGate
      persistor={store.__persistor}
      loading={<Spin spinning><div style={{ width: '100vw', height: '100vh' }}></div></Spin>}
    >
      <PContent {...props} />
    </PersistGate>
  )
}

export default PrivateProvider

export const ProviderSession = props => {
  const store = useStore()
  return (
    <PersistGate
      persistor={store.__persistor}
      loading={<Spin spinning><div style={{ width: '100vw', height: '100vh' }}></div></Spin>}
    >
      {props.children}
    </PersistGate>
  )
}
