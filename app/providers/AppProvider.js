import { useMemo } from 'react'
import dynamic from 'next/dynamic'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import OverlayProvider from './OverlayProvider'
import PrivateProvider from './PrivateProvider'
import getTasksRunning from '../../utils/getTasksRunning'
import useCancelTokenAxios from '../../utils/hook/useCancelTokenAxios'

const ModalResult = dynamic(() => import('@/components/modals/GlobalsModalResult'), { ssr: false })

const AppProvider = props => {
  const { publicPage, useModals, children, roles, department } = props
  const tasksRunning = useSelector((state) => state.tasksRunning)
  useCancelTokenAxios()

  const loading = useMemo(() => {
    return getTasksRunning(tasksRunning)?.loadingOverlay !== false
  }, [tasksRunning])

  return (
    <OverlayProvider loading={loading}>
      {publicPage
        ? children
        : (
          <PrivateProvider roles={roles} department={department}>{children}</PrivateProvider>
        )}
      {useModals.includes('result') && (
        <ModalResult />
      )}
    </OverlayProvider>
  )
}

AppProvider.propTypes = {
  publicPage: PropTypes.bool,
  children: PropTypes.element.isRequired,
  useModals: PropTypes.array,
  roles: PropTypes.array,
  department: PropTypes.array
}

AppProvider.defaultProps = {
  publicPage: false,
  useModals: []
}

export default AppProvider
