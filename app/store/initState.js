import {
  INITIAL_STATE_USER,
  INITIAL_STATE_TASKS_RUNNING,
  INITIAL_STATE_MODAL,
  INITIAL_STATE_MASTER
} from './reducers'

const init_state = {
  user: INITIAL_STATE_USER,
  modal: INITIAL_STATE_MODAL,
  tasksRunning: INITIAL_STATE_TASKS_RUNNING,
  master: INITIAL_STATE_MASTER
}

export default init_state
