import { combineReducers } from 'redux'
import * as appReducers from './reducers'
import { actionTypes } from './reducers/user.reducer'
import init_state from './initState'

const appReducer = combineReducers({
  ...appReducers
})

const rootReducer = (state, action) => {
  switch (action.type) {
  case actionTypes.USER_SIGN_OUT:
    state = {
      ...init_state
    }
    return state
  default:
    break
  }

  return appReducer(state, action)
}

export default rootReducer
