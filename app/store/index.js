import { createStore, applyMiddleware } from 'redux'
import { createWrapper } from 'next-redux-wrapper'
import reduxThunk from 'redux-thunk'
import rootReducer from './rootReducers'
import init_state from './initState'
import logger from 'redux-logger'

const middleware = [reduxThunk]

if (process.env.NODE_ENV === 'development') {
  middleware.unshift(logger)
}

const makeConfiguredStore = reducer => createStore(reducer, init_state, (applyMiddleware(...middleware)))

// create a makeStore function
const makeStore = () => {
  const isServer = typeof window === 'undefined'
  if (isServer) {
    return makeConfiguredStore(rootReducer)
  } else {
    // we need it only on client side
    const { persistStore, persistReducer } = require('redux-persist')
    const storage = require('redux-persist/lib/storage').default

    const persistConfig = {
      key: 'spc',
      whitelist: ['user'], // make sure it does not clash with server keys
      storage
    }

    const persistedReducer = persistReducer(persistConfig, rootReducer)
    const store = makeConfiguredStore(persistedReducer)

    store.__persistor = persistStore(store) // Nasty hack

    return store
  }
}

// export an assembled wrapper
export const wrapper = createWrapper(makeStore, { debug: process.env.NODE_ENV === 'development' })
