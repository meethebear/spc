import { HYDRATE } from 'next-redux-wrapper'

export const actionTypes = {
  MODAL_OPEN: 'MODAL_OPEN',
  MODAL_CLOSE: 'MODAL_CLOSE'
}

export const INITIAL_STATE = {
  type: null,
  open: false,
  title: '',
  description: '',
  onCancel: null,
  onConfirm: null,
  txtCancel: 'Cancel',
  txtConfirm: 'Confirm'
}

export const TYPE = {
  success: 'RESULT_SUCCESS',
  failed: 'RESULT_FAILED',
  confirm: 'CONFIRM'
}

const modalReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
  case HYDRATE:
    return { ...state, ...action.payload?.modal }
  case actionTypes.MODAL_OPEN: {
    return {
      open: true,
      type: action.payload.type, // error, warning, success
      title: action.payload.title,
      description: action.payload.description,
      onCancel: action.payload.onCancel,
      onConfirm: action.payload.onConfirm,
      txtCancel: typeof action.payload.txtCancel === 'undefined' ? INITIAL_STATE.txtCancel : action.payload.txtCancel,
      txtConfirm: typeof action.payload.txtConfirm === 'undefined' ? INITIAL_STATE.txtConfirm : action.payload.txtConfirm
    }
  }
  case actionTypes.MODAL_CLOSE:
    return INITIAL_STATE
  default:
    return state
  }
}

export const selector = state => state.modal

export default modalReducer
