import { HYDRATE } from 'next-redux-wrapper'

export const actionTypes = {
  FETCH_USER_TRANSPORTATION: 'FETCH_USER_TRANSPORTATION',
  FETCH_CUSTOMER: 'FETCH_CUSTOMER',
}

export const INITIAL_STATE = {
  user_transportation: [],
  customer: []
}

const MasterReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case HYDRATE:
      return { ...state, ...action.payload?.master }
    case actionTypes.FETCH_USER_TRANSPORTATION:
      return {
        ...state,
        user_transportation: action.payload
      }
    case actionTypes.FETCH_CUSTOMER:
      return {
        ...state,
        customer: action.payload
      }
    default:
      return state
  }
}

export const selector = state => state.master


export default MasterReducer
