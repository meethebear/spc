import { HYDRATE } from 'next-redux-wrapper'

export const actionTypes = {
  USER_SIGN_IN: 'USER_SIGN_IN',
  USER_REFRESH_TOKEN: 'USER_REFRESH_TOKEN',
  USER_SIGN_OUT: 'USER_SIGN_OUT',
  USER_TOKEN_EXPIRE: 'USER_TOKEN_EXPIRE',
  USER_SIGN_IN_ERROR: 'USER_SIGN_IN_ERROR',
  USER_RESET_EXPIRED: 'USER_RESET_EXPIRED',
  USER_CLEAR_SIGN_IN_ERROR: 'USER_CLEAR_SIGN_IN_ERROR',
}

export const INITIAL_STATE = {
  token: null,
  role: null,
  username: null,
  refreshedDate: null,
  loggedInDate: null,
  expiringDate: null,
  expiredDate: null,
  error: null,
  menu: [],
}

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
  case HYDRATE:
    return { ...state, ...action.payload?.user }
  case actionTypes.USER_SIGN_IN: {
    return {
      ...action.payload,
      refreshedDate: null,
      expiredDate: null
    }
  }
  case actionTypes.USER_REFRESH_TOKEN: {
    const now = new Date()
    return {
      ...state,
      ...action.payload,
      refreshedDate: now,
      expiredDate: null
    }
  }
  case actionTypes.USER_TOKEN_EXPIRE: {
    const now = new Date()
    localStorage.setItem('expiredFromURL', location.pathname)
    location.replace('/api/logout')
    return {
      ...INITIAL_STATE,
      expiredDate: now,
      expiredFromURL: location.pathname
    }
  }
  case actionTypes.USER_SIGN_IN_ERROR:
    return {
      ...state,
      error: action.payload
    }
  case actionTypes.USER_RESET_EXPIRED: {
    return {
      ...state,
      expiredDate: null
    }
  }
  case actionTypes.USER_CLEAR_SIGN_IN_ERROR:
    return {
      ...state,
      error: INITIAL_STATE.error
    }
  case actionTypes.USER_SIGN_OUT:
    return INITIAL_STATE
  default:
    return state
  }
}

export const selector = state => state.user

export default userReducer
