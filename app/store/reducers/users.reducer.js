import moment from 'moment'
import { HYDRATE } from 'next-redux-wrapper'


export const actionTypes = {
  USER_INDEX: 'USER_INDEX',
}

const INITIAL_STATE = {
  users: {
    filter: {
      date: '',
      department: '',
      name: '',
      page: 1,
      per_page: 10,
    },
    data: []
  },
}

const UserReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case HYDRATE:
      return { ...state, ...action.payload?.user }
    case actionTypes.USER_INDEX:
      return {
        ...state,
        users: {
          filter: action.payload.filter || { ...state.users.filter },
          data: action.payload.data
        }
      }
    default:
      return state
  }
}

export const selector = state => state.users


export default UserReducer