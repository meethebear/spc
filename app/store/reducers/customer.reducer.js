import moment from 'moment'
import { HYDRATE } from 'next-redux-wrapper'


export const actionTypes = {
  CUSTOMER_INDEX: 'CUSTOMER_INDEX',
}

const INITIAL_STATE = {
  customer: {
    filter: {
      page: 1,
      per_page: 10,
      name: '',
      sort_name: null,
      type: null,
      date: moment().format('YYYY-MM-DD')
    },
    data: []
  },
}

const CustomerReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case HYDRATE:
      return { ...state, ...action.payload?.customer }
    case actionTypes.CUSTOMER_INDEX:
      return {
        ...state,
        customer: {
          filter: action.payload.filter || { ...state.customer.filter },
          data: action.payload.data
        }
      }
    default:
      return state
  }
}

export const selector = state => state.customer


export default CustomerReducer