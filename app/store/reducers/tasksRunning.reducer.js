import { HYDRATE } from 'next-redux-wrapper'

export const actionTypes = {
  TASKS_START: 'TASKS_START',
  TASKS_FINISHED: 'TASKS_FINISHED',
  TASKS_ERROR: 'TASKS_ERROR',
  TASKS_CLEAR: 'TASKS_CLEAR',
  TASKS_CLEAR_ALL: 'TASKS_CLEAR_ALL',
  TASKS_SET_DATA: 'TASKS_SET_DATA'
}

export const INITIAL_STATE = {}

const tasksRunningReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
  case HYDRATE:
    return { ...state, ...action.payload?.tasksRunning }
  case actionTypes.TASKS_START:
    return {
      ...state,
      [action.name]: {
        loading: true,
        loadType: action.loadType,
        cancelToken: action.cancelToken
      }
    }
  case actionTypes.TASKS_FINISHED:
    return {
      ...state,
      [action.name]: {
        ...state[action.name],
        loading: false,
        cancelToken: null
      }
    }
  case actionTypes.TASKS_ERROR:
    return {
      ...state,
      [action.name]: {
        loading: false,
        message: action.payload.message,
        message_th: action.payload.message_th
      }
    }
  case actionTypes.TASKS_CLEAR: {
    const tempData = Object.assign({}, state)
    delete tempData[action.actionName]
    return { ...tempData }
  }
  case actionTypes.TASKS_CLEAR_ALL:
    state = INITIAL_STATE
    return state
  case actionTypes.TASKS_SET_DATA:
    return {
      ...state,
      [action.name]: {
        loading: false,
        data: action.data || null
      }
    }
  default:
    return state
  }
}

export const selector = state => state.tasksRunning

export default tasksRunningReducer
