export { default as user, INITIAL_STATE as INITIAL_STATE_USER } from './user.reducer'
export { default as tasksRunning, INITIAL_STATE as INITIAL_STATE_TASKS_RUNNING } from './tasksRunning.reducer'
export { default as modal, INITIAL_STATE as INITIAL_STATE_MODAL } from './modal.reducer'
export { default as master, INITIAL_STATE as INITIAL_STATE_MASTER } from './master.reducer'

export { default as order } from "./order.reducer";
export { default as customer } from "./customer.reducer";
export { default as users } from "./users.reducer";
export { default as dashboard } from "./dashboard.reducer";

