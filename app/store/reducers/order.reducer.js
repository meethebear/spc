import moment from 'moment'
import { HYDRATE } from 'next-redux-wrapper'

export const actionTypes = {
  ORDER_INDEX: 'ORDER_INDEX',
  PUT_ORDER_BY_ID: 'PUT_ORDER_BY_ID'
}

const INITIAL_STATE = {
  order: {
    filter: {
      page: 1,
      page_size: 20,
      search: '',
      status: null,
      company_id: null,
      date: moment().format('YYYY-MM-DD')
    },
    data: []
  },

}

const OrderReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case HYDRATE:
      return { ...state, ...action.payload?.order }
    case actionTypes.ORDER_INDEX:
      return {
        ...state,
        order: {
          filter: action.payload.filter || { ...state.order.filter },
          data: action.payload.data
        }
      }
    default:
      return state
  }
}

export const selector = state => state.order


export default OrderReducer