import { HYDRATE } from 'next-redux-wrapper'

export const actionTypes = {
  DASHBOARD_INDEX: 'DASHBOARD_INDEX'
}

const INITIAL_STATE = {
  dashboard: {
    filter: {
      page: 1,
      page_size: 20,
      search: '',
    },
    data: [],
    
  },
}

const DashboardReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case HYDRATE:
      return { ...state, ...action.payload?.dashboard }
    case actionTypes.DASHBOARD_INDEX:
      return {
        ...state,
        dashboard: {
          filter: action.payload.filter || { ...state.dashboard.filter },
          data: action.payload.data
        }
      }
    default:
      return state
  }
}

export const selector = state => state.dashboard


export default DashboardReducer