import { actionTypes } from '../store/reducers/tasksRunning.reducer'

export const taskStart = (name, loadType, cancelToken) => ({
  type: actionTypes.TASKS_START,
  name,
  loadType,
  cancelToken
})

export const taskFinished = (name) => ({
  type: actionTypes.TASKS_FINISHED,
  name
})

export const taskError = (name, message, message_th) => ({
  type: actionTypes.TASKS_ERROR,
  name,
  payload: {
    message,
    message_th
  }
})

export const taskClear = (name) => ({
  type: actionTypes.TASKS_CLEAR,
  name
})

export const taskClearAll = () => ({
  type: actionTypes.TASKS_CLEAR_ALL
})

export const taskSetData = (name, data) => ({
  type: actionTypes.TASKS_SET_DATA,
  name,
  data
})
