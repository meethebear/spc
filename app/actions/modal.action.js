import { actionTypes } from '../store/reducers/modal.reducer'
export { TYPE } from '../store/reducers/modal.reducer'

export const openModal = (payload) => ({
  type: actionTypes.MODAL_OPEN,
  payload
})

export const closeModal = () => ({
  type: actionTypes.MODAL_CLOSE
})
