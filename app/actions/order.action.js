import { actionTypes } from '../store/reducers/order.reducer'

export const actionIndexOrder = data => async (dispatch, getState) => await dispatch({
  type: actionTypes.ORDER_INDEX,
  payload: data
})
