import { actionTypes } from '../store/reducers/dashboard.reducer'

export const actionIndexDashboard = data => async (dispatch, getState) => await dispatch({
  type: actionTypes.DASHBOARD_INDEX,
  payload: data
})