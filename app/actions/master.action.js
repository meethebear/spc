import { actionTypes } from '../store/reducers/master.reducer'

export const actionFetchUserTransportation = data => async (dispatch) => await dispatch({
  type: actionTypes.FETCH_USER_TRANSPORTATION,
  payload: data
})

export const actionFetchCustomer = data => async (dispatch) => await dispatch({
  type: actionTypes.FETCH_CUSTOMER,
  payload: data
})