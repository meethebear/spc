import { actionTypes } from '../store/reducers/customer.reducer'

export const actionIndexCustomer = data => async (dispatch, getState) => await dispatch({
  type: actionTypes.CUSTOMER_INDEX,
  payload: data
})
