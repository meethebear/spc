import { actionTypes } from '../store/reducers/user.reducer'

export const userLogin = data => ({
  type: actionTypes.USER_SIGN_IN,
  payload: data
})

export const userRefreshToken = data => ({
  type: actionTypes.USER_REFRESH_TOKEN,
  payload: data
})

export const userTokenExpire = () => ({
  type: actionTypes.USER_TOKEN_EXPIRE
})

export const userSignOut = () => ({
  type: actionTypes.USER_SIGN_OUT
})

export const onSignInError = (payload) => ({
  type: actionTypes.USER_SIGN_IN_ERROR,
  payload
})

export const userResetExpired = (payload) => ({
  type: actionTypes.USER_RESET_EXPIRED,
  payload
})

export const clearSignInError = (payload) => ({
  type: actionTypes.USER_CLEAR_SIGN_IN_ERROR,
  payload
})

