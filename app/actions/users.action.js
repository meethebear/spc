import { actionTypes } from '../store/reducers/users.reducer'

export const actionIndexUser = (payload) => ({
  type: actionTypes.USER_INDEX,
  payload
})