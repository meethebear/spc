import { useCallback, useEffect } from 'react'
import { useSelector, useDispatch } from "react-redux";
import selector from "@/utils/selector";
import useAPI from '@/utils/hook/useAPI'
import { resError, resOK } from "@/utils/response";
import { actionFetchCustomer } from '@/actions/master.action'

const useGetCustomer = (fetchDidMount = true) => {
const name = 'apiGetMasterCustomer'
  const API = useAPI(name, 'none')
  const { tasksRunning, master } = useSelector(selector(["tasksRunning", "master"]));
  const useThunkDispatch = () => useDispatch()
  const dispatch = useThunkDispatch()

  const apiGetMasterCustomer = useCallback(async () => {
    try {
      API.begin()
      const { data } = await API.get('/master/customer')
      const res = resOK(data)
      if (res.success) {
        dispatch(actionFetchCustomer(res.data))
      }
      return res
    } catch (error) {
      const e = resError(error);
      API.error(e)
    } finally {
      API.end()
    }

    return { success: false }
  }, [])

  useEffect(() => {
    if (fetchDidMount) {
      apiGetMasterCustomer()
    }
  }, [])

  return [master.customer, tasksRunning?.[name]?.loading, apiGetMasterCustomer]
}

export default useGetCustomer
