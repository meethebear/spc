import { useCallback, useEffect } from 'react'
import { useSelector, useDispatch } from "react-redux";
import selector from "@/utils/selector";
import useAPI from '@/utils/hook/useAPI'
import { resError, resOK } from "@/utils/response";
import { actionFetchUserTransportation } from '@/actions/master.action'

const useGetUserTransaction = (fetchDidMount = true) => {
const name = 'masterUserTransaction'
  const API = useAPI(name, 'none')
  const { tasksRunning, master } = useSelector(selector(["tasksRunning", "master"]));
  const useThunkDispatch = () => useDispatch()
  const dispatch = useThunkDispatch()

  const apiMasterUserTransaction = useCallback(async () => {
    try {
      API.begin()
      const { data } = await API.get('/master/user/transportation')
      const res = resOK(data)
      if (res.success) {
        dispatch(actionFetchUserTransportation(res.data))
      }
      return res
    } catch (error) {
      const e = resError(error);
      API.error(e)
    } finally {
      API.end()
    }

    return { success: false }
  }, [])

  useEffect(() => {
    if (fetchDidMount) {
      apiMasterUserTransaction()
    }
  }, [])

  return [master.user_transportation, tasksRunning?.[name]?.loading, apiMasterUserTransaction]
}

export default useGetUserTransaction
