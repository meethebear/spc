import PropTypes from 'prop-types';

const Logout = props => {
  const { fill, ...propsSvg } = props
  return (
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg" {...propsSvg}>
      <path d="M11.6667 6.66665V4.99998C11.6667 4.55795 11.4911 4.13403 11.1785 3.82147C10.8659 3.50891 10.442 3.33331 10 3.33331H4.16667C3.72464 3.33331 3.30072 3.50891 2.98816 3.82147C2.67559 4.13403 2.5 4.55795 2.5 4.99998V15C2.5 15.442 2.67559 15.8659 2.98816 16.1785C3.30072 16.491 3.72464 16.6666 4.16667 16.6666H10C10.442 16.6666 10.8659 16.491 11.1785 16.1785C11.4911 15.8659 11.6667 15.442 11.6667 15V13.3333" stroke={fill} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
      <path d="M5.83325 10H17.4999L14.9999 12.5" stroke={fill} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  )
}

Logout.defaultProps = {
  fill: '#202020'
}

Logout.propTypes = {
  fill: PropTypes.string,
}

export default Logout
