const Close = () => {
  return (
    <svg width="10" height="11" viewBox="0 0 10 11" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g opacity="0.4">
        <rect width="1.58412" height="12.446" rx="0.79206" transform="matrix(0.702572 0.711613 -0.702572 0.711613 8.74414 0.995117)" fill="black" />
        <rect width="1.58412" height="12.446" rx="0.79206" transform="matrix(-0.702572 0.711613 0.702572 0.711613 1.25488 0.995117)" fill="black" />
      </g>
    </svg>
  )
}

export default Close
