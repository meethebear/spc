const Success = () => {
  return (
    <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#clip0_2906_27056)">
        <path fillRule="evenodd" clipRule="evenodd" d="M6.5542 25.0007C6.5542 20.1094 8.49726 15.4185 11.9559 11.9598C15.4146 8.50116 20.1055 6.55811 24.9968 6.55811C29.8881 6.55811 34.5791 8.50116 38.0377 11.9598C41.4964 15.4185 43.4394 20.1094 43.4394 25.0007C43.4394 29.892 41.4964 34.583 38.0377 38.0416C34.5791 41.5003 29.8881 43.4434 24.9968 43.4434C20.1055 43.4434 15.4146 41.5003 11.9559 38.0416C8.49726 34.583 6.5542 29.892 6.5542 25.0007H6.5542ZM23.9444 32.8942L34.5624 19.6204L32.6444 18.086L23.5903 29.3999L17.1771 24.0565L15.6034 25.945L23.9444 32.8966V32.8942Z" fill="#27AE60" />
      </g>
      <circle cx="25" cy="25" r="23.9754" stroke="#27AE60" strokeWidth="2.04918" strokeMiterlimit="1.01879" strokeLinejoin="round" />
      <defs>
        <clipPath id="clip0_2906_27056">
          <rect width="36.8852" height="36.8852" fill="white" transform="translate(6.55762 6.55664)" />
        </clipPath>
      </defs>
    </svg>
  )
}

export default Success
