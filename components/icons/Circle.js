import PropTypes from 'prop-types';

const Circle = (props) => {
  const { fill, ...propsSvg } = props;

  return (
    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg" {...propsSvg}>
      <rect width="26" height="26" rx="13" fill={fill} />
    </svg>
  );
};

Circle.defaultProps = {
  fill: "#F0F0F0",
};

Circle.propTypes = {
  fill: PropTypes.string,
}

export default Circle;
