import PropTypes from "prop-types";

const DropdownOutlined = (props) => {
  const { stroke, width, height, ...propsSvg } = props;

  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...propsSvg}
    >
      <circle opacity="0.5" cx="12" cy="12" r="11.5" stroke={stroke} />
      <path d="M7 10L12 14L17 10" stroke={stroke} strokeWidth="2" />
    </svg>
  );
};

DropdownOutlined.defaultProps = {
  stroke: "black",
  width: 24,
  height: 24,
};

DropdownOutlined.propTypes = {
  stroke: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

export default DropdownOutlined;
