import { useMemo } from 'react'
import PropTypes from 'prop-types';
import styles from '@/styles/components/icons/IconsAlert.module.css'

const IconsAlert = props => {
  const { type } = props

  const _renderType = useMemo(() => {
    if (type === 'error') {
      return (
        <div className={`${styles['screenAlert-icon']} ${styles['screenAlert-error']} ${styles.animate}`}>
          <span className={`${styles['screenAlert-x-mark']}`}>
            <span className={`${styles['screenAlert-line']} ${styles['screenAlert-left']} ${styles.animateXLeft}`}></span>
            <span className={`${styles['screenAlert-line']} ${styles['screenAlert-right']} ${styles.animateXRight}`}></span>
          </span>
          <div className={styles['screenAlert-placeholder']}></div>
          <div className={styles['screenAlert-placeholder']}></div>
          <div className={styles['screenAlert-fix']}></div>
        </div>
      )
    } else if (type === 'warning') {
      return (
        <div className={`${styles['screenAlert-icon']} ${styles['screenAlert-warning']} ${styles.scaleWarning}`}>
          <span className={`${styles['screenAlert-body']} ${styles.pulseWarningIns}`}></span>
          <span className={`${styles['screenAlert-dot']} ${styles.pulseWarningIns}`}></span>
        </div>
      )
    } else {
      return (
        <div>
          <div className={`${styles['screenAlert-icon']} ${styles['screenAlert-success']} ${styles.animate}`}>
            <span className={`${styles['screenAlert-line']} ${styles['screenAlert-tip']} ${styles.animateSuccessTip}`}></span>
            <span className={`${styles['screenAlert-line']} ${styles['screenAlert-long']} ${styles.animateSuccessLong}`}></span>
            <div className={styles['screenAlert-placeholder']}></div>
            <div className={styles['screenAlert-fix']}></div>
          </div>
        </div>
      )
    }
  }, [type])
  return (
    <>
      <a
        href="https://codepen.io/Kam3leoN/pen/mdyVwBm"
        style={{ display: 'none' }}
      />
      {_renderType}
    </>
  )
}

IconsAlert.propTypes = {
  type: PropTypes.any,
}

export default IconsAlert
