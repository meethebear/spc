import { Table } from 'antd'
import PropTypes from 'prop-types'

const MyTable = props => {
  return (
    <>
      <Table
        dataSource={props.data}
        columns={props.columns}
        className='custom-bg-thead-antd'
        pagination={{
          current: props.current,
          pageSize: props.pageSize,
          showSizeChanger: props.showSizeChanger,
          total: props.total,
          position: ['bottomLeft']
        }}
      />
    </>
  )
}

MyTable.propTypes = {
  data: PropTypes.array.isRequired,
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  current: PropTypes.number,
  pageSize: PropTypes.number,
  showSizeChanger: PropTypes.bool,
  total: PropTypes.number
}

MyTable.propTypes = {
  showSizeChanger: false
}

export default MyTable
