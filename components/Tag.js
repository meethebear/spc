import styles from '@/styles/components/Tag.module.css'
import PropTypes from 'prop-types'

const Tag = props => {
  const { color, text } = props
  return (
    <div className={`${styles.teg} ${styles[color]}`}>
      {text}
    </div>
  )
}

Tag.propTypes = {
  color: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
}

export default Tag
