import { Table, Pagination } from 'antd'
import PropTypes from 'prop-types'

const MyTable = props => {
  return (
    <>
      <Table
        dataSource={props.data}
        columns={props.columns}
        pagination={false}
        bordered={props.bordered}
        scroll={props.scroll}
      />
      {props.pagination && (
        <>
          <br />
          <div>
            <Pagination
              current={props.current}
              pageSize={props.pageSize}
              showSizeChanger
            />
          </div>
        </>
      )}
    </>
  )
}

MyTable.propTypes = {
  data: PropTypes.array.isRequired,
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  current: PropTypes.number,
  pageSize: PropTypes.number,
  pagination: PropTypes.bool,
  bordered: PropTypes.bool,
  scroll: PropTypes.object
}

MyTable.defaultProps = {
  pagination: true,
  bordered: false
}

export default MyTable
