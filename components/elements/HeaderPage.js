import { Col, Row } from "antd";
import PropTypes from "prop-types";
import styles from "@/styles/components/elements/HeaderPage.module.css";

const HeaderPage = (props) => {
  const { title } = props;
  return (
    <Row align="middle" className={styles.container}>
      <Col
        span={10}
        xs={{ order: 1 }}
        sm={{ order: 1 }}
        md={{ order: 1 }}
        lg={{ order: 1 }}
      >
        <p className={styles.txt}>{title}</p>
      </Col>
    </Row>
  );
};

HeaderPage.propTypes = {
  title: PropTypes.string,
};

export default HeaderPage;
