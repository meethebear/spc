import PropTypes from 'prop-types';
import Header from "./Header";
import styles from "@/styles/components/layout/backoffice/Layout.module.css";

const Layout = (props) => {
  const { children } = props;

  return (
    <div className={styles.container}>
      <Header />
      <main className={styles.main}>{children}</main>
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.any,
}

export default Layout;
