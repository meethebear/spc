import { useCallback } from 'react'
import Image from 'next/image'
// import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'
// import Link from 'next/link'
import DropDown from '@/components/DropDown'
import { Logout } from '@/components/icons'
import styles from '@/styles/components/layout/backoffice/Header.module.css'

const NavHeader = () => {
  const user = useSelector((state) => state.user);
  console.log('user',user);
  // const { pathname } = useRouter()
  const handlerLogout = useCallback(() => {
    localStorage.clear()
    location.replace('/api/logout')
  }, [])

  // const checkActive = useCallback((path) => {
  //   console.log('pathname', pathname.search(path))
  //   if (path === '/backoffice') {
  //     return path === pathname
  //   }
  //   return pathname.search(path) === 0
  // }, [])

  return (
    <header className={styles.container}>
      <div className='flex align-center gap1'>
        <Image src="/images/logo.svg" alt="photon-express" className={styles.logo} width={120} height={100} />
      </div>
      {/* <div className={styles.nav_link}>
        <Link href="/backoffice/user">
          <a className={checkActive('/backoffice/user') ? styles.link_active : ''}>ข้อมูลผู้ใช้งาน</a>
        </Link>
      </div> */}
      <div className={styles.user}>
        <div className={styles.user_logo}>
          <Image
            src={'/images/user.svg'}
            alt={'user.png'}
            width={30}
            height={30}
          />
        </div>
        <div className={styles.username}>
          <label>{user.username}</label>
        </div>
        <DropDown
          title=''
          items={[{ label: <div className='flex align-center'><Logout /> &nbsp;Logout </div>, key: 'logout', onClick: handlerLogout }]}
        />
      </div>
    </header>
  )
}

export default NavHeader
