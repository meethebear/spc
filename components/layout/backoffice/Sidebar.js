import { useCallback } from 'react'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import Link from 'next/link'
import styles from '@/styles/components/layout/Sidebar.module.css'
import { Dashboard, ChartSquare, Chatbot, Question, Setting, User } from '@/components/icons'

const mappingTransaction = {
  Home: Dashboard,
  Chatbot: Chatbot,
  ChartSquare: ChartSquare,
  Question: Question,
  User: User,
  Setting: Setting,
}

const Sidebar = () => {
  const { pathname } = useRouter()
  const user = useSelector(state => state.user)
  // const onMouseEnterSidebar = useCallback(() => {
  //   document.getElementById('sidebar').classList.add(styles.open)
  // }, [])

  // const onMouseLeaveSidebar = useCallback(() => {
  //   document.getElementById('sidebar').classList.remove(styles.open)
  // }, [])

  const Icon = useCallback((iconName, { ...props }) => {
    const IconResult = mappingTransaction[iconName]
    return <IconResult {...props} />
  }, [])

  return (
    <>
      <aside
        className={styles.container}
        id='sidebar'
        // onMouseEnter={onMouseEnterSidebar}
        // onMouseLeave={onMouseLeaveSidebar}
      >
        <div className={styles.content}>
          <ul className={styles.menu_list}>
            {!!user?.menu && user?.menu.map((item, index) => (
              <li className={`${styles.menu_item} ${pathname === item.path ? styles.menu_item_active : ''}`} key={index}>
                <Link href={item.path}>
                  <a className={styles.link}>
                    {Icon(item.icon, {
                      fill: pathname === item.path ? '#FFFFFF' : '#FFFFFF'
                    })}
                    <span className={styles.link_text}>{item.label}</span>
                  </a>
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </aside>
    </>
  )
}

export default Sidebar
