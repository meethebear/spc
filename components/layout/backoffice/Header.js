import { useCallback } from 'react'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'
import Link from 'next/link'
import DropDown from '@/components/DropDown'
import { Logout } from '@/components/icons'
import styles from '@/styles/components/layout/backoffice/Header.module.css'

const NavHeader = () => {
  const user = useSelector((state) => state.user);
  const { pathname } = useRouter()
  const handlerLogout = useCallback(() => {
    localStorage.clear()
    location.replace('/api/logout')
  }, [])

  const checkActive = useCallback((path) => {
    console.log('pathname', pathname.search(path))
    if (path === '/backoffice') {
      return path === pathname
    }
    return pathname.search(path) === 0
  }, [])

  return (
    <header className={styles.container}>
      <div className='flex align-center gap1'>
        <Image src="/images/logo.svg" alt="photon-express" className={styles.logo} width={120} height={100} />
      </div>
      <div className={styles.nav_link}>
        <Link href="/backoffice">
          <a className={checkActive('/backoffice/dashboard') ? styles.link_active : ''}>แดชบอร์ด</a>
        </Link>
        <Link href="/backoffice/order">
          <a className={checkActive('/backoffice/order') ? styles.link_active : ''}>ข้อมูลงาน</a>
        </Link>
        <Link href="/backoffice/customer">
          <a className={checkActive('/backoffice/customer') ? styles.link_active : ''}>ข้อมูลลูกค้า</a>
        </Link>
      </div>
      <div className={styles.user}>
        <div className={styles.user_logo}>
          <Image
            src={'/images/user.svg'}
            alt={'user.png'}
            width={30}
            height={30}
          />
        </div>
        <div className={styles.username}>
          <label>{user.username}</label>
        </div>
        <DropDown
          title=''
          items={[{ label: <div className='flex align-center'><Logout /> &nbsp;Logout </div>, key: 'logout', onClick: handlerLogout }]}
        />
      </div>
    </header>
  )
}

export default NavHeader
