import Image from "next/image";
import styles from "@/styles/components/layout/Header.module.scss";
import { Typography } from 'antd'

const { Title, Text } = Typography;

const NavHeader = () => {
  return (
    <header className={styles.container}>
      <div className={styles.header_section}>
        <Image
          src="/images/logo.svg"
          alt="photon-express"
          className={styles.logo}
          width={140}
          height={50}
        />
        <Title className={[styles.title, styles.h1]}>Track & Trace</Title>
        <Text className={[styles.title, styles.sub_head]}>ติดตามข้อมูลรายการจัดส่งสินค้าจากรหัสงานของคุณ</Text>
      </div>
    </header>
  );
};

export default NavHeader;
