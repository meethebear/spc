import { Dropdown, Menu, Space } from 'antd'
import { DownOutlined, MoreOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types'

const MyDropDown = props => {
  const menu = (
    <Menu
      items={props.items}
    />
  );

  return (
    <>
      <Dropdown overlay={menu} trigger={['click']}>
        <a onClick={e => e.preventDefault()}>
          <Space>
            {props.title || null}
            {props.icon === 'down' && (
              <DownOutlined />
            )}
            {props.icon === 'more' && (
              <div className='flex align-center justify-center' style={{ width: 38, height: 38, backgroundColor: '#F0F0F0', borderRadius: 6, fontSize: '1.2rem' }}>
                <MoreOutlined />
              </div>
            )}
          </Space>
        </a>
      </Dropdown>
    </>
  )
}

MyDropDown.propTypes = {
  title: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  icon: PropTypes.oneOf(['down', 'more'])
}

MyDropDown.defaultProps = {
  icon: 'down'
}

export default MyDropDown
