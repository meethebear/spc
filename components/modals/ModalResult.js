import PropTypes from 'prop-types';
import { Button, Modal } from 'antd'
import IconsAlert from '@/components/icons/IconsAlert'
import styles from '@/styles/components/modals/ModalCommon.module.css'

const ModalResult = props => {
  const { type, title, subTitle, open, onClose, txtBtnClose } = props

  return (
    <Modal
      visible={open}
      onCancel={onClose}
      keyboard={false}
      destroyOnClose
      footer={false}
      title={title}
    >
      <div className={styles.wrapper_modal}>
        <div className={styles.content_modal}>
          <IconsAlert type={type} />
          <div className={styles.content__text_box}>
            <h3>{subTitle}</h3>
          </div>
          <div className={styles.content__btn_full_bar}>
            <Button onClick={onClose} type='primary' size='large'>
              {txtBtnClose}
            </Button>
          </div>
        </div>
      </div>
    </Modal>
  )
}

ModalResult.defaultProps = {
  txtBtnClose: 'Close'
}

ModalResult.propTypes = {
  type: PropTypes.any,
  title: PropTypes.string,
  subTitle: PropTypes.string,
  open: PropTypes.any,
  onClose: PropTypes.any,
  txtBtnClose: PropTypes.string,
}

export default ModalResult
