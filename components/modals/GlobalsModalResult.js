import { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { closeModal } from '@/actions/modal.action'
import ModalResult from './ModalResult'

const GlobalsModalResult = () => {
  const modal = useSelector(state => state.modal)
  const dispatch = useDispatch()

  const _onClose = useCallback(() => {
    dispatch(closeModal('result'))
    if (typeof modal.onClose === 'function') {
      modal.onClose()
    }
  }, [modal, dispatch])

  return (
    <ModalResult
      type={modal.type}
      title={modal.title}
      subTitle={modal.description}
      open={modal.open}
      onClose={_onClose}
    />
  )
}

export default GlobalsModalResult
