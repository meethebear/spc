import { memo } from 'react'
import { Modal as ModalAntd } from 'antd'
import PropTypes from 'prop-types'

const Modal = (props) => {
  const { children, ...propsModal } = props

  return (
    <div>
      <ModalAntd
        keyboard={false}
        centered
        destroyOnClose
        zIndex={2001}
        {...propsModal}
      >
        {children}
      </ModalAntd>
    </div>
  )
}

Modal.propTypes = {
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onOk: PropTypes.func,
  footer: PropTypes.node,
  children: PropTypes.any,
  title: PropTypes.node
}

export default memo(Modal)
