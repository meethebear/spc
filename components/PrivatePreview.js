import { memo } from 'react'
import usePreviewImg from '@/utils/hook/usePreviewImg'
import PropTypes from 'prop-types'
import Spin from 'antd/lib/spin'
import { LoadingOutlined } from '@ant-design/icons'

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />

const PrivatePreview = props => {
  const { src, ...imgProps } = props
  const [previewURL, loading] = usePreviewImg(src, imgProps.id || src)

  if (loading) {
    return (
      <Spin spinning={loading} indicator={antIcon}>
        <div style={{ width: imgProps.width, height: imgProps.height }}></div>
      </Spin>
    )
  }

  return (
    <img
      src={previewURL}
      loading={'lazy'}
      alt={imgProps?.alt || 'preview'}
      {...imgProps}
    />
  )
}

PrivatePreview.propTypes = {
  src: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number
}

export default memo(PrivatePreview)
