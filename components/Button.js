import { Button } from 'antd'
import PropTypes from 'prop-types'

const MyButton = props => {
  return (
    <>
      <Button onClick={props.onClick} icon={props.icon} type={props.type} htmlType={props.htmlType} className={props.className}>
        {props.children}
      </Button>
    </>
  )
}

MyButton.propTypes = {
  onClick: PropTypes.func,
  icon: PropTypes.node,
  type: PropTypes.oneOf(['dashed', 'ghost', 'link', 'primary', 'text', 'default']),
  htmlType: PropTypes.oneOf(['submit', 'button']),
  children: PropTypes.any,
  className: PropTypes.string
}

MyButton.defaultProps = {
  type: 'primary',
  htmlType: 'button',
  className: ''
}

export default MyButton
