import { useCallback, useContext, useMemo } from 'react';
import { Checkbox as CheckboxAntd } from 'antd'
import { FormContext } from './Form'
import { FormContext as FormContextList } from './FormList'
import { getValues, getRequired, getDisabled, getError, getOnChange, } from './utils'
import PropTypes from 'prop-types'
import styles from '@/styles/components/form/Field.module.css'

const Checkbox = props => {
  const { name, label, value, required, error, disabled, onChange, options, keys, styleField, listCtls, ...propsCheckbox } = props
  const formContextINI = useContext(FormContext)
  const formContext = JSON.stringify(formContextINI) === "{}" ?  useContext(FormContextList) : formContextINI

  const _value = useMemo(() => {
    return getValues(formContext, name, value)
  }, [formContext, name, value])

  const _required = useMemo(() => {
    return getRequired(formContext, name, required)
  }, [formContext, name, required])

  const _error = useMemo(() => {
    return getError(formContext, name, error)
  }, [formContext, name, error])

  const _disabled = useMemo(() => {
    return getDisabled(formContext, name, disabled)
  }, [formContext, name, disabled])

  const _onChange = useCallback((values) => {
    const funChange = getOnChange(formContext, onChange)
    const value = values
    funChange(name, value)
  }, [formContext, name, onChange])

  const _options = useMemo(() => {
    if (options?.length && keys?.length) {
      return options.map(item => ({
        label: item[keys[1]],
        value: item[keys[0]],
        ...item
      }))
    } else {
      return []
    }
  }, [options, keys])

  return (
    <div
      className={`${styles.container} ${_error ? 'a-error' : ''}`}
      style={styleField}
    >
      {!!label && (
        <label className={`${styles.label}`} htmlFor={name} title={label}>
          {label}
          {!!_required && <span className={`${styles.star}`}>&nbsp;*</span>}
        </label>
      )}
      <div className='ant-col ant-form-item-control'>
        <CheckboxAntd.Group value={_value} onChange={_onChange} options={_options} {...propsCheckbox} disabled={_disabled}>
        </CheckboxAntd.Group>
      </div>
      <span className={`${styles.error}`}>
        {_error}
      </span>
    </div>
  )
};

Checkbox.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),
  value: PropTypes.array,
  required: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.func
  ]),
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  options: PropTypes.array.isRequired,
  keys: PropTypes.array,
  styleField: PropTypes.object
}

Checkbox.defaultProps = {
  keys: ['value', 'label'],
  styleField: {}
}

export default Checkbox;
