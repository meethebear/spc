export const getValues = (formContext, name, value) => {
  if (typeof value !== 'undefined') return value
  return formContext?.values?.[name] || ''
}

export const getRequired = (formContext, name, required) => {
  if (typeof required !== 'undefined') return required
  if (formContext?.showStarRequired === false) return false
  return !!formContext?.rules?.[name]?.required
}

export const getError = (formContext, name, error) => {
  if (typeof error === 'function') {
    return error(Object.freeze((formContext?.errors?.[name] || {})))
  }
  if (typeof error !== 'undefined') return error
  return formContext?.errors?.[name] || null
}

export const getDisabled = (formContext, name, disabled) => {
  if (typeof disabled !== 'undefined') return disabled
  return formContext?.isFieldDisable?.(name) || false
}

export const getOnChange = (formContext, onChange) => {
  if (typeof onChange === 'function') return onChange
  return formContext?.handlerChange || (() => {})
}

export const getMessage = (fieldName, fieldNameTH, locale) => {
  if (locale === 'th') {
    return 'กรุณาระบุ ' + fieldNameTH
  }

  return 'Please input ' + fieldName
}

export const getMessagePure = (fieldName, fieldNameTH, locale) => {
  if (locale === 'th') {
    return fieldNameTH
  }

  return fieldName
}

export const getMessageReq = (fieldName, fieldNameTH, locale) => {
  if (locale === 'th') {
    return 'กรุณาระบุ ' + fieldNameTH
  }

  return 'Please input ' + fieldName
}
