import { useCallback, useContext, useMemo } from 'react';
import { Select, Empty } from 'antd'
import { FormContext } from './Form'
import { FormContext as FormContextList } from './FormList'
import { getValues, getRequired, getDisabled, getError, getOnChange, } from './utils'
import PropTypes from 'prop-types'
import styles from '@/styles/components/form/Field.module.css'

const { Option } = Select;

const SelectInput = props => {
  const { name, label, value, required, error, disabled, onChange, id, options, keys, style, ...propsInput } = props
  const formContextINI = useContext(FormContext)
  const formContext = JSON.stringify(formContextINI) === "{}" ?  useContext(FormContextList) : formContextINI

  const selectId = `select-body-${id || name}`

  const _value = useMemo(() => {
    return getValues(formContext, name, value)
  }, [formContext, name, value])

  const _required = useMemo(() => {
    return getRequired(formContext, name, required)
  }, [formContext, name, required])

  const _error = useMemo(() => {
    return getError(formContext, name, error)
  }, [formContext, name, error])

  const _disabled = useMemo(() => {
    return getDisabled(formContext, name, disabled)
  }, [formContext, name, disabled])

  const _onChange = useCallback((value) => {
    const funChange = getOnChange(formContext, onChange)
    // const value = value || ''
    funChange(name, value)
  }, [formContext, name, onChange])

  return (
    <div
      className={`${styles.container} ${_error ? 'a-error' : ''}`}
    >
      {!!label && (
        <label className={`${styles.label}`} htmlFor={name} title={label}>
          {label}
          {!!_required && <span className={`${styles.star}`}>&nbsp;*</span>}
        </label>
      )}
      <div id={selectId}>
        <Select
          getPopupContainer={() => document.getElementById(selectId)}
          optionFilterProp='label'
          filterOption={(input, option) => option.label.toLowerCase().indexOf(input.toLowerCase()) === 0}
          notFoundContent={<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />}
          name={name}
          value={_value || null}
          disabled={_disabled}
          onChange={_onChange}
          style={{ width: '100%', ...style }}
          {...propsInput}
        >
          {!!options?.length &&
            options.map((item, index) => (
              <Option key={index} {...(item.props || {})} value={item[keys[0]]} label={item[keys[1]] || '-'}>
                {item[keys[1]] || '-'}
              </Option>
            ))}
        </Select>
      </div>
      <span className={`${styles.error}`}>
        {_error}
      </span>
    </div>
  )
};

SelectInput.propTypes = {
  id: PropTypes.any,
  name: PropTypes.string.isRequired,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),
  value: PropTypes.any,
  required: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.func
  ]),
  disabled: PropTypes.bool,
  type: PropTypes.string,
  onChange: PropTypes.func,
  mode: PropTypes.oneOf(['multiple', 'tags']),
  options: PropTypes.array.isRequired,
  keys: PropTypes.array,
  showSearch: PropTypes.bool,
  loading: PropTypes.bool,
  style: PropTypes.any,
  allowClear: PropTypes.bool
}

SelectInput.defaultProps = {
  keys: ['value', 'label'],
  showSearch: true
}

export default SelectInput;
