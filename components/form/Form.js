import { createContext, useCallback } from 'react'
import middleware from '@/utils/middleware'
import PropTypes from 'prop-types'

export const FormContext = createContext({})

const Form = (props) => {
  const { children, handlerSubmit, handlerSubmitError, form, loading, preventEnter, showStarRequired, ...formProps } = props
  const { setSubmitting, validate, values, blackList, whiteList, rules, setSubmitted } = form

  const onStartSubmit = useCallback((next) => {
    setSubmitted(true)
    setSubmitting(true)
    if (typeof next === 'function') {
      next()
    }
  }, [setSubmitting, setSubmitted])

  const onEndSubmit = useCallback(() => {
    setSubmitting(false)
  }, [setSubmitting])

  const buildValidate = useCallback((next, end) => {
    const resultValid = validate(next, end, true)
    let errors = {}
    if (typeof resultValid !== 'boolean') {
      errors = resultValid[1]
    }
    if (typeof handlerSubmitError === 'function') {
      handlerSubmitError(errors, values, rules)
    }
  }, [validate, handlerSubmitError, values, rules])

  const _handlerSubmit = useCallback((event) => {
    event.preventDefault()
    if (Array.isArray(handlerSubmit)) {
      middleware([onStartSubmit, buildValidate, ...handlerSubmit, onEndSubmit], { end: onEndSubmit })
    } else if (typeof handlerSubmit === 'function') {
      middleware([onStartSubmit, buildValidate, handlerSubmit, onEndSubmit], { end: onEndSubmit })
    }
  }, [handlerSubmit, buildValidate, onStartSubmit, onEndSubmit])

  const isFieldDisable = useCallback((name) => {
    if (blackList === '*') {
      return true
    } else if (whiteList === '*') {
      return false
    } else if (blackList && blackList?.length) {
      return blackList.includes(name)
    } else if (whiteList && whiteList?.length) {
      return !whiteList.includes(name)
    } else {
      return false
    }
  }, [whiteList, blackList])

  return (
    <FormContext.Provider value={{ ...form, showStarRequired, loading, isFieldDisable }}>
      <form noValidate onSubmit={_handlerSubmit} {...formProps} onKeyDown={(e) => { if (preventEnter && e.code === 'Enter') e.preventDefault() }}>
        {children}
      </form>
    </FormContext.Provider>
  )
}

Form.propTypes = {
  form: PropTypes.object.isRequired,
  handlerSubmit: PropTypes.func.isRequired,
  showStarRequired: PropTypes.bool,
  children: PropTypes.any,
  handlerSubmitError: PropTypes.any,
  loading: PropTypes.any,
  preventEnter: PropTypes.any,
}

Form.defaultProps = {
  showStarRequired: true
}

export default Form
