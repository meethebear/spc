import Input from './Input'
import Number from './InputNumber'
import DateRangePicker from './DateRangePicker'
import Select from './Select'
import Checkbox from './Checkbox'
import Radio from './Radio'
import DatePicker from './DatePicker'
import Upload from './Upload'
import Password from './Password'
import TextArea from './TextArea'
import TimePicker from './TimePicker'

const Field = {
  Input,
  Number,
  DateRangePicker,
  Select,
  Checkbox,
  Radio,
  DatePicker,
  Upload,
  Password,
  TextArea,
  TimePicker
}

export default Field
