import { createContext, useCallback, useContext } from 'react'
import middleware from '@/utils/middleware'
import PropTypes from 'prop-types'

export const FormContext = createContext({})
export const useFormContext = () => {
  const formContext = useContext(FormContext)
  return formContext
}

const Form = (props) => {
  const { children, handlerSubmit, form, loading, preventEnter, showStarRequired, listCtls, onSubmitError, ...formProps } = props
  const { setSubmitting, validate, values, blackList, whiteList, rules, setSubmitted } = form

  const onStartSubmit = useCallback((next) => {
    setSubmitted(true)
    setSubmitting(true)
    if (typeof next === 'function') {
      next()
    }
  }, [setSubmitting, setSubmitted])

  const onEndSubmit = useCallback(() => {
    setSubmitting(false)
  }, [setSubmitting])

  const buildValidate = useCallback((next, end) => {
    try {
      const a = validate(null, null, true)
      const [isValid, errors] = a
      const listIsValidA = []
      for (const keyList in listCtls) {
        if (Object.hasOwnProperty.call(listCtls, keyList)) {
          const listCtl = listCtls[keyList]
          const listIsValid = listCtl.validateListItem()
          listIsValidA.push(listIsValid)
        }
      }
      if (isValid && listIsValidA.every(item => item === true)) {
        next(values)
        return
      }
      end()
      if (typeof onSubmitError === 'function') {
        onSubmitError(errors, values, rules)
      }
      return [isValid, errors]
    } catch (error) {
      console.log('error', error)
    }

  }, [validate, onSubmitError, values, rules, listCtls])

  const buildValues = useCallback((values, next) => {
    const newValues = Object.assign({}, values)
    for (const keyList in listCtls) {
      if (Object.hasOwnProperty.call(listCtls, keyList)) {
        const listCtl = listCtls[keyList]
        const valueList = listCtl.values.map(item => item.values)
        newValues[keyList] = valueList
      }
    }

    next(newValues)
  }, [listCtls])

  const _handlerSubmit = useCallback((event) => {
    event.preventDefault()
    if (Array.isArray(handlerSubmit)) {
      middleware([onStartSubmit, buildValidate, buildValues, ...handlerSubmit, onEndSubmit], { end: onEndSubmit })
    } else if (typeof handlerSubmit === 'function') {
      middleware([onStartSubmit, buildValidate, buildValues, handlerSubmit, onEndSubmit], { end: onEndSubmit })
    }
  }, [handlerSubmit, buildValidate, onStartSubmit, onEndSubmit, buildValues])

  const isFieldDisable = useCallback((name) => {
    if (blackList === '*') {
      return true
    } else if (whiteList === '*') {
      return false
    } else if (blackList && blackList?.length) {
      return blackList.includes(name)
    } else if (whiteList && whiteList?.length) {
      return !whiteList.includes(name)
    } else {
      return false
    }
  }, [whiteList, blackList])

  return (
    <FormContext.Provider value={{ ...form, showStarRequired, loading, isFieldDisable }}>
      <form noValidate onSubmit={_handlerSubmit} {...formProps} onKeyDown={(e) => { if (preventEnter && e.code === 'Enter') e.preventDefault() }}>
        {children}
      </form>
    </FormContext.Provider>
  )
}

Form.propTypes = {
  form: PropTypes.object.isRequired,
  handlerSubmit: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.func
  ]).isRequired,
  showStarRequired: PropTypes.bool,
  children: PropTypes.any,
  loading: PropTypes.any,
  preventEnter: PropTypes.any,
  listCtl: PropTypes.any,
  listCtls: PropTypes.any,
  onSubmitError: PropTypes.any,
}

Form.defaultProps = {
  showStarRequired: true
}

export default Form
