import { useCallback } from 'react'
import { getMessage } from '../utils'

const useCheckValidate = (locale = 'en-US') => {
  return useCallback((values, rules) => {
    const errors = {}

    for (const field in rules) {
      if (Object.hasOwnProperty.call(rules, field)) {
        const fieldRules = rules[field]
        const errMsg = checkErr(fieldRules, values[field], field, values, locale)
        if (errMsg) errors[field] = errMsg
      }
    }
    return errors
  }, [locale])
}

export const checkErr = (fieldRules, value, fieldName, values, locale = 'en-US') => {
  let errMsg
  for (const rule in fieldRules) {
    const valueRule = fieldRules[rule]
    if (typeof valueRule === 'undefined') continue
    if (Object.hasOwnProperty.call(fieldRules, rule)) {
      switch (rule) {
      case 'required':
        // if (!CheckEmpty(value)) errMsg = typeof valueRule === 'string' ? valueRule : valueRule[locale] ? valueRule[locale] : `Please input ${fieldName}`
        if (!CheckEmpty(value)) errMsg = typeof valueRule === 'string' ? valueRule : valueRule[locale] ? valueRule[locale] : getMessage(fieldName, fieldName, locale = 'th')
        break
      case 'isAllowed':
        if (Array.isArray(valueRule)) {
          for (const item of valueRule) {
            if (typeof item?.func === 'function' && !item.func(value, values)) {
              errMsg = item[locale] ? item[locale] : item.msg || `Please input ${fieldName} to be correct`
              break
            }
          }
        } else {
          if (typeof valueRule?.func === 'function' && !valueRule.func(value, values)) errMsg = valueRule.msg[locale] ? valueRule.msg[locale] : valueRule.msg || `Please input ${fieldName} to be correct`
        }
        break
      case 'isAllowedAll': {
        const errMsgs = []
        for (const item of valueRule) {
          if (typeof item?.func === 'function' && !item.func(value, values)) {
            const errMsg = item[locale] ? item[locale] : item.msg || `Please input ${fieldName} to be correct`
            errMsgs.push(errMsg)
          }
        }
        if (errMsgs.length) {
          errMsg = errMsgs.join('\n')
        } else {
          errMsg = undefined
        }

        break
      }
      default:
        break
      }

      if (errMsg) break
    }
  }
  return errMsg
}

const CheckEmpty = (value) => {
  if ((value || value === 0) && typeof value === 'number') {
    return true
  } else if (value && typeof value === 'string' && value.trim() !== '') {
    return true
  } else if (Array.isArray(value)) {
    if (value?.length) {
      return true
    } else {
      return false
    }
  } else if (value && typeof value !== 'string') {
    return true
  } else if (value) {
    return true
  } else {
    return false
  }
}

export default useCheckValidate
