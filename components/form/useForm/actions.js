export const actionTypes = {
  MAIN: {
    CHANGE: 'CHANGE',
    SET: 'SET',
    RESET: 'RESET'
  }
}
