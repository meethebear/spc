import useForm from './index'
import useList from './useList'
import useCheckValidate from './useCheckValidate'
import { useCallback } from 'react'

const useFormList = (config, configList) => {
  const form = useForm(config)
  const listCtl = useList(configList)
  const checkValidate = useCheckValidate()

  const onChange = useCallback((ix, name, value, rules) => {
    const prevItem = listCtl.values[ix]
    const values = {
      ...prevItem.values,
      [name]: value
    }
    const _rules = rules || prevItem.rules
    const newItem = {
      ...prevItem,
      values,
      rules: _rules,
      errors: form.submitted ? checkValidate(values, _rules) : {}
    }
    listCtl.changeListItem(ix, newItem)
  }, [listCtl, form.submitted, checkValidate])

  return {
    ...form,
    listCtl: {
      ...listCtl,
      onChange
    }
  }
}

export default useFormList
