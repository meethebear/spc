import { useCallback, useEffect, useReducer } from 'react'
import useCheckValidate from './useCheckValidate'

function reducerValues(state, action) {
  switch (action.type) {
  case 'ADD': {
    const newState = [...state, action.payload]
    return newState
  }
  case 'ADD_MANY': {
    const newState = [...state, ...action.payload]
    return newState
  }
  case 'CHANGE': {
    const newState = [...state]
    newState.splice(action.payload.ix, 1, action.payload.value)
    return newState
  }
  case 'REMOVE': {
    const newState = [...state]
    newState.splice(action.payload.ix, 1)
    return newState
  }
  case 'SET':
    return action.payload
  case 'SET_RULES_ITEM': {
    const newState = [...state]
    const prevState = newState[action.payload.ix]
    const keys = Object.keys(action.payload.rules)
    const newErrors = {}
    keys.forEach(key => {
      newErrors[key] = undefined
    });

    newState.splice(action.payload.ix, 1, { ...prevState, rules: { ...prevState.rules, ...action.payload.rules }, errors: { ...prevState.errors, ...newErrors } })
    return newState
  }
  case 'RESET':
    return []
  default:
    return state
  }
}

const useList = props => {
  const { initialValues } = props

  const [values, dispatchValues] = useReducer(reducerValues, initialValues)
  const checkValidate = useCheckValidate()

  useEffect(() => {
    if (initialValues) {
      // dispatchValues({
      //   type: 'ADD_MANY',
      //   payload: initialValues
      // })
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  console.log('values', values)

  const addListItem = useCallback((init) => {
    dispatchValues({
      type: 'ADD',
      payload: init
    })
  }, [])

  const changeListItem = useCallback((ix, value) => {
    dispatchValues({
      type: 'CHANGE',
      payload: {
        ix, value
      }
    })
  }, [])

  const removeListItem = useCallback((ix) => {
    dispatchValues({
      type: 'REMOVE',
      payload: {
        ix
      }
    })
  }, [])

  const validateListItem = useCallback(() => {
    let isValid = true
    const valuesFlat = values.map(item => {
      const errors = checkValidate(item.values, item.rules)
      if (Object.keys(errors).length) {
        isValid = false
      }
      return {
        ...item,
        errors
      }
    })
    dispatchValues({
      type: 'SET',
      payload: valuesFlat
    })

    return isValid
  }, [values, checkValidate])

  const addManyListItem = useCallback((num, init) => {
    const arrayNumber = []
    for (let index = 1; index <= Number(num); index++) {
      arrayNumber.push(init)
    }
    dispatchValues({
      type: 'ADD_MANY',
      payload: arrayNumber
    })
  }, [])

  const setList = useCallback((arrayList) => {
    dispatchValues({
      type: 'SET',
      payload: arrayList
    })
  }, [])

  const setRulesItem = useCallback((ix, rules) => {
    dispatchValues({
      type: 'SET_RULES_ITEM',
      payload: {
        ix,
        rules 
      }
    })
  }, [])

  return {
    values,
    addListItem,
    changeListItem,
    removeListItem,
    validateListItem,
    addManyListItem,
    setList,
    setRulesItem
  }
}

export default useList
