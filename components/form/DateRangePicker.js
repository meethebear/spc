import { useCallback, useContext, useMemo } from 'react';
import { DatePicker } from 'antd'
import { FormContext } from './Form'
import { FormContext as FormContextList } from './FormList'
import { getValues, getRequired, getDisabled, getError, getOnChange, } from './utils'
import PropTypes from 'prop-types'
import styles from '@/styles/components/form/Field.module.css'

const { RangePicker } = DatePicker;

const DateRangePicker = props => {
  const { name, label, value, required, error, disabled, onChange, ...propsInput } = props
  const formContextINI = useContext(FormContext)
  const formContext = JSON.stringify(formContextINI) === "{}" ?  useContext(FormContextList) : formContextINI

  const _value = useMemo(() => {
    return getValues(formContext, name, value)
  }, [formContext, name, value])

  const _required = useMemo(() => {
    return getRequired(formContext, name, required)
  }, [formContext, name, required])

  const _error = useMemo(() => {
    return getError(formContext, name, error)
  }, [formContext, name, error])

  const _disabled = useMemo(() => {
    return getDisabled(formContext, name, disabled)
  }, [formContext, name, disabled])

  const _onChange = useCallback((date) => {
    const funChange = getOnChange(formContext, onChange)
    const value = date
    funChange(name, value)
  }, [formContext, name, onChange])

  return (
    <div
      className={`${styles.container} ${_error ? 'a-error' : ''}`}
    >
      {!!label && (
        <label className={`${styles.label}`} htmlFor={name} title={label}>
          {label}
          {!!_required && <span className={`${styles.star}`}>&nbsp;*</span>}
        </label>
      )}
      <RangePicker
        name={name}
        value={_value}
        disabled={_disabled}
        onChange={_onChange}
        {...propsInput}
        style={{ width: '100%', ...(propsInput.styles || {}) }}
      />
      <span className={`${styles.error}`}>
        {_error}
      </span>
    </div>
  )
};

DateRangePicker.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),
  value: PropTypes.any,
  required: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.func
  ]),
  disabled: PropTypes.bool,
  type: PropTypes.string,
  onChange: PropTypes.func
}

export default DateRangePicker;
