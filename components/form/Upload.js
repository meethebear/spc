import { useCallback, useContext, useMemo } from 'react'
import { message, Empty, Typography } from 'antd'
import { UploadOutlined } from '@ant-design/icons'
import { useDropzone } from 'react-dropzone'
import { FormContext } from './Form'
import { FormContext as FormContextList } from './FormList'
import PrivatePreview from '@/components/PrivatePreview'
import { getValues, getDisabled, getError, getOnChange, } from './utils'
import PropTypes from 'prop-types'
import stylesUpload from '@/styles/components/form/Upload.module.css'
import styles from '@/styles/components/form/Field.module.css'
import config from '../../config'

const { Text } = Typography

const Upload = props => {
  const { init, title, name, value, error, disabled, onChange, accept, maxSize, } = props
  const formContextINI = useContext(FormContext)
  const formContext = JSON.stringify(formContextINI) === "{}" ?  useContext(FormContextList) : formContextINI

  const _value = useMemo(() => {
    return getValues(formContext, name, value)
  }, [formContext, name, value])

  const _error = useMemo(() => {
    return getError(formContext, name, error)
  }, [formContext, name, error])

  const _disabled = useMemo(() => {
    return getDisabled(formContext, name, disabled)
  }, [formContext, name, disabled])

  const _onChange = useCallback((value) => {
    const funChange = getOnChange(formContext, onChange)
    // const value = value || ''
    funChange(name, value)
  }, [formContext, name, onChange])


  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: accept,
    maxSize: maxSize,
    onDropAccepted: acceptedFiles => {
      const file = acceptedFiles[0]
      if (!file) return
      _onChange({
        default: init,
        file,
        is_deleted: false,
        preview: ['image/png', 'image/jpeg'].includes(file?.type) ? URL.createObjectURL(file) : `${config.hostFront}/images/bxs_file-pdf.png`
      })
    },
    onDropRejected: (acceptedFiles) => {
      const { errors } = acceptedFiles[0]
      if (errors?.[0]?.code === 'file-too-large') {
        message.error('Only Upload 5MB maximum file size')
      } else if (errors?.[0]?.code === 'file-invalid-type') {
        message.error('Only Upload file type png and jpeg')
      } else {
        message.error('Something went wrong on upload')
      }
    },
    disabled: _disabled
  })

  return (
    <div>
      <div
        className={`${styles.container} ${_error ? 'a-error' : ''}`}
      >
        <div>
          <label className={stylesUpload.box_upload_title}>{title}</label>
          <section className={stylesUpload.container}>
            {_disabled && !_value.file && !_value.preview && !_value.default && !_value.is_deleted
              ? (
                <div style={{ height: 204 }} className='flex align-center justify-center w-100'>
                  <Empty description={'Not have file'} />
                </div>
              )
              : (
                <div {...getRootProps({ className: 'dropzone' })}>
                  <input {...getInputProps()} />
                  {(_value.preview)
                    ? (
                      <div className={stylesUpload.content_preview} style={{ height: 204 }}>
                        <PrivatePreview
                          src={_value.preview}
                          style={{ maxHeight: 190 }}
                        />
                      </div>
                    )
                    : (
                      <div className={stylesUpload.upload_box} style={{ height: 204 }}>
                        <div className={stylesUpload.col_btn}>
                          <UploadOutlined style={{ fontSize: '45px', color: '#ccc' }} />
                        </div>
                        <div className={stylesUpload.col_text}>
                          {isDragActive && <Text strong>Drop some file here...</Text>}
                          {!isDragActive && <Text strong>Drag & Drop file here or browser your device</Text>}
                          <br />
                          <Text type="secondary" style={{ fontSize: '13px' }}>Allowed Documents : jpg, png, pdf - 5MB maximum file size.</Text>
                        </div>
                      </div>
                    )
                  }
                </div>
              )}
          </section>
        </div>
        <span className={`${styles.error}`}>
          {_error}
        </span>
      </div>
    </div>
  )
}

Upload.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),
  value: PropTypes.any,
  required: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.func
  ]),
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  accept: PropTypes.string,
  maxSize: PropTypes.number,
  init: PropTypes.any,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),
}

Upload.defaultProps = {
  accept: 'image/png,image/jpeg,application/pdf',
  maxSize: 5000000
}

export default Upload
