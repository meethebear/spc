import { useCallback, useContext, useMemo } from 'react';
import { FormContext } from './Form'
import { FormContext as FormContextList } from './FormList'
import { getValues, getRequired, getDisabled, getError, getOnChange } from './utils'
import PropTypes from 'prop-types'
import InputNumberFormat from 'react-number-format'
import stylesNumber from 'styles/components/form/Input.module.css'
import styles from '@/styles/components/form/Field.module.css'

const InputNumber = props => {
  const { name, label, value, required, error, disabled, onChange, id, onlyNumber, allowInteger, isAllowed, unit, suffix, className, thousandSeparator, ...propsInput } = props
  const formContextINI = useContext(FormContext)
  const formContext = JSON.stringify(formContextINI) === "{}" ?  useContext(FormContextList) : formContextINI

  const _value = useMemo(() => {
    return getValues(formContext, name, value)
  }, [formContext, name, value])

  const _required = useMemo(() => {
    return getRequired(formContext, name, required)
  }, [formContext, name, required])

  const _error = useMemo(() => {
    return getError(formContext, name, error)
  }, [formContext, name, error])

  const _disabled = useMemo(() => {
    return getDisabled(formContext, name, disabled)
  }, [formContext, name, disabled])

  const _onChange = useCallback((event) => {
    const funChange = getOnChange(formContext, onChange)
    const value = event.value
    funChange(name, value)
  }, [formContext, name, onChange])

  const isAllowedFunc = useCallback((val) => {
    const { floatValue, value } = val
    if (typeof isAllowed === 'function') {
      return isAllowed(val)
    } else if (typeof allowInteger !== 'undefined' && allowInteger !== null) {
      return (floatValue >= 0 && floatValue <= allowInteger) || value === ''
    } else if (onlyNumber) {
      return /^\d+$/g.test(value) || value === ''
    } else if (unit === '%') {
      return (floatValue >= 0 && floatValue <= 100) || value === ''
    } else {
      return true
    }
  },
    [isAllowed, allowInteger, unit, onlyNumber])

  return (
    <div
      className={`${styles.container} ${_error ? 'a-error' : ''}`}
    >
      {!!label && (
        <label className={`${styles.label}`} htmlFor={name} title={label}>
          {label}
          {!!_required && <span className={`${styles.star}`}>&nbsp;*</span>}
        </label>
      )}
      <div className={`${stylesNumber.container}`}>
        <InputNumberFormat
          className={'ant-input ant-input-lg ' + className}
          onValueChange={_onChange}
          isAllowed={isAllowedFunc}
          name={name}
          id={id || name}
          disabled={_disabled}
          value={_value}
          thousandSeparator={thousandSeparator}
          {...propsInput}
          onChange={undefined}
        />
        {unit && (
          <label htmlFor={id || name} className={`${stylesNumber.unit}`}>
            {unit}
          </label>
        )}
        {suffix && (
          <label htmlFor={id || name} className={`${stylesNumber.suffix}`}>
            {suffix}
          </label>
        )}
      </div>
      <span className={`${styles.error}`}>
        {_error}
      </span>
    </div>
  )
};

InputNumber.propTypes = {
  id: PropTypes.any,
  name: PropTypes.string.isRequired,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),
  value: PropTypes.any,
  required: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.func
  ]),
  disabled: PropTypes.bool,
  type: PropTypes.string,
  onChange: PropTypes.func,
  onlyNumber: PropTypes.bool,
  allowInteger: PropTypes.number,
  isAllowed: PropTypes.func,
  unit: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),
  suffix: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),
  className: PropTypes.string,
  styleField: PropTypes.object,
  allowLeadingZeros: PropTypes.bool,
  decimalScale: PropTypes.number,
  allowNegative: PropTypes.bool,
  thousandSeparator: PropTypes.bool
}

InputNumber.defaultProps = {
  onlyNumber: true,
  className: '',
  styleField: {},
  thousandSeparator: true
}

export default InputNumber;
