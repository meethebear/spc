const arraySession = (arr, pair_count = 100) => {
  const result = []
  for (let i = 0; i < arr.length; i += pair_count) {
    result.push(arr.slice(i, i + pair_count))
  }

  return result
}

export default arraySession

