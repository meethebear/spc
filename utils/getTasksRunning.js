const getTasksRunning = (tasksRunning) => {
  return {
    loadingOverlay: getLoadingOverlay(tasksRunning),
    cancelTokens: getCancelTokens(tasksRunning)
  }
}

const getLoadingOverlay = (tasksRunning) => {
  if (!tasksRunning) {
    return false
  } else if (Object.keys(tasksRunning).length <= 0) {
    return false
  } else if (Object.keys(tasksRunning).length > 0) {
    return Object.keys(tasksRunning).some(key => tasksRunning[key]?.loading !== false && tasksRunning[key]?.loadType === 'overlay')
  } else {
    return false
  }
}

const getCancelTokens = (tasksRunning) => {
  if (!tasksRunning) {
    return []
  } else if (Object.keys(tasksRunning).length <= 0) {
    return []
  } else if (Object.keys(tasksRunning).length > 0) {
    return Object.keys(tasksRunning).map(key => tasksRunning[key]?.cancelToken || {})
  } else {
    return []
  }
}

export default getTasksRunning
