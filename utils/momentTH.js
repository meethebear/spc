const moment = require('moment-timezone')

const momentTH = (dateTime = new Date()) => moment(dateTime).tz('Asia/Bangkok')

export default momentTH
