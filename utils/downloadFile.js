const downloadFile = async (file, fileName, type) => {
  try {
    if (!file) {
      console.error('not have file', file)
      return
    }
    const fileDownload = document.createElement('a')
    const blobURL = type === 'url' ? file : window.URL.createObjectURL(file)
    fileDownload.setAttribute('href', blobURL)
    fileDownload.setAttribute('download', fileName)
    fileDownload.click()
  } catch (error) {
    console.error(error)
  }
}

export default downloadFile
