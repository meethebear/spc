class Permission {
  constructor(user) {
    this.user = user
    this.roleUser = user?.role || ''
    this.admin = 'ADMIN'
    this.superAdmin = 'SUPER_ADMIN'
  }

  isAdmin() {
    return this.admin === this.roleUser
  }

  isSuperAdmin() {
    return this.superAdmin === this.roleUser
  }


  isRole() {
    return this.roleUser
  }
}

const permission = (user) => new Permission(user)

export default permission
