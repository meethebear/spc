/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'
import getTasksRunning from '@/utils/getTasksRunning'
import { selector as selectorTrn } from '@/app/store/reducers/tasksRunning.reducer'

let timeout
const useCancelTokenAxios = props => {
  const tasksRunning = useSelector(selectorTrn)
  const router = useRouter()

  useEffect(() => {
    if (timeout) clearTimeout(timeout)
    timeout = setTimeout(() => {
      const handleRouteChange = (url, { shallow }) => {
        getTasksRunning(tasksRunning).cancelTokens.forEach(item => {
          if (typeof item?.cancel === 'function') {
            item.cancel()
          }
        })
      }

      router.events.on('routeChangeStart', handleRouteChange)
    }, 300)
  }, [tasksRunning])
}

export default useCancelTokenAxios
