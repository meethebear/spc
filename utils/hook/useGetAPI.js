import { useCallback, useEffect, useState } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector } from "react-redux";
import { resError, resOK } from "@/utils/response";
// import { message } from 'antd';

const useGetAPI = (name, path, fetchDidMount = true, auth = true) => {
  const [state, setState] = useState(null)
  const API = useAPI(name, 'none')
  const { tasksRunning } = useSelector(selector(["tasksRunning"]));

  const funcGet = useCallback(async (query) => {
    try {
      API.begin()
      const { data } = await API.get(path, { params: query, auth: auth })
      const res = resOK(data)
      if (res) {
        setState(res.data)
      } else {
        setState(null)
      }
      return res
    } catch (error) {
      const e = resError(error)
      API.error(e)
    } finally {
      API.end()
    }

    return { success: false }
  }, [])

  useEffect(() => {
    if (fetchDidMount) {
      funcGet()
    }
  }, [])

  return [state, tasksRunning?.[name]?.loading, funcGet, setState]
}
export default useGetAPI