import { useState, useEffect, useCallback } from 'react'

const usePagination = (initPage, initPerPage) => {
  const [page, setPage] = useState(initPage || 1)
  const [perPage, setPerPage] = useState(initPerPage || 10)

  useEffect(() => {
    if (initPage) setPage(initPage)
  }, [initPage])

  useEffect(() => {
    if (initPerPage) setPerPage(initPerPage)
  }, [initPerPage])

  const onChangePage = useCallback((page) => {
    setPage(page)
  }, [setPage])

  const onChangePerPage = useCallback((perPage) => {
    setPage(1)
    setPerPage(perPage)
  }, [setPage, setPerPage])

  const onChangePagination = useCallback((npage, nperPage, callback) => {
    setPage(npage)
    setPerPage(nperPage)
    if (typeof callback === 'function') {
      const bpage = perPage !== nperPage ? 1 : npage
      callback(bpage, nperPage)
    }
  }, [setPage, setPerPage, perPage])

  return {
    page,
    onChangePage,
    perPage,
    onChangePerPage,
    onChangePagination
  }
}

export default usePagination
