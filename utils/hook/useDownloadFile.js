import { useDispatch, useSelector } from 'react-redux'
import { message } from 'antd'
import useAPI from './useAPI'
import downloadFile from '../downloadFile'

const useDownloadFile = (name) => {
  const dispatch = useDispatch()
  const state = useSelector(state => state)
  const selector = () => state
  const api = useAPI(dispatch, selector, { name: name, loadType: 'overlay' })

  const onDownloadFile = async (url, fileName) => {
    try {
      const { data, statusText, headers } = await api.get(url, {
        responseType: 'blob'
      })
      const disposition = headers['content-disposition']
      const fileName = /(?:filename=")(.*?)(?=")/.exec(disposition)?.[1]?.toString?.() || 'morris-file-download'
      if (!data || statusText !== 'OK') {
        return
      }
      downloadFile(data, fileName)
    } catch (error) {
      console.error(error)
      message.error('Something went wrong to download file.')
    }
  }

  return onDownloadFile
}
export default useDownloadFile
