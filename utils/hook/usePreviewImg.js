/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import useAPI from './useAPI'

const usePreviewImg = (url, name) => {
  const [urlPreview, setUrl] = useState(null)
  const dispatch = useDispatch()
  const selector = useSelector(state => state)
  const selectorFun = () => selector
  const tasksRunning = selectorFun()?.tasksRunning
  const loading = tasksRunning?.[name]?.loading === true
  const api = useAPI(dispatch, selectorFun, { name: name, loadType: 'none' })

  const onPreviewImg = async () => {
    try {
      if (url.includes?.('blob:')) {
        setUrl(url)
        return
      }
      const { data, statusText } = await api.get(url, {
        responseType: 'blob'
      })
      if (!data || statusText !== 'OK') {
        return null
      }
      setUrl(URL.createObjectURL(data))
    } catch (error) {
    }

    return null
  }

  useEffect(() => {
    onPreviewImg()
  }, [url])

  return [urlPreview, loading]
}
export default usePreviewImg
