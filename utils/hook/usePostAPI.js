import { useCallback } from "react";
import useAPI from "@/utils/hook/useAPI";
import selector from "@/utils/selector";
import { useSelector } from "react-redux";
import { resError, resOK } from "@/utils/response";
// import { message } from 'antd';

const usePostAPI = (name, path) => {
  const API = useAPI(name, 'overlay')
  const { tasksRunning } = useSelector(selector(["tasksRunning"]));

  const funcPost = useCallback(async (values) => {
    const body = values
    try {
      API.begin()
      const { data } = await API.post(path, body)
      const res = resOK(data)
      // message.success(res.message)
      return res
    } catch (error) {
      const e = resError(error)
      API.error(e)
    } finally {
      API.end()
    }

    return { success: false }
  }, [])

  return [funcPost, tasksRunning?.[name]?.loading]
}
export default usePostAPI