import { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { userTokenExpire, userRefreshToken as refreshTokenAction } from '@/actions/user.action'
import useCallRefaceToken from '@/features/session/api/useCallRefaceToken.api'

let interval
const checkTime = expiring => {
  if (!expiring) return false
  const now = new Date().getTime()
  const exp = new Date(expiring).getTime()

  if (now >= exp) {
    return true
  } else {
    return false
  }
}

const useRefreshToken = () => {
  const user = useSelector(state => state.user)
  const dispatch = useDispatch()
  const [APIRefaceToken] = useCallRefaceToken()

  const callAPI = useCallback(async () => {
    const { success, data } = await APIRefaceToken()
    if (success) {
      dispatch(refreshTokenAction(data))
    } else {
      dispatch(userTokenExpire())
    }
  }, [APIRefaceToken, dispatch])

  useEffect(() => {
    const toCallAPI = checkTime(user.expiringDate)
    if (toCallAPI) {
      callAPI()
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (interval) clearInterval(interval)
    interval = setInterval(() => {
      const toCallAPI = checkTime(user.expiringDate)
      if (toCallAPI) {
        callAPI()
      }
    }, 1000)
  }, [user?.expiringDate, callAPI])
}

export default useRefreshToken
