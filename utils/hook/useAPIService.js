import { useDispatch, useSelector } from 'react-redux'
import { useApiService as API } from '../api'
import { taskFinished, taskStart, taskError } from '@/app/actions/tasksRunning.action'
import { userTokenExpire } from '@/actions/user.action'

/**
 * @param {string} name is name api
 * @param {string} loadType is config load => [overlay, none]
 */
const useAPIService = (name, loadType, locale) => {
  const dispatch = useDispatch()
  const userAuth = useSelector(state => state.user)
  const start = (cancelToken) => dispatch(taskStart(name, loadType, cancelToken))
  const end = () => setTimeout(() => { dispatch(taskFinished(name)) }, 200)
  const error = (...a) => dispatch(taskError(name, ...a))
  const onExpire = () => dispatch(userTokenExpire())
  const api = API(userAuth, start, end, error, locale, onExpire)
  return api
}

export default useAPIService
