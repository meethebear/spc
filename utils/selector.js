
/**
 * @param {Array} keys is reducers name
 * @returns {Function} function is return {object}
 */
const selector = (keys) => {
  /**
 * @param {object} state is state in redux store
 * @returns {object} state in keys
 */
  const selectorByKeys = (state) => {
    const result = {}
    keys.forEach(key => {
      result[key] = state[key]
    })

    return result
  }

  return selectorByKeys
}

export default selector
