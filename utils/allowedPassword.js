export const isAllowedPassword = [
  {
    func: value => value.length >= 8 && value.length <= 30,
    msg: 'Your password must be between 8 and 30 characters'
  },
  {
    func: value => /[A-Z]/.test(value),
    msg: 'Your password must contain at least one uppercase, or capital, letter (ex: A, B, etc.)'
  },
  {
    func: value => /[a-z]/.test(value),
    msg: 'Your password must contain at least one lowercase letter.'
  },
  {
    func: value => /[0-9]/.test(value),
    msg: 'Your password must contain at least one number digit (ex: 0, 1, 2, 3, etc.)'
  },
  {
    func: value => /\W|_/.test(value),
    msg: 'Your password must contain at least one special character -for example: $, #, @, !,%,^,&,*,(,)'
  }
]

const checkAllowedPassword = (value) => isAllowedPassword.every(item => item.func(value))

export default checkAllowedPassword