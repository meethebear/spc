import config from '../config'

export const MESSAGE = {
  SUCCESS: "Save Success.",
  ERROR: "Something went wrong."
}

export const API_PATH = {
  AUTH: config.hostBackend + '/auth',
  ADMIN: config.hostBackend + '/admin',
  USER: config.hostBackend + '/user'
}

export const STATUS_ORDER = [
  {
    label: "รอผลิต",
    value: "PRODUCTION_PENDING"
  },
  {
    label: "กำลังผลิต",
    value: "PRODUCTION_DOING"
  },
  {
    label: "ผลิตเสร็จ",
    value: "PRODUCTION_FINISHED"
  },
  {
    label: "กำลังจัดส่ง",
    value: "TRANSPORTATION_DOING"
  },
  {
    label: "สำเร็จแล้ว",
    value: "TRANSPORTATION_FINISHED"
  },
  {
    label: "ไม่สำเร็จ",
    value: "TRANSPORTATION_FAILED"
  },
]

export const STATUS_ORDER_MAP = {
  PRODUCTION_PENDING: 'PRODUCTION_PENDING',
  PRODUCTION_DOING: 'PRODUCTION_DOING',
  PRODUCTION_FINISHED: 'PRODUCTION_FINISHED',
  TRANSPORTATION_DOING: 'TRANSPORTATION_DOING',
  TRANSPORTATION_FINISHED: 'TRANSPORTATION_FINISHED',
  TRANSPORTATION_FAILED: 'TRANSPORTATION_FAILED',
}

export const STATUS_ORDER_MAP_STEP = {
  PRODUCTION_PENDING: 0,
  PRODUCTION_DOING: 0,
  PRODUCTION_FINISHED: 1,
  TRANSPORTATION_DOING: 1,
  TRANSPORTATION_FINISHED: 2,
  TRANSPORTATION_FAILED: 2,
}

export const DEPARTMENT = [
  {
    label: "เจ้าหน้าที่",
    value: "OFFICER"
  },
  {
    label: "ฝ่ายผลิต",
    value: "PRODUCTION"
  },
  {
    label: "ฝ่ายจัดส่ง",
    value: "TRANSPORTATION"
  },
]
