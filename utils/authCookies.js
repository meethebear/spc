import { serialize, parse } from 'cookie'

const TOKEN_NAME = 'photon-auth'

export const MAX_AGE = (60 * 60) * 24 // 60 * 10

export function setTokenCookie(res, token, _MAX_AGE = MAX_AGE) {
  const cookie = serialize(TOKEN_NAME, token, {
    maxAge: _MAX_AGE,
    expires: new Date(Date.now() + _MAX_AGE * 1000),
    httpOnly: true,
    secure: process.env.SECURE_COOKIE === 'TRUE',
    path: '/',
    sameSite: 'lax'
  })

  res.setHeader('Set-Cookie', cookie)
}

export function removeTokenCookie(res) {
  const cookie = serialize(TOKEN_NAME, '', {
    maxAge: -1,
    path: '/'
  })

  res.setHeader('Set-Cookie', cookie)
}

export function parseCookies(req) {
  // For API Routes we don't need to parse the cookies.
  if (req?.cookies) return req.cookies

  // For pages we do need to parse the cookies.
  const cookie = req?.headers?.cookie
  return parse(cookie || '')
}

export function getTokenCookie(req) {
  const cookies = parseCookies(req)
  return cookies[TOKEN_NAME]
}
