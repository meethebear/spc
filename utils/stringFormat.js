import momentTH from './momentTH'
import 'moment/locale/th'

class StringFormat {
  constructor(value) {
    this.value = !value && value !== 0 ? '' : value
  }

  price() {
    if (!this.value) return '0.00'
    return parseFloat(this.value).toLocaleString('en-US', {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    })
  }

  normal(precision) {
    if (!this.value) {
      if (precision) {
        return (0).toFixed(precision)
      }

      return '0'
    }
    if (precision) {
      return this._toFixed(precision)?.toString()?.replace(/\B(?=(\d{3})+(?!\d))/g, ',') || ''
    } else {
      const normal = this.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
      return normal
    }
  }

  _toFixed(precision) {
    if (!this.value) return '0'
    return (+(Math.round(+(this.value + 'e' + precision)) + 'e' + -precision)).toFixed(
      precision
    )
  }

  dateTH(format) {
    if (!this.value) return ''
    if (this.value.length === 0) return null
    if (momentTH(this.value).isValid()) {
      const DATEFORMAT = momentTH(this.value).format(format || 'DD/MM/YYYY')
      const yyyy = /\d{4}/.exec(DATEFORMAT)
      const yyyyInt = parseInt(yyyy)
      let yyyyTH = yyyy
      if (!isNaN(yyyyInt)) yyyyTH = yyyyInt + 543
      const DDMMYYYY = DATEFORMAT.replace(/\d{4}/, yyyyTH)
      return DDMMYYYY
    } else {
      return this.value
    }
  }

  /**
   * _formater: string // '##-##-##'
   */
  formater(_formater) {
    let textFormated = _formater
    const strPainText = `${this.value}`

    for (const c of strPainText) {
      textFormated = textFormated.replace('#', c)
    }

    return textFormated
  }

  date(format, locale = 'th') {
    if (!this.value) return ''
    if (this.value.length === 0) return null
    if (momentTH(this.value).isValid()) {
      let DATEFORMAT = momentTH(this.value).locale('en').format(format || 'DD/MM/YYYY')
      if (locale === 'th') {
        DATEFORMAT = momentTH(this.value).locale('th').format(format || 'DD/MM/YYYY')

      }
      return DATEFORMAT
    } else {
      return this.value
    }
  }
}

const format = value => new StringFormat(value)

export default format
