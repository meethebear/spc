export default {
  development: {
    hostBackend: 'http://203.154.82.42:6002/api/v1',
    host: 'http://localhost:4200',
    basePath: ''
  },
  production: {
    hostBackend: process.env.NEXT_PUBLIC_HOST_BACKEND,
    host: process.env.NEXT_PUBLIC_HOST,
    basePath: ''
  }
}[process.env.NODE_ENV || 'development']
