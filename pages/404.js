import Image from 'next/image'
import Link from 'next/link'
import styles from '@/styles/Page404.module.css'
import { useSelector } from 'react-redux'

const Page404 = () => {
  const user = useSelector((state) => state.user);
  return (
    <div className={styles.container}>
      <Image
        width={235}
        height={156}
        src={'/images/404-error.png'}
        alt={'404'}
      />
      <br />
      <br />
      <h3>Page is not found</h3>
      <p>
        The page you’re looking for might have been removed <br /> had it’s name
        changed or is temporarily unavaliable.
      </p>

      <br />
      {user?.role === "SUPER_ADMIN" ? 
      <Link href="/backoffice/user">
        <a className="ant-btn ant-btn-primary ant-btn-lg">{'Back to Home'}</a>
      </Link> :
      <Link href="/backoffice/dashboard">
        <a className="ant-btn ant-btn-primary ant-btn-lg">{'Back to Home'}</a>
      </Link>}
    </div>
  )
}

export default Page404
