import { getLoginSession } from '@/utils/auth'
import { ProviderSession } from '@/app/providers/PrivateProvider'
import Welcome from '@/features/session/screen/Welcome'
// import menu from '../menu'
import PropTypes from 'prop-types';

const SignIn = props => {
  const { user } = props;
  return (
    <ProviderSession session={user}>
      <Welcome {...props} session={user} />
    </ProviderSession>
  )
}

export async function getServerSideProps({ req }) {
  const session = await getLoginSession(req)
  let user = null
  if (session && session?.isLoggedIn) {
    let userMenu = session.menu
    
    user = { ...session, menu: userMenu }
  }
  return {
    props: {
      user
    }
  }
}

SignIn.propTypes = {
  user: PropTypes.any,
}

export default SignIn
