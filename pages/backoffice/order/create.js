import AppProvider from "@/app/providers/AppProvider";
import Layout from "@/components/layout/backoffice/Layout";
import OrderCreateScreen from "@/features/backoffice/order/screen/Create";

const OrderCreate = () => {
  return (
    <AppProvider>
      <Layout>
        <OrderCreateScreen />
      </Layout>
    </AppProvider>
  )
}

export default OrderCreate

