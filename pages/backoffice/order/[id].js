import { useRouter } from 'next/router'
import AppProvider from "@/app/providers/AppProvider";
import Layout from "@/components/layout/backoffice/Layout";
import OrderInfoScreen from "@/features/backoffice/order/screen/Info";

const OrderInfo = () => {
  const { query } = useRouter()
  return (
    <AppProvider>
      <Layout>
        {query.id && (
          <OrderInfoScreen id={query.id} />
        )}
      </Layout>
    </AppProvider>
  )
}

export default OrderInfo

