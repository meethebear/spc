import Layout from "@/components/layout/backoffice/Layout";
import AppProvider from "@/app/providers/AppProvider";
import TaskEdit from "@/features/backoffice/order/screen/edit";

const Edit = () => {
  return (
    <AppProvider>
      <Layout>
        <TaskEdit />
      </Layout>
    </AppProvider>
  );
};

export default Edit;
