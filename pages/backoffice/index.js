const BackOffice = () => {
  return (
    <div></div>
  )
}

export async function getServerSideProps() {
  return {
    redirect: {
      destination: '/backoffice/dashboard',
      permanent: false
    }
  }
}

export default BackOffice
