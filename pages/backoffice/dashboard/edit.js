import Layout from "@/components/layout/backoffice/Layout";
import AppProvider from "@/app/providers/AppProvider";
import DashboardEdit from "@/features/backoffice/dashboard/screen/edit";

const Edit = () => {
  return (
    <AppProvider>
      <Layout>
        <DashboardEdit />
      </Layout>
    </AppProvider>
  );
};

export default Edit;
