import AppProvider from '@/app/providers/AppProvider'
import Layout from '@/components/layout/backoffice/Layout'
import Dashboard from '@/features/backoffice/dashboard/screen'
import { useSelector } from "react-redux";
import Page404 from 'pages/404';

const BackOfficeHome = () => {
  const user = useSelector((state) => state.user);
  return (
    <AppProvider roles={['EMPLOYEE']} department={['OFFICER']}>
      {user?.role === "SUPER_ADMIN" ? <Page404/> :
      <Layout>
        <Dashboard />
      </Layout>
      }
    </AppProvider>
  )
}

export default BackOfficeHome
