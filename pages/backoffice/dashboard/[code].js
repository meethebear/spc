import AppProvider from "@/app/providers/AppProvider";
import Layout from "@/components/layout/backoffice/Layout";
import Detail from "@/features/backoffice/dashboard/screen/detail";
import { useRouter } from "next/router";

const ProductionDetail = () => {
  const { query } = useRouter();

  return (
    <AppProvider>
      <Layout>
        {query.code && (
          <Detail id={query.code} />
        )}
      </Layout>
    </AppProvider>
  );
}

export default ProductionDetail