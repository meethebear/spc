import Layout from "@/components/layout/backoffice/Layout";
import AppProvider from "@/app/providers/AppProvider";
import DetailVendor from "@/features/backoffice/customer/component/DetailVendor";

const Detail = () => {
  return (
    <AppProvider>
      <Layout>
        <DetailVendor />
      </Layout>
    </AppProvider>
  );
};

export default Detail;
