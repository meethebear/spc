import Layout from "@/components/layout/backoffice/Layout";
import AppProvider from "@/app/providers/AppProvider";
import ManageVendor from "@/features/backoffice/customer/component/ManageVendor";

const Edit = () => {
  return (
    <AppProvider>
      <Layout>
        <ManageVendor />
      </Layout>
    </AppProvider>
  );
};

export default Edit;
