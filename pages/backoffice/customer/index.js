import Layout from "@/components/layout/backoffice/Layout";
import CustomerPage from "@/features/backoffice/customer/component/Vendor";
import AppProvider from "@/app/providers/AppProvider";
import { useSelector } from "react-redux";
import Page404 from "pages/404";

const Task = () => {
  const user = useSelector((state) => state.user);

  return (
    <AppProvider>
      {user?.role === "SUPER_ADMIN" ? <Page404/>  
      : <Layout>
        <CustomerPage />
      </Layout>}
    </AppProvider>
  );
};

export default Task;
