import Layout from "@/components/layout/backoffice/Layout";
import AppProvider from "@/app/providers/AppProvider";
import ManageVendorCreate from "@/features/backoffice/customer/component/ManageVendorCreate";

const Create = () => {
  return (
    <AppProvider>
      <Layout>
        <ManageVendorCreate />
      </Layout>
    </AppProvider>
  );
};

export default Create;
