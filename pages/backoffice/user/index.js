import AppProvider from "@/app/providers/AppProvider";
import Layout from "@/components/layout/backoffice/LayoutAdmin";
import UserIndexScreen from "@/features/backoffice/user/screen";
import { useSelector } from "react-redux";
import Page404 from 'pages/404';

const UserIndex = () => {
  const user = useSelector((state) => state.user);

  return (
    <AppProvider>
      {user?.role === "SUPER_ADMIN" ? 
      <Layout>
        <UserIndexScreen />
      </Layout>
      : <Page404/>}
    </AppProvider>
  )
}

export default UserIndex

