import AppProvider from "@/app/providers/AppProvider";
import Layout from "@/components/layout/backoffice/LayoutAdmin";
import UserInfoScreen from "@/features/backoffice/user/screen/Info";

const UserInfo = () => {
  return (
    <AppProvider>
      <Layout>
        <UserInfoScreen />
      </Layout>
    </AppProvider>
  )
}

export default UserInfo

