import AppProvider from "@/app/providers/AppProvider";
import Layout from "@/components/layout/backoffice/LayoutAdmin";
import UserCreateScreen from "@/features/backoffice/user/screen/Create";

const UserCreate = () => {
  return (
    <AppProvider>
      <Layout>
        <UserCreateScreen />
      </Layout>
    </AppProvider>
  )
}

export default UserCreate

