import { getLoginSession } from '@/utils/auth'
import AppProvider from '@/app/providers/AppProvider'
import LoginScreen from '@/features/login/screen'

const Login = props => {
  return (
    <AppProvider publicPage>
      <LoginScreen {...props} />
    </AppProvider>
  )
}

export async function getServerSideProps({ req }) {
  const session = await getLoginSession(req)
  const user = null

  if (session && session?.isLoggedIn) {
    let destinationPath = '/backoffice/dashboard'
    if (session.role === 'SUPER_ADMIN') {
      destinationPath = '/user'
    }
    return {
      redirect: {
        destination: destinationPath,
        permanent: false
      }
    }
  }
  return {
    props: {
      user,
      error: {
        message: session?.message || null, message_th: session?.message_th || null
      }
    }
  }
}

export default Login
