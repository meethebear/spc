// import axios from 'axios'
// import { API_PATH } from '@/utils/constant'
import { setLoginSession, getLoginSession } from '@/utils/auth'
import { MAX_AGE } from '@/utils/authCookies'

// const url = API_PATH.AUTH + '/refresh_token'
async function handler(req, res) {
  try {
    const session = await getLoginSession(req)
    if (!session?.token) {
      res.status(401).end('not token')
      return
    }

    // const token = session.token

    // const { data, status } = await axios.post(url, null, {
    //   headers: {
    //     Authorization: 'Bearer ' + token
    //   }
    // })

    // console.log(
    //   'data', data
    // )

    // const response = data
    const tokenMock = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'

    const response = { token: tokenMock }
    response.expiringDate = new Date(Date.now() + (MAX_AGE * 1000) - ((3 * 60) * 1000))

    // if (!(status >= 200 && status < 400)) {
    //   res.status(status).json(response)
    //   return
    // }
    const user = {
      ...session,
      expiringDate: response.expiringDate,
      token: response.token
    }
    user.isLoggedIn = true
    await setLoginSession(res, user)
    res.status(200).json(response)
  } catch (error) {
    console.log('error', error?.response)
    res.status(401).json((error?.response?.data) || {})
    res.end()
  }
}

export default handler
