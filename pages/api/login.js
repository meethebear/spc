import axios from 'axios'
import { setLoginSession } from '@/utils/auth'
import { MAX_AGE } from '@/utils/authCookies'
import config from '../../config'


async function handler(req, res) {
  const { username, password } = await req.body
  try {
    if (![username, password].every(val => String(val).trim())) {
      res.status(400).json({ message: 'Bad request' })
      return
    }

    const url = config.hostBackend + '/user/sign_in'
    const { data, status } = await axios.post(url, { username, password })

    if (!(status >= 200 && status < 400)) {
      res.status(status).json(data)
      return
    }
    const user = data.data
    const now = new Date()
    user.isLoggedIn = true
    user.loggedInDate = now
    user.expiringDate = new Date(Date.now() + (MAX_AGE * 1000) - ((3 * 60) * 1000))
    await setLoginSession(res, user)
    let _redirectURL = (config.basePath || '') + '/signin'
    res.redirect(_redirectURL)
  } catch (error) {
    console.log('error.response', error?.response)
    let redirectURL = config.basePath + '/login'

    const user = {
      ...(error?.response?.data),
      message: error?.response?.data?.message === 'Unauthorized' ? 'Invalid Username or Password' : error?.response?.data?.message,
      isLoggedIn: false
    }
    const MAX_AGE = 4
    await setLoginSession(res, user, MAX_AGE)
    res.redirect(redirectURL)
    // res.status(401).json((error?.response?.data) || {})
    // res.end()
  }
}

export default handler
