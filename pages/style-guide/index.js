import { useCallback } from 'react'
import Link from 'next/link'
import { Logout } from '@/components/icons'
import DropDown from '@/components/DropDown'

const StyleGuide = () => {

  const handlerLogout = useCallback(() => {
    console.log('onClick DropDownItem Logout')
  }, [])
  return (
    <main>
      <Link href={'/style-guide/form-exp'}>
        <a>Form Exp</a>
      </Link>

      <Link href={'/style-guide/form-list-exp'}>
        <a>Form List Exp</a>
      </Link>


      <DropDown
          title=''
          items={[{ label: <div className='flex align-center'><Logout /> &nbsp;Logout </div>, key: 'logout', onClick: handlerLogout }]}
        />
    </main>
  )
}

export default StyleGuide
