import { useRouter } from 'next/router'
import AppProvider from '@/app/providers/AppProvider'
import Layout from '@/components/layout/Layout'
import TrackingScreen from '@/features/tracking/screen/Info'

const Tracking = () => {
  const { query } = useRouter()
  return (
    <AppProvider publicPage>
      <Layout>
        {!!query.code && (
          <TrackingScreen code={query.code} />
        )}
      </Layout>
    </AppProvider>
  )
}

export default Tracking
