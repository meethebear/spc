import AppProvider from '@/app/providers/AppProvider'
import Layout from '@/components/layout/Layout'
import TrackingScreen from '@/features/tracking/screen'

const Tracking = () => {
  return (
    <AppProvider publicPage>
      <Layout>
        <TrackingScreen />
      </Layout>
    </AppProvider>
  )
}

export default Tracking
