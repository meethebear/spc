import Head from 'next/head'
import { ConfigProvider } from 'antd';
import { wrapper } from '../app/store'
import { colors, font } from '../app/theme'
import "antd/dist/antd.variable.css"
import localeTh from "antd/lib/locale/th_TH"
import '../styles/globals.css'
import PropTypes from 'prop-types';

ConfigProvider.config({
  theme: {...colors, ...font },
})

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Photon Express {Component.title ? '- ' + Component.title : ''}</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <ConfigProvider
        componentSize={'large'}
        locale={localeTh}
      >
        <Component {...pageProps} />
      </ConfigProvider>
    </>
  )
}

MyApp.propTypes = {
  Component: PropTypes.any,
  pageProps: PropTypes.any,
}

export default wrapper.withRedux(MyApp)
